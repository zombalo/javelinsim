var email_regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var ajax = 'ajax.php';

function getParameterByName(name, url) {
	if (!url) {
		url = window.location.href;
	}
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
};

var ajaxWrap = function(options, callback = function(){}, method = 'POST', async = true)
{
	options['request_page'] = getParameterByName('page');

	$.ajax(ajax, {
		method: method,
		async: async,
		data: options,
		success: function(data){
			callback(data);
		}
	});
};

$(document).ready(function(){
	setTimeout(function(){
		var preloadClass = 'preload',
		timeout = 0,
		interval = 70;
		$('.login-page .input-block > *').each(function(){
			var element = $(this);
			setTimeout(function(){
				element.removeClass(preloadClass);
			}, timeout);
			timeout += interval;
		});

		setTimeout(function(){
			timeout = 0,
			interval = 500;
			$('.login-page .right-side img').each(function(){
				var element = $(this);
				setTimeout(function(){
					element.removeClass(preloadClass);
				}, timeout);
				timeout += interval;
			});
		}, 1000);

		timeout = 0,
		interval = 100;

		$('.account-inputs').each(function(){
			var element = $(this);
			setTimeout(function(){
				element.removeClass(preloadClass);
			}, timeout);
			timeout += interval;
		});

		$('.sim-block').each(function(){
			var element = $(this);
			setTimeout(function(){
				element.removeClass(preloadClass);
			}, timeout);
			timeout += interval;
		});

		$('.qa-block').each(function(){
			var element = $(this);
			setTimeout(function(){
				element.removeClass(preloadClass);
			}, timeout);
			timeout += interval;
		});

		$('.blog-post').each(function(){
			var element = $(this);
			setTimeout(function(){
				element.removeClass(preloadClass);
			}, timeout);
			timeout += interval;
		});
	}, 350);

	/*Build charts*/

	// feed-back-1-tab-chart

	if($('.visual-chart').length > 0)
	{
		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawMultSeries);
		// google.charts.setOnLoadCallback(drawChartTab2_1);

		function drawMultSeries() {
			var data = google.visualization.arrayToDataTable([
				['Action', 'Observe', ''],
				['1', 50, 100],
				['2', 30, 60],
				['3', 70, 30],
				['4', 20, 55]
			]);


			var options = {
				title: 'Scenario Summary',
				legend: {position: 'bottom'},
			};

			var chart = new google.visualization.ColumnChart($('.feed-back-1-tab-chart').get(0));

			chart.draw(data, options);
		}

		// function drawChartTab2_1() {
		// 	var data = google.visualization.arrayToDataTable([
		// 		['Communations', '%'],
		// 		['1', 77],
		// 		['1', 13]
		// 	]);

		// 	var options = {
		// 		pieHole: 0.5,
		// 		pieSliceTextStyle: {
		// 			color: 'black',
		// 		},
		// 		legend: 'none'
		// 	};

		// 	var chart = new google.visualization.PieChart($('.feed-back-2-tab-chart-1').get(0));
		// 	chart.draw(data, options);
		// }
	}

});

/*Document resize events*/

$(window).resize(function(){

	if($(window).width() > 767)
	{
		$('.header-menu .items').removeAttr('style');
		$('.header-menu .items').removeClass('trsnone');
		$('.header-menu .menu-button').removeClass('active');
	}
});

/*Responsive menu*/

$('.header-menu .menu-button').click(function(){
	var menuItems = $(this).closest('.header-menu').find('.items'),
	headButton = $(this);

	if(headButton.hasClass('active'))
	{
		headButton.removeClass('active');
		menuItems.addClass('trsnone');

		setTimeout(function(){
			menuItems.slideToggle(300, 'easeInOutExpo', function(){
				menuItems.removeClass('trsnone');
			});
		}, 100);
	}
	else
	{
		menuItems.addClass('trsnone');
		headButton.addClass('active');

		setTimeout(function(){
			$(menuItems).slideToggle(300, 'easeInOutExpo', function(){
				menuItems.removeClass('trsnone');
			});
		}, 100);
	}
});

/*Charts part*/

$('body').on('click', '.tabs-widget .tab-item', function(){
	var tabDelay = 300;
	var cardDelay = 300;
	var set = $(this).closest('.tabs').find('.tab-item');
	var index = set.index($(this));
	var item = $(this);
	var tabs = $(this).closest('.tabs-widget').find('.card-tab-item');

	$(set).removeClass('active');
	item.addClass('active');

	$(tabs).removeClass('active');
	setTimeout(function(){
		$(tabs).removeClass('block');
		$(tabs).eq(index).addClass('block');
		setTimeout(function(){
			$(tabs).eq(index).addClass('active');
		}, cardDelay);
	}, cardDelay);
});

/**************/
/*Sim progress*/
/**************/

var wholeSlides = $('.video-slide').length - 1;
var lastSlide = 0;
var currentSlide = 0;
var nextActive = true;
var prevActive = true;
var recordersCount = $('ziggeorecorder').length;
var recordedCount = 0;

var switchNextButton = function(option)
{
	if(option)
	{
		$('.next-button').removeClass('disabled');
		nextActive = true;
	}
	else
	{
		$('.next-button').addClass('disabled');
		nextActive = false;
	}
};

var switchPrevButton = function(option)
{
	if(option)
	{
		$('.prev-button').removeClass('disabled');
		prevActive = true;
	}
	else
	{
		$('.prev-button').addClass('disabled');
		prevActive = false;
	}
};

$('.video-page .next-button').click(function(){
	if(!nextActive)
		return;

	var index,
	set = $('.stats-wrap .progress-item'),
	videoSet = $('.video-item .video-slide');
	
	$('.stats-wrap .progress-item').each(function(){
		if($(this).hasClass('active'))
		{
			index = set.index($(this));
			return false;
		}
	});
	
		if(index == set.length - 1)
		return;

	//Data grabbing
	var players = $('ziggeoplayer').length,
	button = $(this);
	for(var i = 0; i < players; i++)
	{
		ZiggeoApi.V2.Player.findByElement($('ziggeoplayer').get(i)).stop();
	}

	var recorders = $('ziggeorecorder').length;
	for(var i = 0; i < recorders; i++)
	{
		ZiggeoApi.V2.Player.findByElement($('ziggeorecorder').get(i)).stop();
	}
	
		//Display next question/home page button with loading or just display if videos fully loaded
	if(index + 1 == set.length - 1 && allUploaded == false)
	{
		$('.next-button').addClass('hidden');
		$('.next-main-button-loading').removeClass('hidden');
	}
	else if(index + 1 == set.length - 1 && allUploaded == true)
	{
		$('.next-button').addClass('hidden');
		$('.next-main-button').removeClass('hidden');
	}

	currentSlide++;

	if(currentSlide > lastSlide)
		lastSlide++;

	console.log('currentSlide: ' + currentSlide + ', lastSlide: ' + lastSlide);

		//If new slide bacame a recorder, disable next button
	if(videoSet.eq(index+1).find('ziggeorecorder').length > 0 && currentSlide == lastSlide)
	{
		var recorderIndex = $('ziggeorecorder').index(videoSet.eq(index+1).find('ziggeorecorder'));
		if(recorderIndex >= recordedCount)
		{
			switchNextButton(false);
			switchPrevButton(false);
		}
	}

	//Animations part
	$(set).eq(index).removeClass('active');
	$(videoSet).eq(index).addClass('prev');

	setTimeout(function(){
		$(set).eq(index+1).addClass('active');
	}, 100);

	setTimeout(function(){
		$(videoSet).eq(index).addClass('hidden');

		$(videoSet).eq(index+1).removeClass('hidden');

		setTimeout(function(){
			$(videoSet).eq(index+1).removeClass('next');
		}, 50);
	}, 350);
});

$('.video-page .prev-button').click(function(){
	if(!prevActive)
		return;

	var index,
	set = $('.stats-wrap .progress-item'),
	videoSet = $('.video-item .video-slide');

	$('.stats-wrap .progress-item').each(function(){
		if($(this).hasClass('active'))
		{
			index = set.index($(this));
			return false;
		}
	});

	if(index == 0)
		return;

	//Data grabbing
	var players = $('ziggeoplayer').length,
	button = $(this);
	for(var i = 0; i < players; i++)
	{
		ZiggeoApi.V2.Player.findByElement($('ziggeoplayer').get(i)).stop();
	}

	var recorders = $('ziggeorecorder').length;
	for(var i = 0; i < recorders; i++)
	{
		ZiggeoApi.V2.Player.findByElement($('ziggeorecorder').get(i)).stop();
	}

	currentSlide--;
	if(currentSlide < 0)
		currentSlide = 0;
	console.log('currentSlide: ' + currentSlide + ', lastSlide: ' + lastSlide);
	switchNextButton(true);

	//Animations part
	$(set).eq(index).removeClass('active');
	$(videoSet).eq(index).addClass('next');

	setTimeout(function(){
		$(set).eq(index-1).addClass('active');
	}, 100);

	setTimeout(function(){
		$(videoSet).eq(index).addClass('hidden');

		$(videoSet).eq(index-1).removeClass('hidden');

		setTimeout(function(){
			$(videoSet).eq(index-1).removeClass('prev');
		}, 50);
	}, 350);
	});

	var videoSlides = $('ziggeorecorder').length;
	var loadedVideos = 0;
	var allUploaded = false;

	var videoSlides = $('ziggeorecorder').length;
	var loadedVideos = 0;
	var allUploaded = false;

	var videoUploaded = function()
	{
		loadedVideos++;

		if(loadedVideos == videoSlides)
		{
			allUploaded = true;

			if(lastSlide == wholeSlides)
			{
				$('.next-button').addClass('hidden');
				$('.next-main-button-loading').addClass('hidden');
				$('.next-main-button').removeClass('hidden');
			}
		}
	};

if($('ziggeorecorder').length > 0)
{
	ZiggeoApi.Events.on('system_ready', function() {

		if($('.for-rev-rec').length > 0)
		{
			ZiggeoApi.V2.Recorder.findByElement($('.for-rev-rec').get(0)).on('uploading', function(){
				switchNextButton(true);
				switchPrevButton(true);
				recordedCount++;
			});

			ZiggeoApi.V2.Recorder.findByElement($('.for-rev-rec').get(0)).on('verified', function(){
				ajaxWrap({
					action: 'save_ans_token',
					token_index: '1',
					answer: $('.ans-id').text(),
					invite: $('.invite-id').text(),
					token: this.get('video')
				}, function()
				{
					videoUploaded();
				});

				if($('.for-rev-pl').length > 0)
				{
					$('.for-rev-pl .video-preload').addClass('hidden');
					$('.for-rev-pl').append('<ziggeoplayer ' +
								'class="z-player for-rev-pl" ' +
								'ziggeo-video="' + this.get('video') + '" ' +
								'ziggeo-responsive=true ' +
								'ziggeo-height=340' +
								'></ziggeoplayer>');
				}
			});
		}

		if($('.ans-af-rev').length > 0)
		{
			ZiggeoApi.V2.Recorder.findByElement($('.ans-af-rev').get(0)).on('uploading', function(){
				switchNextButton(true);
				switchPrevButton(true);
				recordedCount++;
			});

			ZiggeoApi.V2.Recorder.findByElement($('.ans-af-rev').get(0)).on('verified', function(){
				ajaxWrap({
					action: 'save_ans_token',
					token_index: '2',
					answer: $('.ans-id').text(),
					invite: $('.invite-id').text(),
					token: this.get('video')
				}, function()
				{
					videoUploaded();
					switchPrevButton(true);
				});
			});
		}

		if($('.ans-af-str').length > 0)
		{
			ZiggeoApi.V2.Recorder.findByElement($('.ans-af-str').get(0)).on('uploading', function(){
				switchNextButton(true);
				switchPrevButton(true);
				recordedCount++;
			});
		
			ZiggeoApi.V2.Recorder.findByElement($('.ans-af-str').get(0)).on('verified', function(){
				ajaxWrap({
					action: 'save_ans_token',
					token_index: '3',
					answer: $('.ans-id').text(),
					invite: $('.invite-id').text(),
					token: this.get('video')
				}, function()
				{
					videoUploaded();
				});
			});
		}
	});
}

/**************/
/*Sim progress*/
/**************/

var setRequired = function(input, message = 'Input required')
{
	setTimeout(function(){
		var element = $('.required-field');
		element.removeClass('hidden');

		setTimeout(function(){
			var offset = $(input).position(),
			width = $(input).outerWidth(),
			height = $(input).outerHeight(),
			rqHeight = $(element).outerHeight();
			$(element).text(message);
			$(element).css('left', offset.left + width);
			$(element).css('top', offset.top + ((height / 2) - (rqHeight / 2)));

			setTimeout(function(){
				element.addClass('active');
			}, 100);
		}, 100);
	}, 650);
};

var hideRequired = function()
{
	$('.required-field').removeClass('active');

	setTimeout(function(){
		$('.required-field').addClass('hidden');
	}, 350);
};

$('.login-button').click(function(){
	hideRequired();

	var forReturn = function(option, element, message)
	{
		if(option)
		{
			setRequired(element, message);
		}
	};

	switch(getParameterByName('page'))
	{
		case 'login':
		{
			var forReturnTrigger = false,
			forReturnElement = null;

			$('.input-block .input-check').each(function(){
				if($(this).val() == '')
				{
					forReturnElement = $(this);
					forReturnTrigger = true;
					return false;
				}
			});

			if(forReturnTrigger)
			{
				forReturn(forReturnTrigger, forReturnElement);
				return;
			}

			if(!$('#input-id-1').is(':checked'))
			{
				forReturn(true, $('.terms'));
				return;
			}

			ajaxWrap({
				action: 'login',
				email: $('input.user-email').val(),
				password: $('input.password').val(),
				remember: $('.remember input').is(':checked')
			}, function(data){
				if(data == 'error')
					forReturn(true, $('.login-button'), 'Incorrect data');
				else if(data = 'success')
					location.href = '?page=overview';
			});

			break;
		}
		case 'register':
		{
			var forReturnTrigger = false,
			forReturnElement = null;

			$('.input-block .input-check').each(function(){
				if($(this).val() == '')
				{
					forReturnElement = $(this);
					forReturnTrigger = true;
					return false;
				}
			});

			if(forReturnTrigger)
			{
				forReturn(forReturnTrigger, forReturnElement);
				return;
			}

			if(!email_regexp.test($('input.user-email').val()))
			{
				forReturnElement = $('input.user-email');
				forReturnTrigger = true;
			}

			if(forReturnTrigger)
			{
				forReturn(forReturnTrigger, forReturnElement, 'Email is not valid');
				return;
			}

			ajaxWrap({
				action: 'email_exists',
				email: $('input.user-email').val()
			}, function(data){
				if(data == 'exists')
				{
					forReturnElement = $('input.user-email');
					forReturnTrigger = true;
				}
			}, 'POST', false);

			if(forReturnTrigger)
			{
				forReturn(forReturnTrigger, forReturnElement, 'Email is busy');
				return;
			}

			if($('input.password').val().length < 8)
			{
				forReturnElement = $('input.password');
				forReturnTrigger = true;
			}

			if(forReturnTrigger)
			{
				forReturn(forReturnTrigger, forReturnElement, 'Password is too short');
				return;
			}

			if($('input.password').val() != $('input.password-again').val())
			{
				forReturnElement = $('input.password');
				forReturnTrigger = true;
			}

			if(forReturnTrigger)
			{
				forReturn(forReturnTrigger, forReturnElement, 'Passwords is not equal');
				return;
			}

			ajaxWrap({
				action: 'register_new_user',
				name: $('input.f-name').val(),
				sec_name: $('input.s-name').val(),
				email: $('input.user-email').val(),
				password: $('input.password').val()
			}, function(data){
				$('.input-block').addClass('done');
				setTimeout(function(){
					$('.input-block').addClass('hidden');

					$('.register-complete').removeClass('hidden');
					setTimeout(function(){
						$('.register-complete').addClass('active');
					}, 100);
				}, 350);
			});

			break;
		}
	}
});

$('.update-button').click(function(){
	hideRequired();
	
	var forReturn = function(option, element, message)
	{
		if(option)
		{
			console.log(message);
			setRequired(element, message);
		}
	};

	switch(getParameterByName('page')) {
		case 'account-settings': {
			var forReturnTrigger = false,
			forReturnElement = null;

			$('.input-check').each(function(){
				if($(this).val() == '')
				{
					forReturnElement = $(this);
					forReturnTrigger = true;
					return false;
				}
			});


			if(!email_regexp.test($('input.user-email').val()))
			{
				forReturnElement = $('input.user-email');
				forReturnTrigger = true;
			}

			if(forReturnTrigger)
			{
				forReturn(forReturnTrigger, forReturnElement, 'Email is not valid');
				return;
			}

			if($('input.password').val().length < 8 && $('input.password').val().length != '')
			{
				forReturnElement = $('input.password');
				forReturnTrigger = true;
			}

			if(forReturnTrigger)
			{
				forReturn(forReturnTrigger, forReturnElement, 'Password is too short');
				return;
			}

			if($('input.password').val() != $('input.password-again').val())
			{
				forReturnElement = $('input.password');
				forReturnTrigger = true;
			}

			if(forReturnTrigger)
			{
				forReturn(forReturnTrigger, forReturnElement, 'Passwords is not equal');
				return;
			}

		ajaxWrap({
			action: 'account-settings-update',
			id: $('.hidden-id').val(),
			name: $('.f-name').val(),
			sec_name: $('.s-name').val(),
			email: $('.user-email').val(),
			password: $('.password').val()
			}, function(data){
				$('.update-button').text('Done!');
				console.log(data);
			}, 'POST', false);

		break;
		}
	}
});

$('.logout-button').click(function(){
	ajaxWrap({
		action: 'logout'
	}, function(data){
		location.reload();
	});
});

$('.sent-forgot-email').click(function(){
	$('.sent-forgot-email i').removeClass('hidden');
	ajaxWrap({
		action: 'email_exists',
		email: $('.forgot-password-email').val()
	}, function(data){
		if(data == 'exists')
		{
			console.log('mail sent');
			ajaxWrap({
				action: 'send_forgot_email',
				email: $('.forgot-password-email').val()
			}, function(data){
				$('.forgot-completed').removeClass('hidden');
				$('.forgot-email-input-box').addClass('hidden');
			});
		}
		else if(data == 'success')
		{
			$('.forgot-completed').removeClass('hidden');
			$('.forgot-email-input-box').addClass('hidden');
			$('.forgot-password-email').addClass('wrong-input');
			$('.sent-forgot-email i').addClass('hidden');
		}
	});
});

$('.change-forgoten-password').click(function(){
	$('input.new-password-again').removeClass('wrong-input');
	$('input.new-password').removeClass('wrong-input');


	if($('input.new-password').val() == '')
	{
		$('input.new-password').addClass('wrong-input');
		return;
	}

	if($('input.new-password-again').val() == '')
	{
		$('input.new-password-again').addClass('wrong-input');
		return;
	}

	if($('input.new-password').val().length < 7 || $('input.new-password-again').val().length < 7)
	{
		$('input.new-password-again').addClass('wrong-input');
		$('input.new-password').addClass('wrong-input');
		return;
	}

	if($('input.new-password').val() != $('input.new-password-again').val())
	{
		$('input.new-password-again').addClass('wrong-input');
		$('input.new-password').addClass('wrong-input');
		return;
	}

	ajaxWrap({
		action: 'update_password',
		token: getParameterByName('token'),
		password: $('input.new-password-again').val()
	}, function(data){


	});
});