<?php
	require_once 'config.php';

	function days($days)
	{
		$type = gettype($days);

		if($type == 'integer' || $type == 'double' || $type == 'float')
			return 86400 * $days;
		else
			return falase;
	}

	//Updates required
	$_pages_array_ = array(
		'sim-progress',
		'overview',
		'take-a-sim',
		'video-feedback',
		'tables-feedback',
		'terms-of-use',
		'charts-feedback',
		'score-feedback',
		'account-settings',
		'login',
		'register',
		'faqs'
	);

	if(isset($_POST['action']))
	{
		switch($_POST['action'])
		{
			case 'login':
			{
				$pass = hash('sha256', $_POST['password']);
				$user = $sqlc->query("SELECT id, email, password FROM users WHERE email = '{$_POST['email']}' AND password = '{$pass}'");

				session_start();

				if($user->num_rows > 0)
				{
					session_destroy();
					session_unset();
					session_write_close();

					session_start();
					session_regenerate_id(true);
					$user_hash = hash('sha256', $_SERVER['HTTP_USER_AGENT']);
					$_SESSION['user_hash'] = $user_hash;
					$_SESSION['user_id'] = $user->fetch_assoc()['id'];

					unset($_COOKIE['user_hash']);
					unset($_COOKIE['user_ss_id']);

					if($_POST['remember'] == 'true')
					{
						setcookie('user_hash', $user_hash, time() + days(10));
						setcookie('user_ss_id', session_id(), time() + days(10));
					}
					
					echo 'success';
				}
				else
					echo 'error';

				break;
			}
			case 'email_exists':
			{
				$email = $sqlc->query("SELECT email FROM users WHERE email = '{$_POST['email']}'");
				$token_email = $sqlc->query("SELECT email FROM register_tokens WHERE email = '{$_POST['email']}'");
				if($email->num_rows == 0 && $token_email->num_rows == 0)
					echo 'success';
				else
					echo 'exists';

				break;
			}
			case 'register_new_user':
			{
				$email = $sqlc->query("SELECT email FROM users WHERE email = '{$_POST['email']}'");

				if($email->num_rows > 0)
				{
					break;
				}

				$pass = hash('sha256', $_POST['password']);
				$token = bin2hex(random_bytes(10));
				$expire = time() + days(4);

				$sqlc->query("INSERT INTO register_tokens (name, sec_name, email, password, register_token, expire) VALUES(
					'{$_POST['name']}',
					'{$_POST['sec_name']}',
					'{$_POST['email']}',
					'{$pass}',
					'{$token}',
					'{$expire}'
				)");

				$message = '<html><body>';
				$message .= '<p style="font-size: 18px;">Hello ' . $_POST['name'] . ', thank you for registration on our platform.<br>To сonfirm your account, please visit <a href="http://' . DOMAIN . 
				'/test.php?page=confirm-account&token=' . $token . '">this link.</a></p>';
				$message .= "</body></html>";
				
				$to = strip_tags($_POST['email']);
				$subject = 'Confirm your JavelinSim account';
				
				$headers = "From: JavelinSim@mail.com\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				mail($to, $subject, $message, $headers);

				break;
			}
			case 'logout':
			{
				session_start();
				session_destroy();
				session_unset();
				session_write_close();
				setcookie('user_hash', null, -1);
				setcookie('user_ss_id', null, -1);

				break;
			}
			case 'save_ans_token':
			{
				$token_name = 'record_' . $_POST['token_index'] . '_token';
				$sqlc->query("UPDATE answers SET $token_name = '{$_POST['token']}' WHERE id = '{$_POST['answer']}'");

				$invite = $sqlc->query("SELECT * FROM user_tests WHERE id = '{$_POST['invite']}'")->fetch_assoc();
				$questions_count = $sqlc->query("SELECT ordering 
					FROM questions 
					WHERE test_id = '{$invite['test_id']}' 
					ORDER BY ordering DESC 
					LIMIT 1")->fetch_assoc()['ordering'];

				$question = $sqlc->query("SELECT * 
					FROM questions 
					WHERE test_id = '{$invite['test_id']}'
					AND ordering = '{$invite['progress']}'")->fetch_assoc();

				if(intval($question['answer_slides']) == intval($_POST['token_index']))
				{
					$sqlc->query("UPDATE answers SET state = 'finished' WHERE id = '{$_POST['answer']}'");

					if(intval($invite['progress']) < intval($questions_count))
						$sqlc->query("UPDATE user_tests SET progress = progress+1 WHERE id = '{$_POST['invite']}'");
					else
						$sqlc->query("UPDATE user_tests SET status = 'finished' WHERE id = '{$_POST['invite']}'");

				}

				break;
			}

			case 'account-settings-update':
			{
				$pass = hash('sha256', $_POST['password']);
				
				$sqlc->query("UPDATE users SET 
					name = '{$_POST['name']}',
					sec_name = '{$_POST['sec_name']}',
					email = '{$_POST['email']}',
					password = '$pass'
				
					WHERE id = '{$_POST['id']}'");
				
				if(!$sqlc->error){
					echo 'success';
				}
				else
					echo 'error';
			}
			case 'send_forgot_email':
			{
				$token = rand(1000, 999999);

				echo strip_tags($_POST['email']);

				$user_id = $sqlc->query("SELECT id FROM users WHERE email = '{$_POST['email']}'")->fetch_assoc()['id'];

				$sqlc->query("INSERT INTO restore_tokens (user_id, token) VALUES('{$user_id}', '{$token}')");

				$message = '<html><body>';
				$message .= '<p style="font-size: 18px;">';
				$message .= 'Please follow this <a href="https://javelinsim.com?page=restore-password&token=' . $token . '">link</a> to restore your email.<br><br>';
				$message .= 'If you not requested password reset, immediately contact support by this email adress questions@javelinsim.com.<br><br>';
				$message .= 'Javelin Sim';
				$message .= "</p></body></html>";

				$to = strip_tags($_POST['email']);
				$subject = 'Restore password for JavelinSim';

				$headers = "From:  support@javelinsim.com\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				mail($to, $subject, $message, $headers);

				break;
			}
			case 'update_password':
			{
				$token = $sqlc->query("SELECT * FROM restore_tokens WHERE token = '{$_POST['token']}'")->fetch_assoc();
				break;
			}
		}
	}

	$sqlc->close();
?>