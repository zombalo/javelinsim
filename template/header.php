<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $page_title ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//assets-cdn.ziggeo.com/v1-r23/ziggeo.css"/>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
	<link rel="stylesheet" href="../js/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/cover.css">
	<link rel="stylesheet" href="../stylesheet/style.css">
	<link rel="stylesheet" type="text/css" href="../stylesheet/responsive.css">
</head>
<body>