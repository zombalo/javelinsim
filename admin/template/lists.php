<?php
	function tests_list()
	{
		global $sqlc, $__USER_ID__;
		$sql_res = $sqlc->query("SELECT * FROM tests ORDER BY ordering");
		foreach($sql_res as $sql_row)
		{
			//Date formatting
			$time_buffer = strtotime($sql_row['created']);
			$created = date('m.d.Y', $time_buffer);

			//Questions count
			$question_count = $sqlc->query("SELECT id FROM questions WHERE test_id = '{$sql_row['id']}'")->num_rows;

			//Slides count part
			$slide_count = 0;
			$questions = $sqlc->query("SELECT rerecord, strong_require, rev_of_ans, ans_af_rev, ans_af_str 
				FROM questions
				WHERE test_id = '{$sql_row['id']}'");

			foreach($questions as $question)
			{
				$slide_count++;
				if($question['rerecord'] != 'None')
				{
					$slide_count++;
				}
				unset($question['rerecord']);

				$question = array_values($question);
				for($i = 0; $i < count($question); $i++)
				{
					if($question[$i] != '')
					{
						$slide_count++;
					}
				}
			}

			//Completed count
			$completed_count = 0;
			$users = $sqlc->query("SELECT DISTINCT user_id AS id 
				FROM answers 
				WHERE test_id = '{$sql_row['id']}'");

			foreach($users as $user)
			{
				$answers_count = $sqlc->query("SELECT id 
					FROM answers 
					WHERE test_id = '{$sql_row['id']}'
					AND user_id = '{$user['id']}'")->num_rows;

				if($answers_count == $question_count)
					$completed_count++;
			}
		?>
		<div class="list-item">
			<!-- <div test-id="<?php //echo $sql_row['id'] ?>"  class="invite-button">Invite</div> -->
			<div test-id="<?php echo $sql_row['id'] ?>" class="copy-block"></div>
			<div test-id="<?php echo $sql_row['id'] ?>" class="order-block clearfix">
				<div class="to-bottom"></div>
				<div class="to-top"></div>
			</div>
			<a href="?page=test&id=<?php echo $sql_row['id'] ?>" class="fa-item">
				<div class="row">
					<div class="col-sm-4 list-col"><?php echo $sql_row['name'] ?></div>
					<div class="col-sm-2 list-col"><?php echo $created ?></div>
					<div class="col-sm-1 list-col"><?php echo $sql_row['takers'] ?></div>
					<div class="col-sm-1 list-col"><?php echo $sql_row['invited'] ?></div>
					<div class="col-sm-1 list-col"><?php echo $completed_count ?></div>
					<div class="col-sm-1 list-col"><?php echo $question_count ?></div>
					<div class="col-sm-2 list-col"><?php echo $slide_count ?></div>
				</div>
			</a>
		</div>
		<?php
		}
	}

	function questions_list()
	{
		global $sqlc, $__USER_ID__;
		$sql_list = $sqlc->query("SELECT title, id FROM questions WHERE test_id = '{$_GET['id']}' ORDER BY ordering");
		foreach($sql_list as $sql_item)
		{ ?>
		<div class="list-item">
			<div test-id="<?php echo $sql_item['id'] ?>" class="copy-block"></div>
			<div test-id="<?php echo $sql_item['id'] ?>" class="order-block clearfix">
				<div class="to-bottom"></div>
				<div class="to-top"></div>
			</div>
			<a href="?page=question&tid=<?php echo $_GET['id'] ?>&id=<?php echo $sql_item['id'] ?>" class="fa-item">
				<div class="row">
					<div class="col-sm-12 list-col"><?php echo $sql_item['title'] ?></div>
				</div>
			</a>
		</div>
		<?php
		}
	}

	function answers_list()
	{
		global $sqlc, $__USER_ID__;
		$sql_res = $sqlc->query("SELECT * FROM tests ORDER BY ordering");
		foreach($sql_res as $sql_row)
		{
			//Time calculation
			$time_buffer = strtotime($sql_row['created']);
			$created = date('m.d.Y', $time_buffer);

			//"Completed", is meant quantity of users, that completed all question from this test
			//(I wrote this because algorithm can be changed in future for counting another param)
			$completed_count = 0;
			$question_count = $sqlc->query("SELECT id FROM questions WHERE test_id = '{$sql_row['id']}'")->num_rows;
			$users = $sqlc->query("SELECT DISTINCT user_id FROM answers WHERE test_id = '{$sql_row['id']}'");
			
			foreach($users as $user)
			{
				$user_completed_count = $sqlc->query("SELECT id 
					FROM answers 
					WHERE user_id = '{$user['user_id']}' 
					AND test_id = '{$sql_row['id']}'")->num_rows;

				if($user_completed_count == $question_count)
				{
					$completed_count++;
				}
			}

			//Scored users quantity
			$scored_count = mysqli_fetch_all($sqlc->query("SELECT DISTINCT user_id 
				FROM user_scores 
				WHERE test_id = '{$sql_row['id']}'"));

			$scored_text_count = mysqli_fetch_all($sqlc->query("SELECT DISTINCT user_id 
				FROM text_scores 
				WHERE test_id = '{$sql_row['id']}'"));

			for($i = 0; $i < count($scored_count); $i++)
			{
				for($j = 0; $j < count($scored_text_count); $j++)
				{
					if($scored_count[$i] == $scored_text_count[$j])
					{
						unset($scored_count[$i]);
					}
				}
			}

			$users_count = count(array_merge($scored_count, $scored_text_count));

			//Unscored user quantity

			$unscored_count = $sqlc->query("SELECT DISTINCT user_id 
				FROM answers 
				WHERE test_id = '{$sql_row['id']}'")->num_rows;

			$unscored_count -= $users_count;
			if($unscored_count < 0)
				$unscored_count = 0;

			//Screen scored percent
			$questions = $sqlc->query("SELECT id, ans_af_rev, ans_af_str, rerecord FROM questions WHERE test_id = '{$sql_row['id']}'");
			$slides_count = 0;
			$question_slides = array();
			$answered_users = $sqlc->query("SELECT DISTINCT user_id 
				FROM answers 
				WHERE test_id = '{$sql_row['id']}'");

			foreach($questions as $question)
			{
				$inner_buffer = 0;
				if($question['rerecord'] == 'None')
				{
					continue;
				}

				$inner_buffer++;
				$slides_count++;

				if($question['ans_af_rev'] == 'checked')
				{
					$inner_buffer++;
					$slides_count++;
				}

				if($question['ans_af_str'] == 'checked')
				{
					$inner_buffer++;
					$slides_count++;
				}

				array_push($question_slides, ['id' => $question['id'], 'slides' => $inner_buffer]);
			}

			$slides_count *= $answered_users->num_rows;
			$scored_slides = 0;

			foreach($answered_users as $answered_user)
			{
				$point_scores = mysqli_fetch_all($sqlc->query("SELECT DISTINCT test_id, question_id, response
					FROM user_scores 
					WHERE test_id = '{$sql_row['id']}'
					AND user_id = '{$answered_user['user_id']}'"));

				$text_scores = mysqli_fetch_all($sqlc->query("SELECT DISTINCT test_id, question_id, response
					FROM text_scores 
					WHERE test_id = '{$sql_row['id']}'
					AND user_id = '{$answered_user['user_id']}'"));

				$scored_slides += count(array_unique(array_merge($point_scores, $text_scores), SORT_REGULAR));
			}

			if($slides_count == 0)
				$scored_percent = 0;
			else
			{
				$scored_percent = intval($scored_slides / $slides_count * 100);
			}

			if(is_nan($scored_percent) || is_infinite($scored_percent))
				$scored_percent = 0;

			?>
			<div class="list-item">
				<a href="?page=persons&id=<?php echo $sql_row['id'] ?>" class="fa-item">
					<div class="row">
						<div class="col-sm-3 list-col"><?php echo $sql_row['name'] ?></div>
						<div class="col-sm-2 list-col"><?php echo $created ?></div>
						<div class="col-sm-1 list-col"><?php echo $sql_row['takers'] ?></div>
						<div class="col-sm-1 list-col tip-col"><?php echo $sql_row['invited'] ?>
							<div link="index.php?page=users&filter=true&type=0&test-id=<?php echo $sql_row['id'] ?>" class="link-col">Jump to...</div>
						</div>
						<div class="col-sm-1 list-col tip-col"><?php echo $completed_count ?>
							<div link="index.php?page=users&filter=true&type=1&test-id=<?php echo $sql_row['id'] ?>" class="link-col">Jump to...</div>
						</div>
						<div class="col-sm-1 list-col tip-col"><?php echo $users_count ?>
							<div link="index.php?page=users&filter=true&type=2&test-id=<?php echo $sql_row['id'] ?>" class="link-col">Jump to...</div>
						</div>
						<div class="col-sm-1 list-col"><?php echo $unscored_count ?></div>
						<div class="col-sm-2 list-col"><?php echo $scored_percent . '%' ?></div>
					</div>
				</a>
			</div>
			<?php
		}
	}

	function answered_persons_list()
	{
		global $sqlc, $__USER_ID__;
		$question_count = $sqlc->query("SELECT id FROM questions WHERE test_id = '{$_GET['id']}'")->num_rows;
		$sql_res = $sqlc->query("SELECT DISTINCT user_id FROM answers WHERE test_id = '{$_GET['id']}'");
		$question_id = mysqli_fetch_assoc($sqlc->query("SELECT id FROM questions WHERE test_id = '{$_GET['id']}' ORDER BY id LIMIT 1"))['id'];
		foreach($sql_res as $sql_row)
		{
			$user_row = mysqli_fetch_assoc($sqlc->query("SELECT * FROM users WHERE id = '{$sql_row['user_id']}'"));

			$time_buffer = strtotime($user_row['since']);
			$created = date('m.d.Y', $time_buffer);

			$get_of = $sqlc->query("SELECT id FROM answers WHERE state = 'finished' AND test_id = '{$_GET['id']}' AND user_id = '{$sql_row['user_id']}'")->num_rows;
		?>
		<div class="list-item">
			<a href="?page=rating&test-id=<?php echo $_GET['id'] ?>&user-id=<?php echo $sql_row['user_id'] ?>&question-id=<?php echo $question_id ?>" class="fa-item">
				<div class="row">
					<div class="col-sm-3 list-col"><?php echo $user_row['name'] ?> <?php echo $user_row['sec_name'] ?></div>
					<div class="col-sm-3 list-col"><?php echo $user_row['email'] ?></div>
					<div class="col-sm-3 list-col"><?php echo $created ?></div>
					<div class="col-sm-1 list-col"><?php echo $user_row['tested'] ?></div>
					<div class="col-sm-2 list-col"><?php echo $get_of ?> of <?php echo $question_count ?></div>
				</div>
			</a>
		</div>
		<?php
		}
	}

	function tokens_list()
	{
		global $sqlc, $__USER_ID__;
		$tokens = $sqlc->query("SELECT * FROM invite_tokens ORDER BY id DESC");
		foreach($tokens as $token)
		{
		?>
			<div token-value="<?php echo $token['token'] ?>" token-id="<?php echo $token['id'] ?>" class="token-list-item clearfix">
				<div class="token-value"><?php echo DOMAIN . '/?action=register&token=' . $token['token'] ?></div>
				<div class="remove-token-button pull-right"></div>
				<div class="downwrapper choose-link-test pull-right">
					<div class="dropdown clearfix">
					<?php
					$current_test = mysqli_fetch_assoc($sqlc->query("SELECT id, name FROM tests WHERE id = '{$token['test_id']}'"));
					?>
						<div value="<?php echo $current_test['id'] ?>" class="text"><?php echo $current_test['name'] ?></div>
						<div class="c-caret">
							<img src="img/caret.png">
						</div>
						<div class="items">
							<?php
							$tests = $sqlc->query("SELECT id, name FROM tests WHERE id IN (SELECT test_id FROM questions) ORDER BY ordering");
							foreach($tests as $test)
							{
								?>
								<div value="<?php echo $test['id'] ?>" class="item test-choose token-link-test-choose"><?php echo $test['name'] ?></div>
								<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
	}

	function rules_list()
	{
		global $sqlc, $__USER_ID__;
		$users = $sqlc->query("SELECT * FROM admin_users WHERE id <> '{$__USER_ID__}'");
		foreach($users as $user)
		{
			$user_mime = mysqli_fetch_assoc($sqlc->query("SELECT title 
				FROM admin_user_mime_types 
				WHERE id = '{$user['user_mime_type_id']}'"))['title'];

			if($user['active'] == '1')
				$user_active = 'yes';
			else
				$user_active = 'no';
		?>
		<div class="list-item" user-id="<?php echo $user['id'] ?>">
			<div class="users-action-block trs-none" style="display: none;">
				<div class="assign-checkbox-block">
					<input disabled type="checkbox" id="checkbox-<?php echo $user['id'] ?>" class="user-list-checkbox">
					<label for="checkbox-<?php echo $user['id'] ?>"></label>
				</div>
			</div>
			<a href="index.php?page=admin-user&id=<?php echo $user['id'] ?>" class="fa-item">
				<div class="row">
					<div class="col-sm-5 list-col"><?php echo $user['email'] ?></div>
					<div class="col-sm-5 list-col type-col"><?php echo $user_mime ?></div>
					<div class="col-sm-2 list-col active-col"><?php echo $user_active ?></div>
				</div>
			</a>
		</div>
		<?php
		}
	}
?>