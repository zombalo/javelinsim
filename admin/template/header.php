<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php
	if(isset($_GET['page']))
	{
		if($_GET['page'] == 'confirm-account')
		{
			$page_title = 'Confirm account';
		}
		else
		{
			$page_title = mysqli_fetch_assoc($sqlc->query("SELECT title 
				FROM admin_pages 
				WHERE link = '{$_GET['page']}'"))['title'];
		}
	}
	else if(isset($_COOKIE['admin_auth_token']) && isset($_COOKIE['admin_hash']))
	{
		$page_title = 'Main';
	}
	else
	{
		$page_title = 'Login';
	}
	?>
	<title><?php echo $page_title ?> - Admin</title>
	<link rel="stylesheet" href="../../stylesheet/font-awesome-4.7.0/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//assets-cdn.ziggeo.com/v1-stable/ziggeo.css" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
	<link rel="stylesheet" href="js/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="stylesheet/style.css">
	<link rel="stylesheet" type="text/css" href="stylesheet/responsive.css">
	<link rel="stylesheet" type="text/less" href="stylesheet/style.less">
</head>
<body>