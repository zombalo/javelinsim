<?php
	require_once 'config.php';
	require_once 'template/header.php';
	require_once 'template/lists.php';

	function display_calendar($class = '', $day = -1, $month = -1, $year = -1)
	{
		$months = array(
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		);

		if($day == -1)
		{
			$day = date('d', time());
			$month = date('n', time());
			$year = date('Y', time());
		}

		?>
		<div value="<?php echo $month ?>" class="calendar-widget clearfix float-left <?php echo $class ?>">
			<input type="text" class="day" value="<?php echo $day ?>">
			<div class="months clearfix">
				<div class="text"><?php echo $months[intval($month) - 1] ?></div>
				<div class="drop-caret"><i class="fa fa-chevron-down"></i></div>
				<div class="options">
					<div value="1" class="option">January</div>
					<div value="2" class="option">February</div>
					<div value="3" class="option">March</div>
					<div value="4" class="option">April</div>
					<div value="5" class="option">May</div>
					<div value="6" class="option">June</div>
					<div value="7" class="option">July</div>
					<div value="8" class="option">August</div>
					<div value="9" class="option">September</div>
					<div value="10" class="option">October</div>
					<div value="11" class="option">November</div>
					<div value="12" class="option">December</div>
				</div>
			</div>
			<input type="text" class="year" value="<?php echo $year ?>">
		</div>
		<?php
	}

	if(isset($_GET['page']) && $_GET['page'] == 'confirm-account')
	{
		setcookie('admin_hash', '', time() - 100);
		setcookie('admin_auth_token', '', time() - 100);
		$__AUTH_TRIGGER = false;
	}

	?>
	<noscript>
		<div class="not-found-placeholder">For properly work of website you should enable JavaScript</div>
		<style type="text/css">
			.blur-blocker,
			.blur-blocker 
			{
				display: none;
			}
		</style>
	</noscript>
	<?php
	if($__AUTH_TRIGGER == true)
	{
		function buildPagesList($page_id, $for_user = false, $user = 0)
		{
			global $sqlc;

			$pages = $sqlc->query("SELECT * FROM admin_pages WHERE id = '{$page_id}'");
			foreach($pages as $page)
			{
				if($for_user)
				{
					$user_rule = mysqli_fetch_assoc($sqlc->query("SELECT visibility, type 
						FROM admin_user_rules 
						WHERE user_id = '{$user}' 
						AND page_id = '{$page['id']}'"));
				}
				?>
				<div class="page-rule-block" page-id="<?php echo $page['id'] ?>">
					<div class="page-rule-controls clearfix">
						<div class="rule-caret float-left"></div>
						<div class="page-title float-left">Page - <?php echo $page['title'] ?></div>
						<div status="<?php if($for_user){if($user_rule['visibility']=='0'){echo 'checked';}} ?>" class="page-visible-trigger trigger-widget float-left<?php if($for_user){if($user_rule['visibility']=='0'){echo ' trigger-widget-active';}} ?>">
							<div class="description">Page visible?</div>
							<div class="trigger-area">
								<div class="circle"></div>
							</div>
						</div>
						<div class="report-select-block select-rule-dropdown <?php if($for_user){if($user_rule['visibility']=='0'){echo ' blur-wrapper';}} ?>">
							<div class="disabled <?php if($for_user) {if($user_rule['visibility']=='1') {echo 'hidden';} } else {echo 'hidden';} ?>"></div>
							<div class="select-desc">Choose user abilities</div>
							<div class="downwrapper">
								<div class="dropdown clearfix">
									<div value="<?php if($for_user){echo $user_rule['type'];} else{echo 'read_only';} ?>" class="text"><?php if($for_user){if($user_rule['type'] == 'read_write'){echo 'View and Change';} else {echo 'View';}} else {echo 'View';} ?></div>
									<div class="c-caret">
										<img src="img/caret.png">
									</div>
									<div class="items">
										<div value="read_only" class="item test-choose">View</div>
										<div value="read_write" class="item test-choose">View and Change</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="page-rule-content <?php if($for_user){if($user_rule['visibility']=='0'){echo ' blur-wrapper';}} ?>">
						<div class="disabled <?php if($for_user) {if($user_rule['visibility']=='1') {echo 'hidden';} } else {echo 'hidden';} ?>"></div>
					<?php
					$sub_pages = $sqlc->query("SELECT * FROM admin_pages WHERE parent_page_id = '{$page['id']}'");
					if($sub_pages->num_rows > 0)
					{
						foreach($sub_pages as $sub_page)
						{
							if($for_user)
								buildPagesList($sub_page['id'], true, $user);
							else
								buildPagesList($sub_page['id']);
						}
					}
					?>
					</div>
				</div>
				<?php
			}
		}

	?>
	<div class="create-user-popup">
		<div class="title">Create user</div>
		<div class="content clearfix">
			<div class="input-block">
				<div class="title">First name</div>
				<input class="f-name" type="text">
			</div>
			<div class="input-block">
				<div class="title">Last name</div>
				<input class="l-name" type="text">
			</div>
			<div class="input-block">
				<div class="title">Email</div>
				<input class="email" type="text">
			</div>
			<div class="input-block">
				<div class="title">Password</div>
				<input class="password" type="text">
			</div>
			<div class="checkbox-block">
				<input <?php echo $sql_res['scoring_list_mode'] ?> type="checkbox" name="" id="checkbox-create-user" class="create-user-assign-sim">
				<label for="checkbox-create-user">Invite user to simulation</label>
			</div>
			<div class="assign-sim-user-create">
				<div class="report-select-block">
					<div class="select-desc">Assign simulation for users</div>
					<div class="downwrapper choose-user-test-create-user">
						<div class="dropdown clearfix">
						<?php
						$first_test = mysqli_fetch_assoc($sqlc->query("SELECT id, name
							FROM tests 
							WHERE id IN (
							SELECT test_id FROM questions
							) ORDER BY ordering LIMIT 1"));
						?>
							<div value="<?php echo $first_test['id'] ?>" class="text"><?php echo $first_test['name'] ?></div>
							<div class="c-caret">
								<img src="img/caret.png">
							</div>
							<div class="items">
							<?php
							$tests = $sqlc->query("SELECT id, name FROM tests WHERE id IN (SELECT test_id FROM questions) ORDER BY ordering");
							foreach($tests as $test)
							{
							?>
								<div value="<?php echo $test['id'] ?>" class="item test-choose current-user-test-choose"><?php echo $test['name'] ?></div>
							<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php
				$time_offset = time() + 604800;
				$day = date('d', $time_offset);
				$month = date('n', $time_offset);
				$year = date('Y', $time_offset);
				display_calendar('create-user-assign-test-calendar', $day, $month, $year);
				?>
			</div>
		</div>
		<div class="buttons-block">
			<div class="button success">Save</div>
			<div send-email="true" class="button success">Save &amp; Email</div>
			<div class="button reject">Cancel</div>
		</div>
	</div>
	<div class="modal-block">
		<div class="title"></div>
		<div class="content"></div>
		<div class="buttons-block">
			<div class="button success">Ok</div>
			<div class="button reject">Cancel</div>
		</div>
	</div>
	<!-- Score details -->
	<div class="score-details-wrap hidden clearfix">
		<div class="current-score-id hidden"></div>
		<div ordering="" item-id="new" class="detail-list-item-proto h0-state hidden clearfix">
			<div class="buttons-wrap">
				<div class="list-item-control up-button"></div>
				<div class="list-item-control remove-button"></div>
				<div class="list-item-control down-button"></div>
			</div>
			<textarea placeholder="Score detail title" class="title-area"></textarea>
			<textarea placeholder="Score detail description" class="desc-area"></textarea>
		</div>
		<div class="back-space-button"></div>
		<div class="score-details">
			<div class="top-controls border-controls">
				<div class="button top-button save-score-details-button">Save</div>
				<div class="button top-button add-detail-item-button">Add one</div>
			</div>
			<!-- <div class="score-details-loading-block hidden">
				<div class="loading"></div>
			</div> -->
			<div class="score-details-content">
				<div class="list-headers clearfix">
					<div class="head-title">Detail description</div>
					<div class="head-title">Detail title</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Score details end -->
	<div class="main-notify clearfix">
			<img src="img/checkmark.png" alt="">
			<span>Action complete</span>
	</div>
	<div class="blur-blocker"> <!-- Wraps whole page -->
		<div class="page-blocker"></div>
		<!-- <div class="loading-hidden">
			<div class="loader"></div>
		</div> -->
		<div class="container-fluid">
			<div class="row core-row">
				<div class="left-bar">
					<div class="menu-icon clearfix">
					</div>
					<a href="index.php" class="bar-icon-1 bar-item fa-item">Main</a>
					<?php
					$pages_query = "SELECT link, title FROM admin_pages WHERE id IN (
						SELECT page_id FROM admin_user_rules WHERE user_id = '{$__USER_ID__}' AND visibility = '1'
					) AND parent_page_id = '0'
					ORDER BY ordering";

					$pages = mysqli_fetch_all($sqlc->query($pages_query), MYSQLI_ASSOC);

					for($i = 0; $i < count($pages); $i++)
					{
					?>
						<a href="index.php?page=<?php echo $pages[$i]['link'] ?>" class="bar-icon-<?php echo $i+2 ?> bar-item fa-item"><?php echo $pages[$i]['title'] ?></a>
					<?php } ?>
				</div>
				<div class="icon-wrap">
					<div class="icon-row"></div>
					<div class="icon-row"></div>
					<div class="icon-row"></div>
				</div>
				<div class="col-sm-12">
					<div class="admin-content">
					<?php
						$user_active = mysqli_fetch_assoc($sqlc->query("SELECT active FROM admin_users WHERE id = '{$__USER_ID__}'"))['active'];

						if(isset($_GET['page']))
						{
							$page_id = mysqli_fetch_assoc($sqlc->query("SELECT id FROM admin_pages WHERE link = '{$_GET['page']}'"))['id'];
							$visibility_rule = mysqli_fetch_assoc($sqlc->query("SELECT visibility FROM admin_user_rules WHERE page_id = '{$page_id}' AND user_id = '{$__USER_ID__}'"));

							if($visibility_rule['visibility'] == '0')
							{
								header('Location: permissions.php?message=You has no permissions to visit this page or perform this action.');
								exit();
							}

							if($user_active == '0')
							{
								header('Location: permissions.php?message=Your accoutn not active right now.');
								exit();
							}

							switch($_GET['page'])
							{
								case 'answers':
								{
									//Here goes list of tests
									?>
									<div class="list-header">
										<div class="row">
											<div class="col-sm-3 header-col">Simulation</div>
											<div class="col-sm-2 header-col">Created</div>
											<div class="col-sm-1 header-col">Takers</div>
											<div class="col-sm-1 header-col">Invited</div>
											<div class="col-sm-1 header-col">Completed</div>
											<div class="col-sm-1 header-col">Scored</div>
											<div class="col-sm-1 header-col">Not scored</div>
											<div class="col-sm-2 header-col">Percent screens scored</div>
										</div>
									</div>
									<div class="page-list">
									<?php
									answers_list();
									?>
									</div>
									<?php
									break;
								}
								case 'persons':
								{
									?>
									<div class="list-header">
										<div class="row">
											<div class="col-sm-3 header-col">Name</div>
											<div class="col-sm-3 header-col">Email</div>
											<div class="col-sm-3 header-col">Date</div>
											<div class="col-sm-1 header-col">Tested?</div>
											<div class="col-sm-2 header-col">Responses given</div>
										</div>
									</div>
									<?php
									$question_count = $sqlc->query("SELECT id FROM questions WHERE test_id = '{$_GET['id']}'")->num_rows;
									$sql_res = $sqlc->query("SELECT DISTINCT user_id FROM answers WHERE test_id = '{$_GET['id']}'");
									$question_id = mysqli_fetch_assoc($sqlc->query("SELECT id FROM questions WHERE test_id = '{$_GET['id']}' ORDER BY id LIMIT 1"))['id'];
									foreach($sql_res as $sql_row)
									{
										$user_row = mysqli_fetch_assoc($sqlc->query("SELECT * FROM users WHERE id = '{$sql_row['user_id']}'"));

										$time_buffer = strtotime($user_row['since']);
										$created = date('m.d.Y', $time_buffer);

										$get_of = $sqlc->query("SELECT id FROM answers WHERE state = 'finished' AND test_id = '{$_GET['id']}' AND user_id = '{$sql_row['user_id']}'")->num_rows;
									?>
									<div class="list-item">
										<a href="?page=rating&test-id=<?php echo $_GET['id'] ?>&user-id=<?php echo $sql_row['user_id'] ?>&question-id=<?php echo $question_id ?>" class="fa-item">
											<div class="row">
												<div class="col-sm-3 list-col"><?php echo $user_row['name'] ?> <?php echo $user_row['sec_name'] ?></div>
												<div class="col-sm-3 list-col"><?php echo $user_row['email'] ?></div>
												<div class="col-sm-3 list-col"><?php echo $created ?></div>
												<div class="col-sm-1 list-col"><?php echo $user_row['tested'] ?></div>
												<div class="col-sm-2 list-col"><?php echo $get_of ?> of <?php echo $question_count ?></div>
											</div>
										</a>
									</div>
									<?php
									}
									break;
								}
								case 'rating':
								{
									$prev_user = mysqli_fetch_assoc($sqlc->query("SELECT user_id 
										FROM answers 
										WHERE  user_id < '{$_GET['user-id']}' 
										AND test_id = '{$_GET['test-id']}' 
										ORDER BY user_id DESC 
										LIMIT 1"));
									$next_user = mysqli_fetch_assoc($sqlc->query("SELECT 
										user_id 
										FROM answers 
										WHERE  user_id > '{$_GET['user-id']}' 
										AND test_id = '{$_GET['test-id']}' 
										ORDER BY user_id 
										LIMIT 1"));

									if($prev_user == NULL)
									{
										$prev_user = $_GET['user-id'];
									}
									else
									{
										$prev_user = $prev_user['user_id'];
									}

									if($next_user == NULL)
									{
										$next_user = $_GET['user-id'];
									}
									else
									{
										$next_user = $next_user['user_id'];
									}

									$current_test = mysqli_fetch_assoc($sqlc->query("SELECT name FROM tests WHERE id = '{$_GET['test-id']}'"))['name'];

									$rest_tests = $sqlc->query("SELECT id, name 
										FROM tests 
										WHERE id <> '{$_GET['test-id']}' 
										ORDER BY ordering");

									?>
									<div class="content-container non-controls">
										<div class="row">
											<div class="col-sm-12">
												<div class="rating-score-bar clearfix">
													<div class="select-test-title">Current simulation</div>
													<div class="test-selector">
														<div class="current-test"><?php echo $current_test; ?></div>
														<div class="tests-list">
														<?php
														foreach($rest_tests as $rest_test)
														{
															$link_question;
															$link_user;
															$scored_trigger = true;
															$questions = $sqlc->query("SELECT id 
																FROM questions 
																WHERE test_id = '{$rest_test['id']}' 
																ORDER BY ordering");

															foreach($questions as $question)
															{
																$link_question = $question['id'];

																$users = $sqlc->query("SELECT user_id as id 
																	FROM answers 
																	WHERE question_id = '{$question['id']}' 
																	ORDER BY user_id");

																foreach($users as $user)
																{
																	if($sqlc->query("SELECT id 
																		FROM user_scores
																		WHERE question_id = '{$question['id']}'
																		AND user_id = '{$user['id']}'")->num_rows > 0 ||
																		$sqlc->query("SELECT id 
																		FROM text_scores
																		WHERE question_id = '{$question['id']}'
																		AND user_id = '{$user['id']}'")->num_rows > 0)
																	{
																		$scored_trigger = true;
																		continue;
																	}
																	else
																	{
																		$scored_trigger = false;
																		$link_user = $user['id'];
																		break 2;
																	}
																}
															}

															if($scored_trigger == true)
															{
																continue;
															}

														?>
															<a href="index.php?page=rating&test-id=<?php echo $rest_test['id'] ?>&user-id=<?php echo $link_user ?>&question-id=<?php echo $link_question ?>" class="fa-item"><div class="item"><?php echo $rest_test['name'] ?></div></a>
														<?php } ?>
														</div>
													</div>
													<div class="current-user-bar clearfix">
													<?php
													$user_name = mysqli_fetch_assoc($sqlc->query("SELECT name, sec_name, email FROM users WHERE id = '{$_GET['user-id']}'"));
													?>
														<div class="user-name"><?php echo $user_name['name'] . ' ' . $user_name['sec_name'] ?></div>
														<div class="user-email"><?php echo $user_name['email'] ?></div>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="questions-bar">
												<span class="score-header">Questions</span>
												<div class="under-row"></div>
												<?php
												$test_questions = $sqlc->query("SELECT id, title FROM questions WHERE test_id = '{$_GET['test-id']}' ORDER BY ordering");
												foreach($test_questions as $test_question)
												{
													?>
													<a href="?page=rating&test-id=<?php echo $_GET['test-id'] ?>&user-id=<?php echo $_GET['user-id'] ?>&question-id=<?php echo $test_question['id'] ?>" class="fa-item">
														<div class="test-question clearfix <?php 
														if($test_question['id'] == $_GET['question-id'])
															echo 'current-scoring-question';
														 ?>"><?php if($sqlc->query("SELECT id FROM user_scores WHERE question_id = '{$test_question['id']}' AND user_id = '{$_GET['user-id']}'")->num_rows > 0 || 
														 $sqlc->query("SELECT id FROM text_scores WHERE question_id = '{$test_question['id']}' AND user_id = '{$_GET['user-id']}'")->num_rows > 0) {?>
														<img class="scored-question-mark" src="img/checkmark-white.png">
													<?php } ?><?php echo $test_question['title'] ?></div>
													</a>
													<?php
												}
												?>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="center-answer">
													<div class="question-switch-block clearfix">
														<a href="?page=rating&test-id=<?php echo $_GET['test-id'] ?>&user-id=<?php echo $prev_user ?>&question-id=<?php echo $_GET['question-id'] ?>" class="fa-item">
															<div class="switch-question-button prev-question pull-left"><div class="button-row">&#8249;</div> Prev. user</div>
														</a>
														<a href="?page=rating&test-id=<?php echo $_GET['test-id'] ?>&user-id=<?php echo $next_user ?>&question-id=<?php echo $_GET['question-id'] ?>" class="fa-item">
															<div class="switch-question-button next-question pull-right">Next user  <div class="button-row">&#8250;</div></div>
														</a>
													</div>
													<div class="splitter"></div>
													<div class="inner-tabs scoring-tabs">
														<div class="tabs-button-container clearfix">
														<?php
														$answer_slides = mysqli_fetch_assoc($sqlc->query("SELECT answer_slides
															FROM questions
															WHERE id = '{$_GET['question-id']}'"))['answer_slides'];

														$titles = array('Answer', 'Review of answer', 'Answer after strong');
														$width = 100 / intval($answer_slides);

														for($i = 0; $i < intval($answer_slides); $i++)
														{
															?>
															<div style="width: <?php echo $width ?>%;" class="tab-button <?php if($i == 0) echo 'current-inner-tab-button' ?>"><?php echo $titles[$i] ?></div>
															<?php
														}
														?>
															<div class="tab-button-back"></div>
														</div>
														<div class="inner-content-tabs clearfix">
															<?php
															for($i = 0; $i < intval($answer_slides); $i++)
															{
																$query_buffer = 'record_' . strval($i + 1) . '_token';
																$slide_token = mysqli_fetch_assoc($sqlc->query("SELECT $query_buffer 
																	FROM answers 
																	WHERE test_id = '{$_GET['test-id']}' 
																	AND question_id = '{$_GET['question-id']}' 
																	AND user_id = '{$_GET['user-id']}'"));

															?>
															<div class="inner-tab-content <?php if($i != 0) { echo  'inner-hidden-tab inner-nodisplay'; } ?>">
																<div class="slide-player">
																<?php if($slide_token[$query_buffer] != '') { ?>
																	<ziggeoplayer
																	ziggeo-video="<?php echo $slide_token[$query_buffer] ?>"
																	ziggeo-responsive="true"
																	></ziggeoplayer>
																<?php } else { ?>
																	<div class="no-video-rating">No user response</div>
																<?php } ?>
																</div>
															</div>
															<?php
															}
															?>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="score-bar">
													<span class="score-header">Scoring checklist</span>
													<div class="under-row"></div>
														<div class="score-tabs">
														<?php
														$score_list_mode = mysqli_fetch_assoc($sqlc->query("SELECT scoring_list_mode 
															FROM questions 
															WHERE id = '{$_GET['question-id']}'"))['scoring_list_mode'];

														if($score_list_mode == 'checked')
														{
															$slides_count = '1';
															$single_list = '1';
														}
														else
														{
															$slides_count = mysqli_fetch_assoc($sqlc->query("SELECT answer_slides 
																FROM questions 
																WHERE id = '{$_GET['question-id']}'"))['answer_slides'];
															$single_list = '0';
														}

														$iterator = 1;
														$checked_scores = mysqli_fetch_all($sqlc->query("SELECT score_id 
																FROM user_scores 
																WHERE user_id = '{$_GET['user-id']}' 
																AND question_id = '{$_GET['question-id']}'"), MYSQLI_ASSOC);

														for($i = 0; $i < $slides_count; $i++)
														{
															$mode = '';
															$option = 'radio';
															$checked = '';
															$scores_iterator = $i + 1;
															
															$comment = mysqli_fetch_assoc($sqlc->query("SELECT rate 
																FROM text_scores 
																WHERE question_id = '{$_GET['question-id']}' 
																AND user_id = '{$_GET['user-id']}' 
																AND response = '{$scores_iterator}'"))['rate'];

															if($comment == NULL)
															{
																$comment = '';
															}
															
															$scores = $sqlc->query("SELECT * 
																FROM question_scores 
																WHERE question_id = '{$_GET['question-id']}' 
																AND response = '{$scores_iterator}' 
																AND single_list = '{$single_list}'
																ORDER BY ordering");

															$mode_col = 'scoring_mode_' . strval($scores_iterator);

															$mode_raw = mysqli_fetch_assoc($sqlc->query("SELECT {$mode_col} 
																FROM questions 
																WHERE id = '{$_GET['question-id']}'"))[$mode_col];

															if($mode_raw == '2')
															{
																$option = 'checkbox';
																$mode = 'checkbox-mode';
															}
															?>
															<div class="score-content-tab <?php if($i > 0) { echo 'inner-hidden-tab inner-nodisplay'; } ?>">
															<?php
															foreach($scores as $score)
															{
																for($j = 0; $j < count($checked_scores); $j++)
																{
																	if($checked_scores[$j]['score_id'] == $score['id'])
																	{
																		$checked = 'checked';
																		break;
																	}
																}

																?>
																<div class="score-list-item">
																	<input name="<?php echo intval($i) + 1 ; ?> "<?php echo $checked ?>  class="score-input <?php echo $mode ?>" value="<?php echo $score['id'] ?>" score="<?php echo $score['value'] ?>" type="<?php echo $option ?>" id="score-id-<?php echo $iterator ?>">
																	<label class="score-text" for="score-id-<?php echo $iterator ?>"><?php echo $score['score_text'] ?></label>
																	<label class="score-value-tip">Score: <?php echo $score['value'] ?></label>
																</div>
																<?php
																$checked = '';
																$iterator++;
															}
															?>
																<div class="under-row"></div>
																<div class="score-header">Comment</div>
																<textarea class="score-comment"><?php echo $comment ?></textarea>
															</div>
															<?php
														}
														?>
														</div>
												</div>
											</div>
										</div>
									</div>
									<?php
									break;
								}
								case 'tests':
								{
									?>
									<div class="top-controls">
										<div class="new-test-name">
											<input type="text" placeholder="New simulation name">
										</div>
										<div class="button top-button copy-test-button">Copy test</div>
										<a href="?page=add-test" class="fa-item">
											<div class="button top-button">Add test</div>
										</a>
										<div class="button top-button order-button">Ordering</div>
									</div>
									<div class="list-header">
										<div class="row">
											<div class="col-sm-4 header-col">Simulation</div>
											<div class="col-sm-2 header-col">Date created</div>
											<div class="col-sm-1 header-col">Takers</div>
											<div class="col-sm-1 header-col">Invited</div>
											<div class="col-sm-1 header-col">Completed</div>
											<div class="col-sm-1 header-col">Questions</div>
											<div class="col-sm-2 header-col">Question pages</div>
										</div>
									</div>
									<div class="test-list-item-proto">
										<!-- <div test-id=""  class="invite-button">Invite</div> -->
										<div test-id="" class="copy-block"></div>
										<div test-id="" class="order-block clearfix">
											<div class="to-bottom"></div>
											<div class="to-top"></div>
										</div>
										<a href="?page=test&id=" class="fa-item">
											<div class="row">
												<div class="col-sm-4 list-col"></div>
												<div class="col-sm-2 list-col"></div>
												<div class="col-sm-1 list-col">0</div>
												<div class="col-sm-1 list-col"></div>
												<div class="col-sm-1 list-col">0</div>
												<div class="col-sm-1 list-col"></div>
												<div class="col-sm-2 list-col"></div>
											</div>
										</a>
									</div>
									<div class="page-list">
									<?php
									tests_list();
									?>
									</div>
									<?php
									break;
								}
								case 'add-test':
								{
									?>
									<div class="top-controls">
										<!-- <div class="button top-button add-preset-button">Add preset</div> -->
										<div class="button top-button add-test-button">Create test</div>
									</div>
									<div class="content-container">
										<div class="score-preset-proto clearfix hidden" style="display: none;">
											<input type="text" class="score-preset-input" placeholder="Detail title">
											<div class="remove-preset-button"></div>
										</div>
										<input placeholder="Test name" class="test-name"></input>
										<div class="checkbox-block margin-top-20">
											<input <?php echo $sql_res['scoring_list_mode'] ?> type="checkbox" name="" id="checkbox-5" class="use-scoring-model">
											<label for="checkbox-5">Use standard scoring model</label>
										</div>
										<!-- <div class="create-report">
											<div class="section-head">Score details presets</div>
											<div class="presets-container">
												<div class="score-preset clearfix">
													<input type="text" class="score-preset-input" placeholder="Detail title">
													<div class="remove-preset-button"></div>
												</div>
												<div class="score-preset clearfix">
													<input type="text" class="score-preset-input" placeholder="Detail title">
													<div class="remove-preset-button"></div>
												</div>
											</div>
										</div> -->
									</div>
									<?php
									break;
								}
								case 'test':
								{
									setcookie('id_buffer', $_GET['id']);
									$_COOKIE['id_buffer'] = $_GET['id'];
									?>
									<div class="top-controls">
										<div class="top-button button s-test-button">Save</div>
										<a href="?page=question&meta=new&id=<?php echo $_COOKIE['id_buffer'] ?>" class="fa-item">
											<div class="top-button button new-question">New question</div>
										</a>
										<div class="button top-button copy-question-button">Copy question</div>
										<div class="button top-button order-button">Ordering</div>
										<!-- <div class="top-button button rm-button rm-test">Remove</div> -->
									</div>
									<div class="content-container clearfix">
										<?php
										$sql_res = mysqli_fetch_assoc($sqlc->query("SELECT * FROM tests WHERE id = '{$_GET['id']}'"));
										?>
										<div class="row">
											<div class="col-sm-12">
												<input type="text" placeholder="Test name" class="test-name" value="<?php echo $sql_res['name'] ?>">
											</div>
											<div class="col-sm-12">
												<div class="splitter"></div>
											</div>
											<div class="col-sm-12">
												<div class="list-header">
													<div class="row">
														<div class="col-sm-12 header-col">Question title</div>
													</div>
												</div>
												<div class="test-list-item-proto">
													<div test-id="" class="copy-block"></div>
													<div test-id="" class="order-block clearfix">
														<div class="to-bottom"></div>
														<div class="to-top"></div>
													</div>
													<a href="?page=question&id=" class="fa-item">
														<div class="row">
															<div class="col-sm-12 list-col"></div>
														</div>
													</a>
												</div>
												<div class="page-list">
												<?php
												questions_list();
												?>
												</div>
											</div>
										</div>
									</div>
									<?php
									break;
								}
								case 'question':
								{
									require_once $displayer_path . 'question.php';
									if(isset($_GET['meta']) && $_GET['meta'] == 'new')
									{
										$meta = false;
										?>
										<div class="hidden new-question-meta">new</div>
										<?php
									}
									else
									{
									?>
										<div class="hidden question-page-flag"></div>
									<?php
										$meta = true;
									}

									setcookie('qid_buffer', $_GET['id']);
									$_COOKIE['qid_buffer'] = $_GET['id'];

									if($meta)
									{
										$prev_id = mysqli_fetch_assoc($sqlc->query("SELECT id FROM questions WHERE id < '{$_COOKIE['qid_buffer']}' AND test_id = '{$_COOKIE['id_buffer']}' ORDER BY id DESC LIMIT 1"))['id'];
										$next_id = mysqli_fetch_assoc($sqlc->query("SELECT id FROM questions WHERE id > '{$_COOKIE['qid_buffer']}' AND test_id = '{$_COOKIE['id_buffer']}' ORDER BY id LIMIT 1"))['id'];

										$prev_link = 'index.php?page=question&id=';
										$next_link = 'index.php?page=question&id=';

										if($prev_id == null)
										{
											$prev_link = '#';
										}
										else
										{
											$prev_link .= $prev_id;
										}

										if($next_id == null)
										{
											$next_link = '#';
										}
										else
										{
											$next_link .= $next_id;
										}
									}

									?>
									<div class="top-controls">
									<?php if($meta) { ?>
										<div class="top-button button remove-question-button">Remove</div>
										<div class="top-button button cancle-question-changes-button">Cancel</div>
									<?php } ?>
										<div class="top-button button s-qus-button">Save</div>
										<!-- <div class="top-button button rm-button rm-question">Remove</div> -->
									</div>
									<div class="progress loading-hidden" style="margin-bottom: 18px;">
											<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span class="hidden">Completed</div>
										</div>
									<div class="content-container clearfix">
										<div class="tabs-container">
											<div class="tabs-header">
											<?php if($meta) { ?>
												<a href="<?php echo $prev_link ?>">
													<div class="switch-question-button prev-question pull-left"><div class="button-row">&#8249;</div> Previous</div>
												</a>
												<?php } ?>
												<div class="head-tabs-wrap clearfix">
													<div class="head-tab active-tab">Question</div>
													<div class="head-tab">Response</div>
													<?php if($meta) { ?>
													<div class="head-tab">Scoring & feedback</div>
													<?php } ?>
												</div>
												<?php if($meta) { ?>
												<a href="<?php echo $next_link ?>">
													<div class="switch-question-button next-question pull-right">Next <div class="button-row">&#8250;</div></div>
												</a>
												<?php } ?>
											</div>
											<?php
											
											$sql_res = array();
											
											if($meta)
											{
												$sql_res = mysqli_fetch_assoc($sqlc->query("SELECT * FROM questions WHERE id = '{$_GET['id']}'"));
											}
											else
											{
												$columns = $sqlc->query("SHOW COLUMNS FROM questions");
												foreach($columns as $column)
												{
													$sql_res[$column['Field']] = '';
												}

												$sql_res['mins'] = '3';
												$sql_res['secs'] = '0';

												$sql_res['rerecord'] = 'Re-record';
											}
											?>
											<div class="tab-content question-description-tab active-tab-content">
												<div class="row">
													<div class="col-sm-12">
														<div class="col-sm-6 test-area-left">
															<input type="text" class="item-title" placeholder="Question title" value="<?php echo $sql_res['title'] ?>">
															<textarea class="item-ques" placeholder="Question"><?php echo $sql_res['question'] ?></textarea>
														</div>
														<div class="col-sm-6 test-area-right">
															<input type="text" placeholder="Item number" class="item-num" value="<?php echo $sql_res['item_number'] ?>">
															<textarea class="item-cont" placeholder="Context"><?php echo $sql_res['context'] ?></textarea>
														</div>
													</div>
													<div class="col-sm-12">
														<div class="splitter"></div>
													</div>
													<div class="col-sm-12">
														<div class="section-head">Question</div>
													</div>
													<div class="col-sm-12">
														<div class="content-block">
															<div class="table-block">
															<?php if($sql_res['question_token'] != '') {?>
																<div class="qus-cell">
																	<div class="inner-cell zig-ready-cell">
																		<ziggeoplayer class="qus-player"
																		ziggeo-video='<?php echo $sql_res['question_token'] ?>'
																		ziggeo-responsive=true
																		></ziggeoplayer>
																	</div>
																</div>
															<?php } ?>
																<div class="qus-cell">
																	<div class="inner-cell full-cell">
																		<div class="upload-video">
																			<div class="upload-title">Upload video question</div>
																			<div class="video-row">
																				<div class="border-circle">
																					<div class="image-circle"></div>
																				</div>
																			</div>
																			<div class="upload-part">
																				<input type="file" accept="video/*" class="video-hidden-input" id="question-video-input">
																				<label for="question-video-input" class="upload-button">Upload</label>
																				<div class="file-title">Some file name</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-content hidden-tab">
												<div class="row">
													<div class="col-sm-12">
														<div class="report-select-block inrow-record-mode clearfix">
															<div class="select-desc">Choose scoring mode</div>
															<div class="downwrapper">
																<div class="dropdown clearfix">
																	<div value="<?php echo $sql_res['rerecord'] ?>" class="text rec-mode"><?php echo $sql_res['rerecord'] ?></div>
																	<div class="c-caret">
																		<img src="img/caret.png">
																	</div>
																	<div class="items">
																		<div value="None" class="item">None</div>
																		<div value="Once" class="item">Once</div>
																		<div value="Re-record" class="item">Re-record</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="time-limit-select clearfix">
															<div class="descr-for">Time limit</div>
															<input type="text" placeholder="Mins." class="mins" value="<?php echo $sql_res['mins'] ?>">
															<input type="text" placeholder="Secs." class="secs" value="<?php echo $sql_res['secs'] ?>">
														</div>
													</div>
													<div class="col-sm-12">
														<div class="splitter"></div>
													</div>
													<div class="col-sm-12">
														<div>
															<div class="checkbox-block checkbox-after">
																<input <?php echo $sql_res['rev_of_ans'] ?> type="checkbox" name="" id="checkbox-1" class="rev_of_ans">
																<label for="checkbox-1">Require review of answer</label>
															</div>
														</div>
														<div>
															<div class="checkbox-block checkbox-after">
																<input <?php echo $sql_res['ans_af_rev'] ?> type="checkbox" name="" id="checkbox-2" class="ans_af_rev">
																<label for="checkbox-2">Require answer after review</label>
															</div>
														</div>
														<div>
															<div class="checkbox-block checkbox-after">
																<input <?php echo $sql_res['strong_require'] ?> type="checkbox" name="" id="checkbox-3" class="req_stn">
																<label for="checkbox-3">Require review of strong response</label>
															</div>
														</div>
														<div>
															<div class="checkbox-block checkbox-after">
																<input <?php echo $sql_res['ans_af_str'] ?> type="checkbox" name="" id="checkbox-4" class="ans_af_str">
																<label for="checkbox-4">Require answer after strong response</label>
															</div>
														</div>
													</div>
													<div class="col-sm-12">
														<?php
														if($sql_res['strong_require'] != 'checked')
															$strong_display = 'none';
														else
															$strong_display = 'block';
														?>
														<div class="question-edit-strong-wrapper clearfix" style="display: <?php echo $strong_display ?>">
															<div class="col-sm-12">
																<div class="splitter"></div>
															</div>
															<div class="col-sm-12">
																<div class="row">
																	<div class="col-sm-12">
																		<div class="section-head">Strong response</div>
																	</div>
																	<div class="col-sm-12">
																		<div class="content-block">
																			<div class="table-block">
																			<?php if($sql_res['strong_token'] != '') {?>
																				<div class="qus-cell">
																					<div class="inner-cell zig-ready-cell">
																						<ziggeoplayer class="qus-player"
																						ziggeo-video='<?php echo $sql_res['strong_token'] ?>'
																						ziggeo-responsive=true
																						></ziggeoplayer>
																					</div>
																				</div>
																			<?php } ?>
																				<div class="qus-cell">
																					<div class="inner-cell full-cell">
																						<div class="upload-video">
																							<div class="upload-title">Upload video question</div>
																							<div class="video-row">
																								<div class="border-circle">
																									<div class="image-circle"></div>
																								</div>
																							</div>
																							<div class="upload-part">
																								<input type="file" accept="video/*" class="video-hidden-input" id="strong-video-input">
																								<label for="strong-video-input" class="upload-button">Upload</label>
																								<div class="file-title">Some file name</div>
																							</div>
																						</div>
=======
														<div class="splitter"></div>
													</div>
													<div class="col-sm-12">
														<div class="col-sm-12">
															<div class="section-head">Strong response</div>
														</div>
														<div class="col-sm-12">
															<div class="content-block">
																<div class="table-block">
																<?php if($sql_res['strong_token'] != '') {?>
																	<div class="qus-cell">
																		<div class="inner-cell zig-ready-cell">
																			<ziggeoplayer class="qus-player"
																			ziggeo-video='<?php echo $sql_res['strong_token'] ?>'
																			ziggeo-responsive=true
																			></ziggeoplayer>
																		</div>
																	</div>
																<?php } ?>
																	<div class="qus-cell">
																		<div class="inner-cell full-cell">
																			<div class="upload-video">
																				<div class="upload-title">Upload video question</div>
																				<div class="video-row">
																					<div class="border-circle">
																						<div class="image-circle"></div>
																					</div>
																				</div>
																				<div class="upload-part">
																					<input type="file" accept="video/*" class="video-hidden-input" id="strong-video-input">
																					<label for="strong-video-input" class="upload-button">Upload</label>
																					<div class="file-title">Some file name</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<?php if($meta) { ?>
											<div class="tab-content scores-data-tab hidden-tab">
												<div class="row">
													<div class="col-sm-12">
														<div class="score-proto clearfix" ordering="" score-id="" style="display: none;">
															<div class="score-copy-block hidden score-copy-block-new"></div>
															<div class="remove-button"></div>
															<div class="score-text"><input type="text" placeholder="Score title" value=""></div>
															<div class="score-value"><input type="text" placeholder="Score" value=""></div>
															<div class="proto-spacer"></div>
															<div class="up-score-button"></div>
															<div class="down-score-button"></div>
														</div>
														<div class="inner-tabs question-scores-tabs">
															<div class="tabs-button-container clearfix">
															<?php
															if($sql_res['scoring_list_mode'] != 'checked')
															{
																for($i = 0; $i < $sql_res['answer_slides']; $i++)
																{
																	?>
																	<div class="tab-button <?php if($i == 0) echo 'current-inner-tab-button' ?>">Response <?php echo $i+1 ?></div>
																	<?php
																}
															}
															else
															{
																$sql_res['answer_slides'] = '1';
															}
															?>
																<div class="tab-button-back"></div>
																<div class="score-mode-check-block">
																	<div class="checkbox-block">
																		<input <?php echo $sql_res['scoring_list_mode'] ?> type="checkbox" name="" id="checkbox-5" class="scoring-list-mode">
																		<label for="checkbox-5">Single checklist</label>
																	</div>
																</div>
															</div>
															<div class="inner-content-tabs clearfix">
															<?php
															for($i = 1; $i < $sql_res['answer_slides'] + 1; $i++)
															{
															?>
																<div class="inner-tab-content <?php if($i > 1) { echo 'inner-hidden-tab inner-nodisplay'; } ?>">
																	<div class="row">
																		<div class="col-sm-12">
																			<div class="scoring-wrap">
																			<?php
																			$scoring_text;
																			if($sql_res['scoring_list_mode'] != 'checked')
																			{
																				$score_col_name = 'scoring_mode_' . $i;
																			}
																			else
																			{
																				$score_col_name = 'single_list_scoring_mode';
																			}

																			if(!$meta)
																			{
																				$sql_res[$score_col_name] = '1';
																			}

																			if($sql_res[$score_col_name] == '1')
																			{
																				$scoring_text = 'Single choice';
																			}
																			else if($sql_res[$score_col_name] == '2')
																			{
																				$scoring_text = 'Multiple choice';
																			}
																			?>
																				<div class="report-select-block pull-left">
																					<div class="select-desc">Choose scoring mode</div>
																					<div class="downwrapper scoring-mode">
																						<div class="dropdown clearfix">
																							<div value="<?php echo $sql_res[$score_col_name] ?>" class="text"><?php echo $scoring_text ?></div>
																							<div class="c-caret">
																								<img src="img/caret.png">
																							</div>
																							<div class="items">
																								<div value="1" class="item">Single choice</div>
																								<div value="2" class="item">Multiple choice</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<?php if($i == 1 && $sql_res['scoring_list_mode'] != 'checked') { ?>
																					<div class="stop-copy-score-button hidden">Stop copying</div>
																					<div class="copy-score-block clearfix">
																						<div class="copy-score-hover-button">Copy score to...</div>
																						<div class="buttons-block clearfix">
																						<?php for($j = 1; $j < $sql_res['answer_slides']; $j++) { ?>
																							<div response="<?php echo $j+1 ?>" class="copy-response-button">Response <?php echo $j+1 ?></div>
																						<?php } ?>
																						</div>
																					</div>
																				<?php } ?>
																			</div>
																		</div>
																		<div class="col-sm-12 score-items-wrap">
																		<?php
																		$inside_query = "AND single_list = '";
																		if($sql_res['scoring_list_mode'] == 'checked')
																			$inside_query .= "1'";
																		else
																			$inside_query .= "0'";

																		$score_res = $sqlc->query("SELECT * FROM question_scores WHERE question_id = '{$_COOKIE['qid_buffer']}' AND response = '{$i}' {$inside_query} ORDER BY ordering");

																		foreach($score_res as $score) {
																		?>
																		<div class="score-item clearfix" ordering="<?php echo $score['ordering'] ?>" score-id="<?php echo $score['id'] ?>">
																			<div class="score-copy-block hidden"></div>
																			<div class="remove-button"></div>
																			<div class="score-text"><input type="text" placeholder="Score title" value="<?php echo $score['score_text'] ?>"></div>
																			<div class="score-value"><input type="text" placeholder="Score" value="<?php echo $score['value'] ?>"></div>
																			<div class="details-button"></div>
																			<div class="up-score-button"></div>
																			<div class="down-score-button"></div>
																		</div>
																		<?php } ?>
																		</div>
																		<div class="col-sm-12">
																		<div class="splitter"></div>
																			<div class="add-score-block clearfix">
																				<div class="score-copy-block score-copy-block-new hidden"></div>
																				<div class="score-text"><input type="type" placeholder="Score title"></div>
																				<div class="score-value"><input type="text" placeholder="Score"></div>
																				<div class="add-button"></div>
																			</div>
																		</div>
																	</div>
																</div>
																<?php
																}
																?>
															</div>
														</div>
													</div>
												</div>
											</div>
											<?php } ?>
										</div>
									</div>
									<?php
									break;
								}
								case 'users':
								{
									?>
									<div class="top-controls">
										<div class="button top-button assign-user-test-save-button user-test-assign-invisible">Assign</div>
										<div class="button top-button create-simple-user-button">Create user</div>
										<div class="button top-button assign-user-test-button">Assign menu</div>
										<div class="progress loading-hidden">
											<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 0%"><span class="hidden">Completed</div>
										</div>
									</div>
									<div class="content-container clearfix">
										<div class="test-users-chooser-block clearfix">
											<div class="clearfix">
												<div class="report-select-block float-left">
													<div class="select-desc">Assign simulation for users</div>
													<div class="downwrapper choose-user-test">
														<div class="dropdown clearfix">
														<?php
														$first_test = mysqli_fetch_assoc($sqlc->query("SELECT id, name
															FROM tests 
															WHERE id IN (
															SELECT test_id FROM questions
															) ORDER BY ordering LIMIT 1"));
														?>
															<div value="<?php echo $first_test['id'] ?>" class="text"><?php echo $first_test['name'] ?></div>
															<div class="c-caret">
																<img src="img/caret.png">
															</div>
															<div class="items">
															<?php
															$tests = $sqlc->query("SELECT id, name FROM tests WHERE id IN (SELECT test_id FROM questions) ORDER BY ordering");
															foreach($tests as $test)
															{
															?>
																<div value="<?php echo $test['id'] ?>" class="item test-choose current-user-test-choose"><?php echo $test['name'] ?></div>
															<?php } ?>
															</div>
														</div>
													</div>
												</div>
												<?php
												// 604800
												$time_offset = time() + 604800;
												$day = date('d', $time_offset);
												$month = date('n', $time_offset);
												$year = date('Y', $time_offset);
												display_calendar('assign-test-calendar', $day, $month, $year);
												?>
												<!-- <div class="time-picker clearfix float-left">
													<input class="hours" type="text">
													<div class="colon">:</div>
													<input class="minutes" type="text">
													<div class="day-time-picker">
														<div class="day-item active">AM</div>
														<div class="day-item">PM</div>
													</div>
													<div class="timezone">EST</div>
												</div> -->
											</div>
											<div class="users-for-assign-list"></div>
										</div>
										<!--<div class="additional-search">
											<div class="addt-header clearfix">
												<img src="img/caret-left.png" alt="">
												<span>Additional search</span>
											</div>
											<div class="addt-block">
												<div class="report-select-block">
													<div class="select-desc">Invited to simulation</div>
													<div class="downwrapper test-state-chooser">
														<div class="dropdown clearfix">
															<div value="0" class="text">None</div>
															<div class="c-caret">
																<img src="img/caret.png">
															</div>
															<div class="items">
															<div value="0" class="item invited-test-item">None</div>
															<?php
															// $tests = $sqlc->query("SELECT id, name
															// 	FROM tests
															// 	ORDER BY ordering");

															// foreach($tests as $test)
															{
															?>
																<div value="<?php //echo $test['id'] ?>" class="item invited-test-item"><?php //echo $test['name'] ?></div>
															<?php } ?>
															</div>
														</div>
													</div>
												</div>
												<div class="report-select-block">
													<div class="select-desc">Completed the simulation</div>
													<div class="downwrapper test-state-chooser">
														<div class="dropdown clearfix">
															<div value="0" class="text">None</div>
															<div class="c-caret">
																<img src="img/caret.png">
															</div>
															<div class="items">
																<div value="0" class="item completed-test-item">None</div>
															<?php
															// $tests->data_seek(0);

															// foreach($tests as $test)
															{
															?>
																<div value="<?php //echo $test['id'] ?>" class="item completed-test-item"><?php //echo $test['name'] ?></div>
															<?php } ?>
															</div>
														</div>
													</div>
												</div>
												<div class="report-select-block">
													<div class="select-desc">Scored for simulation</div>
													<div class="downwrapper test-state-chooser">
														<div class="dropdown clearfix">
															<div value="0" class="text">None</div>
															<div class="c-caret">
																<img src="img/caret.png">
															</div>
															<div class="items">
																<div value="0" class="item scored-test-item">None</div>
															<?php
															// $tests->data_seek(0);

															// foreach($tests as $test)
															{
															?>
																// <div value="<?php echo $test['id'] ?>" class="item scored-test-item"><?php //echo $test['name'] ?></div>
															<?php } ?>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div> -->
										<div user-id="" class="list-item-proto user-list-item" style="display: none;">
											<div class="assign-check-wrap">
												<div class="assign-checkbox-block">
													<input disabled type="checkbox" id="checkbox-" class="user-list-checkbox">
													<label for="checkbox-"></label>
												</div>
												<div class="tested-badge"></div>
											</div>
											<a href="index.php?page=user&id=" class="fa-item">
												<div class="row">
													<div class="col-sm-3 list-col">&nbsp</div>
													<div class="col-sm-3 list-col">&nbsp</div>
													<div class="col-sm-2 list-col">&nbsp</div>
													<div class="col-sm-2 list-col">&nbsp</div>
													<div class="col-sm-2 list-col">&nbsp</div>
												</div>
											</a>
										</div>
										<div class="search-block">
											<div class="row">
												<div class="col-sm-3 search-input-wrap"><input placeholder="Name" type="text"></div>
												<div class="col-sm-3 search-input-wrap"><input placeholder="Email" type="text"></div>
												<div class="col-sm-2 search-input-wrap"><input placeholder="Invited" type="text"></div>
												<div class="col-sm-2 search-input-wrap"><input placeholder="Completed" type="text"></div>
												<div class="col-sm-2 search-input-wrap"><input placeholder="Scored" type="text"></div>
											</div>
										</div>
										<div class="list-header">
											<div class="row">
												<div class="col-sm-3 header-col"><div state="none" col-num="1" class="search-caret"></div>Name</div>
												<div class="col-sm-3 header-col"><div state="none" col-num="2" class="search-caret"></div>Email</div>
												<div class="col-sm-2 header-col"><div state="none" col-num="3" class="search-caret"></div>Invited</div>
												<div class="col-sm-2 header-col"><div state="none" col-num="4" class="search-caret"></div>Completed</div>
												<div class="col-sm-2 header-col"><div state="none" col-num="5" class="search-caret"></div>Scored</div>
											</div>
										</div>
										<div class="search-fail-wrap">
											<div class="search-fail-block">
												<div class="message">No one search result does not found :(</div>
												<div class="icon"></div>
											</div>
										</div>
										<?php
										$sql_res = $sqlc->query("SELECT id, invited, name, sec_name, email, completed, scored FROM users ORDER BY id");
										foreach($sql_res as $sql_row)
										{
										?>
										<div user-id="<?php echo $sql_row['id'] ?>" class="list-item user-list-item result-list-item">
											<div class="assign-check-wrap">
												<div class="assign-checkbox-block">
													<input disabled type="checkbox" id="checkbox-<?php echo $sql_row['id'] ?>" class="user-list-checkbox">
													<label for="checkbox-<?php echo $sql_row['id'] ?>"></label>
												</div>
												<div class="tested-badge">
												</div>
											</div>
											<a href="index.php?page=user&id=<?php echo $sql_row['id'] ?>" class="fa-item">
												<div class="row">
													<div class="col-sm-3 list-col"><?php echo $sql_row['name'] . ' ' . $sql_row['sec_name'] ; ?></div>
													<div class="col-sm-3 list-col"><?php echo $sql_row['email'] ?></div>
													<div class="col-sm-2 list-col"><?php echo $sql_row['invited'] ?></div>
													<div class="col-sm-2 list-col"><?php echo $sql_row['completed'] ?></div>
													<div class="col-sm-2 list-col"><?php echo $sql_row['scored'] ?></div>
												</div>
											</a>
										</div>
										<?php } ?>
									</div>
									<?php
									break;
								}
								case 'user':
								{
									$sql_res = mysqli_fetch_assoc($sqlc->query("SELECT * FROM users WHERE id = '{$_GET['id']}'"));
									?>
									<div class="content-container non-padding non-controls clearfix">
										<div class="profile-header clearfix">
											<div class="name"><?php echo $sql_res['name'] ?></div>
											<div class="title">account</div>
										</div>
										<div class="profile-info">
											<div class="row">
												<div class="col-sm-2 img-wrap">
													<img class="user-icon" src="img/user-large.png" alt="">
												</div>
												<div class="col-sm-2">
													<div class="user-name-container">
														<div class="edit-button"></div>
														<div class="save-edited"></div>
														<div class="edit-block">
															<div class="title title-name"><?php echo $sql_res['name'] ?></div>
															<input placeholder="Name" type="text" class="input name" value="<?php echo $sql_res['name'] ?>">
														</div>
														<div class="edit-block">
															<div class="title title-sec_name"><?php echo $sql_res['sec_name'] ?></div>
															<input placeholder="Second Name" type="text" class="input sec_name" value="<?php echo $sql_res['sec_name'] ?>">
														</div>
														<div class="edit-block">
															<div class="title title-email"><?php echo $sql_res['email'] ?></div>
															<input placeholder="Email" type="text" class="input email" value="<?php echo $sql_res['email'] ?>">
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="stat-box">
														<img src="img/test-icon.png" alt="">
														<div class="title">Sim. invites</div>
														<div class="stat-splitter"></div>
														<div class="value">3</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="stat-box">
														<img src="img/checkmark-icon.png" alt="">
														<div class="title">Completed</div>
														<div class="stat-splitter"></div>
														<div class="value">1</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="stat-box">
														<img src="img/score-icon.png" alt="">
														<div class="title">Scored</div>
														<div class="stat-splitter"></div>
														<div class="value">4</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="stat-box">
														<img src="img/cancled-icon.png" alt="">
														<div class="title">Canceled</div>
														<div class="stat-splitter"></div>
														<div class="value">5</div>
													</div>
												</div>
												<div class="col-sm-12">
													<div class="status-notes">
														<div class="note clearfix">
															<div class="circle invited"></div>
															<div class="title"> - Only invited</div>
														</div>
														<div class="note clearfix">
															<div class="circle inprogress"></div>
															<div class="title"> - In progress</div>
														</div>
														<div class="note clearfix">
															<div class="circle completed"></div>
															<div class="title"> - Sim. completed</div>
														</div>
														<div class="note clearfix">
															<div class="circle expired"></div>
															<div class="title"> - Invite expired</div>
														</div>
														<div class="note clearfix">
															<div class="circle scored"></div>
															<div class="title"> - Scored</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="user-invites">
											<div class="invites-head clearfix">
												<div class="head-item">Simulation name</div>
												<div class="head-item">Required completion</div>
												<div class="head-item">Progress</div>
												<div class="head-item">Status</div>
												<div class="head-item">Controls</div>
											</div>
											<div class="invite-items">
											<?php

											$invites = $sqlc->query("SELECT * FROM user_tests WHERE user_id = '{$_GET['id']}'");

											function getProgressStatus($user_id, $test_id, $invite_id)
											{
												global $sqlc;

												$questions_count = $sqlc->query("SELECT id 
													FROM questions 
													WHERE test_id = '{$test_id}'")->num_rows;

												//Check for just invited

												$answers_count = $sqlc->query("SELECT id 
													FROM answers 
													WHERE test_id = '{$test_id}'
													AND user_id = '{$user_id}'")->num_rows;

												if($answers_count == 0)
												{
													?><div class="status-item invited"></div><?php
												}

												//Check for in progress

												if($answers_count > 0 && $answers_count < $questions_count)
												{
													?><div class="status-item inprogress"></div><?php
												}

												//Check for user completed sim

												if($answers_count == $questions_count)
												{
													?><div class="status-item completed"></div><?php
												}

												$expire_date = $sqlc->query("SELECT required_date 
													FROM user_tests 
													WHERE id = '{$invite_id}'")->fetch_assoc()['required_date'];

												if(time() > intval($expire_date))
												{
													?><div class="status-item expired"></div><?php
												}

												$point_scores = $sqlc->query("SELECT DISTINCT question_id 
													FROM user_scores 
													WHERE user_id = '{$user_id}'
													AND test_id = '{$test_id}'")->fetch_all(MYSQLI_ASSOC);

												$text_scores = $sqlc->query("SELECT DISTINCT question_id 
													FROM text_scores
													WHERE user_id = '{$user_id}'
													AND test_id = '{$test_id}'")->fetch_all(MYSQLI_ASSOC);

												$scored = count(array_unique(array_merge($point_scores, $text_scores), SORT_REGULAR));

												if($scored = $questions_count)
												{
													?><div class="status-item scored"></div><?php
												}
											}

											foreach($invites as $invite)
											{
												//Calendar calculations
												$sim_data = $sqlc->query("SELECT * FROM tests WHERE id = '{$invite['test_id']}'")->fetch_assoc();
												$day = date('j', $invite['required_date']);
												$month = date('n', $invite['required_date']);
												$year = date('Y', $invite['required_date']);

												$first_question = $sqlc->query("SELECT id FROM questions WHERE test_id = '{$sim_data['id']}' ORDER BY ordering")->fetch_assoc()['id'];

												//Progress calculation

												$questions_count = $sqlc->query("SELECT id FROM questions WHERE test_id = '{$invite['test_id']}'")->num_rows;
												$answers_count = $sqlc->query("SELECT id 
													FROM answers 
													WHERE test_id = '{$test_id}'
													AND user_id = '{$user_id}'
													AND  state = 'finished'")->num_rows;

												?>
												<div 
												item-id="<?php echo $invite['id'] ?>"
												test-id="<?php echo $invite['test_id'] ?>"
												day="<?php echo $day ?>"
												month="<?php echo $month ?>"
												year="<?php echo $year ?>"
												class="invite-item clearfix">
													<div class="invite-col"><?php echo $sim_data['name'] ?></div>
													<div class="invite-col calendar-col"><?php display_calendar('', $day, $month, $year) ?></div>
													<div class="invite-col"><?php echo $answers_count ?> of <?php echo $questions_count ?></div>
													<div class="invite-col progress-status">
														<?php getProgressStatus($invite['user_id'], $invite['test_id'], $invite['id']) ?>
														<!-- <div class="status-item completed"></div> -->
													</div>
													<div class="invite-col invite-controls">
														<div class="invite-controls-wrap clearfix">
															<a href="?page=rating&test-id=<?php echo $sim_data['id'] ?>&user-id=<?php echo $_GET['id'] ?>&question-id=<?php echo $first_question ?>">
																<div class="invite-control clearfix">
																	<i class="fa fa-check-square-o"></i>
																</div>
															</a>
															<div class="invite-control save-user-invite">
																<i class="fa fa-floppy-o"></i>
															</div>
															<div class="invite-control last remove-user-invite">
																<i class="fa fa-trash"></i>
															</div>
														</div>
													</div>
												</div>
												<?php
											}
											?>
											</div>
										</div>
									</div>
									<?php
									break;
								}
								case 'link-tokens':
								{
									?>
									<div class="create-link-token">
										<div class="section-head inline-button">Link tokens
											<div class="add-link-token-button pull-right"></div>
										</div>
										<?php
										$first_test = mysqli_fetch_assoc($sqlc->query("SELECT id, name FROM tests WHERE id IN (SELECT test_id FROM questions) ORDER BY ordering LIMIT 1"));
										?>
										<div token-value="" token-id="" class="token-list-item-proto proto-token-list-item-style overflow-hidden clearfix">
											<div class="token-value"><?php echo DOMAIN ?>/?action=register&token=</div>
											<div class="remove-token-button pull-right"></div>
											<div class="downwrapper choose-link-test pull-right">
												<div class="dropdown clearfix">
													<div value="<?php echo $first_test['id'] ?>" class="text"><?php echo $first_test['name'] ?></div>
													<div class="c-caret">
														<img src="img/caret.png">
													</div>
													<div class="items">
													<?php
													$tests = $sqlc->query("SELECT id, name FROM tests WHERE id IN (SELECT test_id FROM questions) ORDER BY ordering");
													foreach($tests as $test)
													{
													?>
															<div value="<?php echo $test['id'] ?>" class="item test-choose token-link-test-choose"><?php echo $test['name'] ?></div>
													<?php } ?>
													</div>
												</div>
											</div>
										</div>
										<!--  -->
										<div class="page-list link-tokens-list">
										<?php
										tokens_list();
										?>
										</div>
									</div>
									<?php
									break;
								}
								case 'settings':
								{
									?>
									<div class="top-controls">
										<div class="button top-button save-settings-button">Save</div>
									</div>
									<div class="content-container">
										<div class="row">
											<div class="col-sm-8">
												<div class="create-report practice-question-block">
													<div class="section-head">Question for practice</div>
													<div class="report-select-block">
														<div class="select-desc">Select question for user practice</div>
														<div class="downwrapper practice-question-select">
														<?php
														$current_question = mysqli_fetch_assoc($sqlc->query("SELECT id, title FROM questions WHERE for_test = '1'"));
														$questions = $sqlc->query("SELECT id, title FROM questions ORDER BY ordering");
														?>
															<div class="dropdown clearfix">
																<div value="<?php echo $current_question['id'] ?>" class="text"><?php echo $current_question['title'] ?></div>
																<div class="c-caret">
																	<img src="img/caret.png">
																</div>
																<div class="items">
																<?php
																foreach($questions as $question)
																{
																?>
																	<div value="<?php echo $question['id'] ?>" class="item"><?php echo $question['title'] ?></div>
																<?php } ?>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="create-report practice-question-block">
													<div class="section-head">Question for start</div>
													<div class="report-select-block">
														<div class="select-desc">Select question for newly registered user</div>
														<div class="downwrapper start-question-select">
														<?php
														$current_question = mysqli_fetch_assoc($sqlc->query("SELECT id, title FROM questions WHERE for_start = '1'"));
														$questions = $sqlc->query("SELECT id, title FROM questions ORDER BY ordering");
														?>
															<div class="dropdown clearfix">
																<div value="<?php echo $current_question['id'] ?>" class="text"><?php echo $current_question['title'] ?></div>
																<div class="c-caret">
																	<img src="img/caret.png">
																</div>
																<div class="items">
																<?php
																foreach($questions as $question)
																{
																?>
																	<div value="<?php echo $question['id'] ?>" class="item"><?php echo $question['title'] ?></div>
																<?php } ?>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="create-report change-password-block">
													<div class="section-head">Change password</div>
													<input class="change-password-input admin-password" type="password" placeholder="Old password">
													<input class="change-password-input password-new" type="password" placeholder="New password">
													<input class="change-password-input password-new-repeat" type="password" placeholder="New password again">
													<div class="button-wrap">
														<div class="button change-password-submit">Change</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php
									break;
								}
								case 'admin-rules':
								{
									?>
									<div class="top-controls">
										<a href="index.php?page=create-user" class="fa-item">
											<div class="top-button button create-user">Create user</div>
										</a>
									</div>
									<div class="content-container">
										<div class="row">
											<div class="col-sm-8">
												<div class="items-block">
													<div class="list-header">
														<div class="row">
															<div class="col-sm-5 header-col">User login</div>
															<div class="col-sm-5 header-col">User type</div>
															<div class="col-sm-2 header-col">Active?</div>
														</div>
													</div>
													<div class="page-list">
													<?php
													rules_list();
													?>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="items-block">
													<div class="head">User list actions</div>
													<div class="user-actions-switch-wrap">
														<div class="switch-block">
															<div value="hide" class="switch switch-active">Off</div>
															<div value="show" class="switch">On</div>
														</div>
													</div>
													<div class="admin-actions-wrap" style="display: none">
														<div class="admin-buttons clearfix">
															<div class="button activate-users">Activate users</div>
															<div class="button deactivate-users">Deactivate users</div>
															<div class="button remove-users">Remove users</div>
															<div class="button change-type">Change type</div>
														</div>
														<div class="spacer"></div>
														<?php $first_mime = mysqli_fetch_assoc($sqlc->query("SELECT id FROM admin_user_mime_types ORDER BY id"))['id'] ?>
														<div class="vertical-select-widget user-mime-type-select" value="<?php echo $first_mime ?>">
															<div class="select-mode"></div>
															<?php
															$user_mime_types = $sqlc->query("SELECT * FROM admin_user_mime_types ORDER BY id");
															foreach($user_mime_types as $user_mime_type)
															{
															?>
															<div value="<?php echo $user_mime_type['id'] ?>" class="select-item"><?php echo $user_mime_type['title'] ?></div>
															<?php
															}
															?>
														</div>
													</div>
													</div>
												</div>
											</div>
										</div>
										</div>
									</div>
									<?php
									break;
								}
								case 'create-user':
								{
									?>
									<div class="top-controls">
										<div class="top-button button create-user-button">Create</div>
									</div>
									<div class="content-container">
										<div class="row">
											<div class="col-sm-3">
												<div class="items-block">
													<div class="head">User data</div>
													<div class="hover-tip">
														<div class="content">
															<div class="tip clearfix">
																<div class="img"></div>
																<span>Tip!</span>
															</div>
															After clicking button "Create" user will get email message with assigned here password and link to account confirmation, this invite will be expired after 10 days.
														</div>
													</div>
													<div class="onpage-form create-admin-user-form">
														<div class="input-error all-fields">Complete all fieleds</div>
														<div class="input-error wrong-email">Wrong email</div>
														<div class="input-error pass-length">Minimum password length 8 characters</div>
														<div class="input-error email-exists">Email already exists</div>
														<input placeholder="Email" type="text" class="new-user-email">
														<input placeholder="Password" type="text" class="new-user-password">
														<div class="button-wrap">
															<div class="button generate-user-password">Generate password</div>
														</div>
													</div>
												</div>
												<div class="items-block">
													<div class="head">User type</div>
													<div class="hover-tip">
														<div class="content">
															<div class="tip clearfix">
																<div class="img"></div>
																<span>Tip!</span>
															</div>
															This section just gives opportunity to choose user predestination to more easily manage administator users list in future.
														</div>
													</div>
													<div class="spacer"></div>
													<?php $first_mime = mysqli_fetch_assoc($sqlc->query("SELECT id FROM admin_user_mime_types ORDER BY id"))['id'] ?>
													<div class="vertical-select-widget user-mime-type-select" value="<?php echo $first_mime ?>">
														<div class="select-mode"></div>
														<?php
														$user_mime_types = $sqlc->query("SELECT * FROM admin_user_mime_types ORDER BY id");
														foreach($user_mime_types as $user_mime_type)
														{
														?>
														<div value="<?php echo $user_mime_type['id'] ?>" class="select-item"><?php echo $user_mime_type['title'] ?></div>
														<?php
														}
														?>
													</div>
												</div>
											</div>
											<div class="col-sm-9">
												<div class="items-block">
													<div class="head">Page rules</div>
													<div class="create-user-rule-buttons">
														<div class="hover-button all-read clearfix">
															<div class="img float-left" img="img/glasses.png"></div>
															<div class="title float-left">All read only</div>
														</div>
														<div class="hover-button all-read-write clearfix">
															<div img="img/read-write.png" class="img float-left"></div>
															<div class="title float-left">All read/write</div>
														</div>
														<div class="hover-tip inline-block">
															<div class="content bottom">
																<div class="tip clearfix">
																	<div class="img"></div>
																	<span>Tip!</span>
																</div>
																"Page visible?" trigger means, that page will be visible for created user or no.<br>
																If page available for user, dropdown says, will be user able to use change functions or just look content.
															</div>
														</div>
													</div>
													<div class="rule-pages-list">
													<?php
														$pages = $sqlc->query("SELECT * FROM admin_pages WHERE parent_page_id = '0'");
														foreach($pages as $page)
														{
															buildPagesList($page['id']);
														}
													?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php
									break;
								}
								case 'admin-user':
								{
									?>
									<div class="content-container non-controls">
										<div class="col-sm-3">
											<div class="items-block">
												<div class="btn-wrap align-left">
													<div class="button save-style-button update-admin-email">Save</div>
												</div>
												<div class="spacer"></div>
												<div class="head">User email</div>
												<div class="onpage-form update-admin-email-form">
													<div class="input-error required-field">This field is required</div>
													<div class="input-error wrong-email">Wrong email</div>
													<div class="input-error email-exists">Email already exists</div>
													<input placeholder="Email" type="text" class="new-user-email" value="<?php echo mysqli_fetch_assoc($sqlc->query("SELECT email FROM admin_users WHERE id = '{$_GET['id']}'"))['email'] ?>">
												</div>
											</div>
											<div class="items-block">
												<div class="btn-wrap align-left">
													<div class="button save-style-button update-admin-password">Save</div>
												</div>
												<div class="spacer"></div>
												<div class="head">User password</div>
												<div class="onpage-form update-admin-password-form">
													<div class="input-error required-field">This field is required</div>
													<div class="input-error pass-length">Minimum password length 8 characters</div>
													<input placeholder="Passowrd" type="text" class="new-user-password">
													<div class="button-wrap">
														<div class="button generate-user-password">Generate password</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-9">
											<div class="items-block">
												<div class="btn-wrap align-left">
													<div class="button save-style-button update-admin-rules">Save</div>
												</div>
												<div class="spacer"></div>
												<div class="head">User permissions</div>
												<div class="create-user-rule-buttons">
														<div class="hover-button all-read clearfix">
															<div class="img float-left" img="img/glasses.png"></div>
															<div class="title float-left">All read only</div>
														</div>
														<div class="hover-button all-read-write clearfix">
															<div img="img/read-write.png" class="img float-left"></div>
															<div class="title float-left">All read/write</div>
														</div>
														<div class="hover-tip inline-block">
															<div class="content bottom">
																<div class="tip clearfix">
																	<div class="img"></div>
																	<span>Tip!</span>
																</div>
																"Page visible?" trigger means, that page will be visible for created user or no.<br>
																If page available for user, dropdown says, will be user able to use change functions or just look content.
															</div>
														</div>
													</div>
													<div class="rule-pages-list">
														<?php
														$pages = $sqlc->query("SELECT * FROM admin_pages WHERE parent_page_id = '0'");
														foreach($pages as $page)
														{
															buildPagesList($page['id'], true, $_GET['id']);
														}
														?>
													</div>
											</div>
										</div>
									</div>
									<?php
									break;
								}
							}
						}
						else
						{
						?>
							<div class="top-controls">
								<div class="top-button button leave-admin-button">Exit</div>
							</div>
							<div class="content-container">
								<div class="row">
									<div class="col-sm-4">
										<div class="stats-wrapper">
											<div class="stats-header">Gave the first answers</div>
											<div>
												<canvas class="tryed-users"></canvas>
											</div>
										</div>
									</div>
									<div class="col-sm-8">
										<div class="stats-wrapper">
											<div class="stats-header">Visits</div>
											<div>
												<canvas class="visitors"></canvas>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="splitter"></div>
									<div class="col-sm-12">
										<div class="create-report">
											<div class="section-head">Report</div>
											<div class="report-select-block">
												<div class="select-desc">Select test for report</div>
												<div class="downwrapper test-chooser">
													<div class="dropdown clearfix">
														<div value="0" class="text">Choose test</div>
														<div class="c-caret">
															<img src="img/caret.png">
														</div>
														<div class="items">
														<?php
														$tests = $sqlc->query("SELECT * FROM tests ORDER BY id");
														foreach($tests as $test)
														{
														?>
															<div value="<?php echo $test['id'] ?>" class="item test-choose"><?php echo $test['name'] ?></div>
														<?php } ?>
														</div>
													</div>
												</div>
											</div>
											<div class="report-select-block choose-question-block" style="display: none;">
												<div class="select-desc">Select question for report</div>
												<div class="downwrapper question-chooser">
													<div class="dropdown clearfix">
														<div value="0" class="text">Choose test</div>
														<div class="c-caret">
															<img src="img/caret.png">
														</div>
														<div class="items">
														</div>
													</div>
												</div>
											</div>
											<div class="report-cols-wrapper" style="display: none;">
												<div class="tip-block">Each chosen param will be imagined as column in result table</div>
												<div class="repotr-optioin-wrapper">
													<div class="downwrapper">
														<div class="remove-button"></div>
														<div class="dropdown clearfix">
															<div value="" class="text">Option</div>
															<div class="c-caret">
																<img src="img/caret.png">
															</div>
															<div class="items">
																<div value="1" class="item">Name</div>
																<div value="2" class="item">Sec. name</div>
																<div value="3" class="item">Email</div>
																<div value="4" class="item">Link to video</div>
																<div value="5" class="item">Rates</div>
																<div value="6" class="item">Test name</div>
																<div value="7" class="item">Question name</div>
															</div>
														</div>
													</div>
													<div class="add-option-button"></div>
												</div>
											</div>
											<div class="report-download" style="display: none">
												<div class="title">Wait for appear download button</div>
												<div class="icon-row">
													<div class="icon"></div>
												</div>
												<div class="r-button-wrap">
													<div class="button">Download</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php
						}
					?>
					</div>
				</div>
			</div>
		</div>
	</div><!-- Wraps whole page (blur blocker)-->
		<?php
	} else
	{
	?>
	<div class="blur-blocker">
	<?php 
	if(isset($_GET['page']))
	{
		switch($_GET['page'])
		{
			case 'confirm-account':
			{
				$token = mysqli_fetch_assoc($sqlc->query("SELECT * FROM admin_register_tokens WHERE register_token = '{$_GET['token']}'"));

				if($token == NULL || $token['expire'] < time())
				{
					$message = 'This confirmation link expired or not found.';
				}
				else
				{
					//Creating user itself
					$sqlc->query("INSERT INTO admin_users (email, password, user_mime_type_id) 
						VALUES('{$token['email']}', '{$token['password']}', '{$token['user_mime_type_id']}')");

					$user_id = mysqli_fetch_assoc($sqlc->query("SELECT id 
						FROM admin_users 
						ORDER BY id DESC 
						LIMIT 1"))['id'];

					$user_hash = hash('sha256', md5($user_id) . md5($token['email']));

					$sqlc->query("UPDATE admin_users SET user_hash = '{$user_hash}' WHERE id = '{$user_id}'");

					//Getting rules for crated user
					$rules = $sqlc->query("SELECT * FROM admin_temp_rules WHERE token_id = '{$token['id']}'");
					$sqlc->query("DELETE FROM admin_temp_rules WHERE id = '{$token['id']}'");

					foreach($rules as $rule)
					{
						$sqlc->query("INSERT INTO admin_user_rules (user_id, page_id, visibility, type) VALUES (
							'{$user_id}',
							'{$rule['page_id']}',
							'{$rule['visibility']}',
							'{$rule['type']}'
							)");
					}

					$message = 'Your account successfully confirmed.';
				}

				$sqlc->query("DELETE FROM admin_register_tokens WHERE id = '{$token['id']}'");
				$sqlc->query("DELETE FROM admin_temp_rules WHERE token_id = '{$token['id']}'");

				?>
				<div class="confirm-block">
					<div class="title"><?php echo $message ?></div>
					<div class="tologin-block">
						<a href="index.php" class="fa-item">
							<div class="button">Login page</div>
						</a>
					</div>
				</div>
				<?php
			}
		}
	} else {
		?>
		<div class="login-container">
			<input class="cr-email" placeholder="Email" type="text">
			<input class="cr-password" placeholder="Password" type="password">
			<div class="checkbox-block checkbox-after no-space">
				<input type="checkbox" name="" id="checkbox-1" class="remember-login">
				<label for="checkbox-1">Remember me</label>
			</div>
			<div class="btn-wrap">
				<div class="login-button button">Sign In</div>
			</div>
		</div>
		<?php
	}
	?>
	</div>
	<?php
	}

	require_once 'template/footer.php';
	$sqlc->close();
?>