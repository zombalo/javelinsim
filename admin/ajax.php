<?php
	require_once 'config.php';
	require_once 'template/lists.php';

	if($__AUTH_TRIGGER == false && isset($_POST['admin_action']) && $_POST['admin_action'] != 'login')
		exit();

	if(isset($_POST['admin_action']))
		$query_type = $_POST['admin_action'];
	else if(isset($_GET['admin_action']))
		$query_type = $_GET['admin_action'];

	if(isset($query_type) && $query_type != 'login')
	{
		$query_data = mysqli_fetch_assoc($sqlc->query("SELECT * FROM ajax_queries WHERE query = '{$query_type}'"));
		$user_page_rules = mysqli_fetch_assoc($sqlc->query("SELECT * 
			FROM admin_user_rules 
			WHERE page_id = '{$query_data['page_id']}' 
			AND user_id = '{$__USER_ID__}'"));

		if($user_page_rules['visibility'] == '0')
		{
			echo 'permissions_halt';
			exit();
		}
		else if($user_page_rules['visibility'] == '1')
		{
			if($user_page_rules['type'] == 'read_only' && $query_data['type'] == 'write')
			{
				echo 'permissions_halt';
				exit();
			}
		}
	}

	function copy_score_details($old, $new)
	{
		global $sqlc;

		$sqlc->query("INSERT INTO score_details (
			score_id, title, description, ordering
			) SELECT '{$new}', title, description, ordering
			FROM score_details WHERE score_id = '{$old}'");
	}

	function copy_test_scores($old_id, $new_id)
	{
		global $sqlc;
		$questions = $sqlc->query("SELECT id FROM questions WHERE test_id = '{$old_id}'");
		$new_questions = $sqlc->query("SELECT id FROM questions WHERE test_id = '{$new_id}'");

		foreach($questions as $question)
		{
			$new_question = mysqli_fetch_assoc($new_questions);
			$old_scores = $sqlc->query("SELECT id FROM question_scores WHERE question_id = '{$question['id']}'");
			foreach($old_scores as $old_score)
			{
				$sqlc->query("INSERT INTO question_scores (
					question_id,
					response,
					score_text,
					value,
					ordering,
					single_list)
					SELECT 
					'{$new_question['id']}',
					response,
					score_text,
					value,
					ordering,
					single_list
					FROM question_scores WHERE id = '{$old_score['id']}'");

				$new_score = mysqli_fetch_assoc($sqlc->query("SELECT id 
					FROM question_scores 
					ORDER BY id DESC 
					LIMIT 1"))['id'];

				copy_score_details($old_score['id'], $new_score);
			}
		}
	}

	function copy_question_scores($old_id, $new_id)
	{
		global $sqlc;

		$old_scores = $sqlc->query("SELECT id FROM question_scores WHERE question_id = '{$old_id}'");
		foreach($old_scores as $old_score)
		{
			$sqlc->query("INSERT INTO question_scores (
				question_id,
				response,
				score_text,
				value,
				ordering,
				single_list)
				SELECT 
				'{$new_id}',
				response,
				score_text,
				value,
				ordering,
				single_list
				FROM question_scores WHERE id = '{$old_score['id']}'");

			$new_score = mysqli_fetch_assoc($sqlc->query("SELECT id 
					FROM question_scores 
					ORDER BY id DESC 
					LIMIT 1"))['id'];

			copy_score_details($old_score['id'], $new_score);
		}
	}

	if(isset($_POST['admin_action']))
	{
		//Section globals goes here
		$cols = ['name', 'email', 'invited', 'completed', 'scored'];
		//And ends here

		switch($_POST['admin_action'])
		{
			case 'email_exists':
			{
				$email = $sqlc->query("SELECT * FROM users WHERE email = '{$_POST['email']}'")->fetch_assoc();
				if($email != NULL)
					echo 'rejected';
				break;
			}
			case 'create_simple_user':
			{
				$name = $sqlc->real_escape_string($_POST['f_name']);
				$sec_name = $sqlc->real_escape_string($_POST['l_name']);
				$email = $sqlc->real_escape_string($_POST['email']);
				$password = hash('sha256', $sqlc->real_escape_string($_POST['password']));

				$invited = 'no';
				if($_POST['assign_sim'] == 'true')
					$invited = 'yes';
				
				$sqlc->query("INSERT INTO users (name, sec_name, email, password, invited) VALUES(
					'{$name}',
					'{$sec_name}',
					'{$email}',
					'{$password}',
					'{$invited}'
				)");

				$sql_row = $sqlc->query("SELECT * FROM users ORDER BY id DESC LIMIT 1")->fetch_assoc();

				if($_POST['send_email'] == 'true')
				{
					$message = '<html><body>';
					$message .= '<p style="font-size: 18px;">Hello ' . $_POST['f_name'] . '.<br>';
					$message .= 'Welcome to Javelin Sim! You are now able to login and use the platform.<br><br>';
					$message .= 'Your username is: ' . $_POST['email'] . '<br>';
					$message .= 'Password is: ' . $_POST['password'] . '<br><br>';
					$message .= 'Once a simulation has been added to your account you will be able to start and complete it.<br><br>';
					$message .= 'This website is also where you will come to view your feedback from the simulation.<br><br>';
					$message .= 'If you have any questions please email questions@javelinsim.com.<br><br>';
					$message .= 'Good luck<br>';
					$message .= 'Javelin Sim';
					$message .= "</p></body></html>";

					$to = strip_tags($_POST['email']);
					$subject = 'Your has invited to JavelinSim';

					$headers = "From:  welcome@javelinsim.com\r\n";
					$headers .= "MIME-Version: 1.0\r\n";
					$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
					mail($to, $subject, $message, $headers);
				}

				if($_POST['assign_sim'] == 'true')
				{
					$time = strtotime($_POST['month'] . '/' . $_POST['day'] . '/' . $_POST['year']);

					$first_question = mysqli_fetch_assoc($sqlc->query("SELECT id 
						FROM questions 
						WHERE test_id = '{$_POST['test']}' 
						ORDER BY ordering LIMIT 1"))['id'];

					$current_time = time();

					$sqlc->query("DELETE FROM answers WHERE user_id = '{$sql_row['id']}' AND test_id = '{$_POST['test']}'");
					$sqlc->query("DELETE FROM user_tests WHERE user_id = '{$sql_row['id']}' AND test_id = '{$_POST['test']}'");


					$sqlc->query("INSERT INTO user_test_invites (user_id, test_id, date) VALUES(
						'{$sql_row['id']}',
						'{$_POST['test']}',
						'{$current_time}'
					)");

					$sqlc->query("INSERT INTO user_tests (user_id, test_id, question_id, required_date) VALUES(
						'{$sql_row['id']}',
						'{$_POST['test']}',
						'{$first_question}',
						'{$time}'
					)");
				}

				?>
				<div user-id="<?php echo $sql_row['id'] ?>" class="list-item user-list-item result-list-item">
					<div class="assign-check-wrap">
						<div class="assign-checkbox-block">
							<input disabled type="checkbox" id="checkbox-<?php echo $sql_row['id'] ?>" class="user-list-checkbox">
							<label for="checkbox-<?php echo $sql_row['id'] ?>"></label>
						</div>
						<div class="tested-badge">
						</div>
					</div>
					<a href="index.php?page=user&id=<?php echo $sql_row['id'] ?>" class="fa-item">
						<div class="row">
							<div class="col-sm-3 list-col"><?php echo $sql_row['name'] . ' ' . $sql_row['sec_name'] ; ?></div>
							<div class="col-sm-3 list-col"><?php echo $sql_row['email'] ?></div>
							<div class="col-sm-2 list-col"><?php echo $sql_row['invited'] ?></div>
							<div class="col-sm-2 list-col"><?php echo $sql_row['completed'] ?></div>
							<div class="col-sm-2 list-col"><?php echo $sql_row['scored'] ?></div>
						</div>
					</a>
				</div>
				<?php

				break;
			}
			case 'login':
			{
				$pass = hash('sha256', $_POST['pass']);
				$res = $sqlc->query("SELECT user_hash FROM admin_users WHERE email = '{$_POST['email']}' AND password = '{$pass}'");
				
				if($res->num_rows != 0)
				{
					// hash('sha256', md5('1') . md5('admin') . bin2hex(random_bytes(5))); (user hash)

					$user_hash = mysqli_fetch_assoc($res)['user_hash'];
					$user_token = hash('sha256', $user_hash) . md5($_SERVER['HTTP_USER_AGENT']);

					if($_POST['remember'] == 'true')
					{
						// 31536000 = year
						setcookie('admin_hash', $user_hash, time() + 31536000);
						setcookie('admin_auth_token', $user_token, time() + 31536000);
					}
					else
					{
						setcookie('admin_hash', $user_hash);
						setcookie('admin_auth_token', $user_token);
					}

					echo 'success';
				}

				break;
			}
			case 'add_test':
			{
				$test_name = $sqlc->real_escape_string($_POST['name']);
				$last_order = intval(mysqli_fetch_assoc($sqlc->query("SELECT ordering 
					FROM tests 
					ORDER BY ordering DESC 
					LIMIT 1"))['ordering']) + 1;

				$use_template = '';
				if($_POST['use_template'] == 'true')
					$use_template = '1';
				if($sqlc->query("INSERT INTO tests (name, ordering, use_template) VALUES ('{$test_name}', '{$last_order}', '{$use_template}')") == true)
					echo 'success';
				break;
			}
			case 'save_test':
			{
				$test_name = $sqlc->real_escape_string($_POST['test_name']);
				$sqlc->query("UPDATE tests SET 
				name = '{$test_name}'
				WHERE id = '{$_POST['test_id']}'");

				echo 'success';
				break;
			}
			case 'save_question':
			{
				$score_mode = mysqli_fetch_assoc($sqlc->query("SELECT scoring_list_mode FROM questions WHERE id = '{$_POST['question_id']}'"))['scoring_list_mode'];

				if($score_mode == 'checked')
					$single_list = '1';
				else
					$single_list = '0';

				//Saving scores
				if(isset($_POST['removed']))
				{
					for($i = 0; $i < count($_POST['removed']); $i++)
					{
						$sqlc->query("DELETE FROM question_scores WHERE id = '{$_POST['removed'][$i]}'");
						$sqlc->query("DELETE FROM score_details WHERE score_id  = '{$_POST['removed'][$i]}'");
					}
				}

				//NOTICE
				//Make also checking of users scores after removing scores from question

				if(isset($_POST['ordering']))
				{
					for($i = 0; $i < count($_POST['ordering']); $i++)
					{
						$sqlc->query("UPDATE question_scores 
							SET ordering = '{$_POST['ordering'][$i]['ordering']}' 
							WHERE id = '{$_POST['ordering'][$i]['id']}'");
					}
				}

				if(isset($_POST['changed']))
				{
					for($i = 0; $i < count($_POST['changed']); $i++)
					{
						$proto = $_POST['changed'][$i];
						if(isset($proto['text']))
						{
							$sqlc->query("UPDATE question_scores SET score_text = '{$proto['text']}' WHERE id = '{$proto['id']}'");
						}

						if(isset($proto['value']))
						{
							$value_buffer = round(floatval(str_replace(',', '.', $proto['value'])), 2);
							$sqlc->query("UPDATE question_scores SET value = '{$value_buffer}' WHERE id = '{$proto['id']}'");
						}
					}
				}

				if(isset($_POST['new']))
				{
					for($i = 0; $i < count($_POST['new']); $i++)
					{
						if($_POST['new'][$i][0] == 'none')
							continue;

						for($j = 0; $j < count($_POST['new'][$i]); $j++)
						{
							$proto = $_POST['new'][$i][$j];
							$buf = $i+1;
							$title_buffer = $sqlc->real_escape_string($proto['text']);
							$value_buffer = round(floatval(str_replace(',', '.', $proto['value'])), 2);
							$sqlc->query("INSERT INTO question_scores (question_id, score_text, value, ordering, response, single_list) VALUES (
								'{$_POST['question_id']}',
								'{$title_buffer}',
								'{$value_buffer}',
								'{$proto['ordering']}',
								'{$buf}',
								'{$single_list}'
								)");

							$test_id = mysqli_fetch_assoc($sqlc->query("SELECT test_id FROM questions WHERE id = '{$_POST['question_id']}'"))['test_id'];
							$last_id = mysqli_fetch_assoc($sqlc->query("SELECT id FROM question_scores ORDER BY id DESC LIMIT 1"))['id'];

							$presets = $sqlc->query("SELECT * FROM test_presets WHERE test_id = '{$test_id}'");
							$p = 1;
							foreach($presets as $preset)
							{
								$detail_title_buffer = $sqlc->real_escape_string($preset['preset_text']);
								$sqlc->query("INSERT INTO score_details (score_id, title, ordering) VALUES(
									'{$last_id}',
									'{$detail_title_buffer}',
									'{$p}'
									)");
								$p++;
							}
						}
					}
				}

				//Scoring mode

				for($i = 0; $i < count($_POST['scoring_modes']); $i++)
				{
					$col_name = 'scoring_mode_' . strval($i + 1);
					$sqlc->query("UPDATE questions SET {$col_name} = '{$_POST['scoring_modes'][$i]}' WHERE id = '{$_POST['question_id']}'");
				}

				//Saving question properties

				$item_number = $sqlc->real_escape_string($_POST['item_num']);
				$title = $sqlc->real_escape_string($_POST['item_title']);
				$context = $sqlc->real_escape_string($_POST['item_cont']);
				$question = $sqlc->real_escape_string($_POST['item_ques']);

				//Counting slides for user answers
				$slides_count = 0;
				if($_POST['rec_mode'] != 'None')
					$slides_count++;

				if($_POST['aar']) //Answer after review
					$slides_count++;

				if($_POST['afsr']) // Answer after strong
					$slides_count++;

				if($_POST['rec_mode'] == 'None')
					$slides_count = 0;


				//Saving data to database

				$for_true = $sqlc->query("UPDATE questions SET 
				item_number = '{$item_number}',
				context = '{$context}',
				question = '{$question}',
				title = '{$title}',
				mins = '{$_POST['mins']}',
				secs = '{$_POST['secs']}',
				rerecord = '{$_POST['rec_mode']}',
				rev_of_ans = '{$_POST['roa']}',
				ans_af_rev = '{$_POST['aar']}',
				ans_af_str = '{$_POST['afsr']}',
				strong_require = '{$_POST['rs']}',
				answer_slides = '{$slides_count}',
				scoring_list_mode = '{$_POST['scoring_list_mode']}'
				WHERE id = '{$_POST['question_id']}'");

				if($for_true == true)
				{
					echo 'success';
				}
				break;
			}
			case 'create_question':
			{
				$order = mysqli_fetch_assoc($sqlc->query("SELECT ordering 
					FROM questions 
					WHERE test_id = '{$_POST['test_id']}' 
					ORDER BY ordering DESC 
					LIMIT 1"))['ordering'];

				$order = intval($order);
				$order++;

				$sqlc->query("INSERT INTO questions (test_id, ordering) VALUES(
					'{$_POST['test_id']}',
					'{$order}'
				)");

				$last_id = mysqli_fetch_assoc($sqlc->query("SELECT id FROM questions ORDER BY id DESC LIMIT 1"))['id'];

				$use_template = $sqlc->query("SELECT use_template FROM tests WHERE id = '{$_POST['test_id']}'")->fetch_assoc()['use_template'];

				if($use_template == '1')
				{
					$sqlc->query("INSERT INTO question_scores (question_id, response, single_list, score_text, value, ordering)
						SELECT '{$last_id}', '1', '1', score_title, score_value, score_ordering FROM score_presets ORDER BY score_ordering
						");

					$scores = $sqlc->query("SELECT id, ordering FROM question_scores WHERE question_id = '{$last_id}' ORDER BY ordering");

					foreach($scores as $score)
					{
						$sqlc->query("INSERT INTO score_details (score_id, title, description, ordering)
							SELECT '{$score['id']}', preset_title, preset_value, preset_ordering FROM score_presets WHERE score_ordering = '{$score['ordering']}'
							");

						$sqlc->error;
					}
				}

				echo $last_id;
				break;
			}
			case 'remove_question':
			{
				$sqlc->query("DELETE FROM score_details 
					WHERE score_id IN 
					(SELECT id as score_id 
					FROM question_scores 
					WHERE question_id = '{$_POST['id']}')");

				$sqlc->query("DELETE FROM question_scores WHERE question_id = '{$_POST['id']}'");
				$sqlc->query("DELETE FROM answers WHERE question_id = '{$_POST['id']}'");
				$sqlc->query("DELETE FROM user_scores WHERE question_id = '{$_POST['id']}'");
				$sqlc->query("DELETE FROM text_scores WHERE question_id = '{$_POST['id']}'");

				$question = $sqlc->query("SELECT test_id, ordering 
					FROM questions 
					WHERE id = '{$_POST['id']}'")->fetch_assoc();

				$sqlc->query("UPDATE questions 
					SET ordering = ordering - 1 
					WHERE test_id = '{$question['test_id']}' 
					AND ordering > '{$question['ordering']}' 
					ORDER BY ordering");

				$sqlc->query("DELETE FROM questions WHERE id = '{$_POST['id']}'");

				echo 'success';

				break;
			}
			case 'remove_test':
			{
				// $questions = $sqlc->query("SELECT id FROM questions WHERE test_id = '{$_COOKIE['id_buffer']}'");

				// foreach($questions as $question)
				// {

				// 	$q_token = mysqli_fetch_assoc($sqlc->query("SELECT question_token FROM questions WHERE id = '{$question['id']}'"))['question_token'];
				// 	if($q_token != '')
				// 	{
				// 		$ziggeo->videos()->delete($q_token);
				// 	}

				// 	$s_token = mysqli_fetch_assoc($sqlc->query("SELECT strong_token FROM questions WHERE id = '{$question['id']}'"))['strong_token'];
				// 	if($s_token != '')
				// 	{
				// 		$ziggeo->videos()->delete($s_token);
				// 	}
					
				// 	$sqlc->query("DELETE FROM questions WHERE id = '{$question['id']}'");
				// }

				// $sqlc->query("DELETE FROM tests WHERE id = '{$_COOKIE['id_buffer']}'");
				// echo 'success';
				break;
			}
			case 'started_users':
			{
				$st_users = $sqlc->query("SELECT DISTINCT user_id FROM answers")->num_rows;
				$users_count = $sqlc->query("SELECT id FROM users")->num_rows;
				$users_count -= $st_users;

				echo json_encode(['notReady' => $users_count, 'ready' => $st_users]);
				break;
			}
			case 'get_visits':
			{
				$mounth = date('n');
				$mounth_name = date('F');
				$year = date('o');

				$sql_res = $sqlc->query("SELECT DISTINCT day FROM visitors WHERE mounth = '{$mounth}' AND year = '{$year}' ORDER BY day");

				$visits = array();

				foreach($sql_res as $sql_row)
				{
					$count = $sqlc->query("SELECT DISTINCT ip, user_id FROM visitors WHERE day = '{$sql_row['day']}' AND mounth = '{$mounth}' AND year = '{$year}'")->num_rows;
					array_push($visits, ['day' => $sql_row['day'], 'count' => $count, 'mounth' => $mounth_name]);
				}

				echo json_encode($visits);

				break;
			}
			case 'get_questions':
			{
				$questions = mysqli_fetch_all($sqlc->query("SELECT id, title FROM questions WHERE test_id = '{$_POST['test']}' ORDER BY id"), MYSQLI_ASSOC);

				echo json_encode($questions);

				break;
			}
			case 'order_up':
			{
				$id = $_POST['id'];

				switch($_POST['list_type'])
				{
					case 'test':
					{
						$current = mysqli_fetch_assoc($sqlc->query("SELECT id, ordering FROM tests WHERE id = '{$id}'"));
						$prev = mysqli_fetch_assoc($sqlc->query("SELECT id, ordering 
							FROM tests 
							WHERE ordering < '{$current['ordering']}' 
							ORDER BY ordering DESC LIMIT 1"));

						$sqlc->query("UPDATE tests SET ordering = '{$current['ordering']}' WHERE id = '{$prev['id']}'");
						$sqlc->query("UPDATE tests SET ordering = '{$prev['ordering']}' WHERE id = '{$current['id']}'");

						break;
					}
					case 'question':
					{
						$test_id = mysqli_fetch_assoc($sqlc->query("SELECT test_id FROM questions WHERE id = '{$id}'"))['test_id'];

						$current = mysqli_fetch_assoc($sqlc->query("SELECT id, ordering 
							FROM questions 
							WHERE id = '{$id}' AND test_id = '{$test_id}'"));

						$prev = mysqli_fetch_assoc($sqlc->query("SELECT id, ordering 
							FROM questions 
							WHERE ordering < '{$current['ordering']}' AND test_id = '{$test_id}'
							ORDER BY ordering DESC LIMIT 1"));

						$sqlc->query("UPDATE questions 
							SET ordering = '{$current['ordering']}' 
							WHERE id = '{$prev['id']}'");

						$sqlc->query("UPDATE questions 
							SET ordering = '{$prev['ordering']}' 
							WHERE id = '{$current['id']}'");

						break;
					}
				}

				echo 'success';
				break;
			}
			case 'order_down':
			{
				$id = $_POST['id'];

				switch($_POST['list_type'])
				{
					case 'test':
					{
						$current = mysqli_fetch_assoc($sqlc->query("SELECT id, ordering FROM tests WHERE id = '{$id}'"));
						$next = mysqli_fetch_assoc($sqlc->query("SELECT id, ordering 
							FROM tests 
							WHERE ordering > '{$current['ordering']}' 
							ORDER BY ordering LIMIT 1"));

						$sqlc->query("UPDATE tests SET ordering = '{$current['ordering']}' WHERE id = '{$next['id']}'");
						$sqlc->query("UPDATE tests SET ordering = '{$next['ordering']}' WHERE id = '{$current['id']}'");

						break;
					}
					case 'question':
					{
						$test_id = mysqli_fetch_assoc($sqlc->query("SELECT test_id FROM questions WHERE id = '{$id}'"))['test_id'];

						$current = mysqli_fetch_assoc($sqlc->query("SELECT id, ordering 
							FROM questions 
							WHERE id = '{$id}' AND test_id = '{$test_id}'"));

						$next = mysqli_fetch_assoc($sqlc->query("SELECT id, ordering 
							FROM questions 
							WHERE ordering > '{$current['ordering']}' AND test_id = '{$test_id}'
							ORDER BY ordering LIMIT 1"));

						$sqlc->query("UPDATE questions 
							SET ordering = '{$current['ordering']}' 
							WHERE id = '{$next['id']}'");

						$sqlc->query("UPDATE questions 
							SET ordering = '{$next['ordering']}' 
							WHERE id = '{$current['id']}'");

						break;
					}
				}

				echo 'success';
				break;
			}
			case 'save_user_score':
			{
				$sqlc->query("DELETE FROM user_scores WHERE 
					user_id = '{$_POST['user']}' 
					AND question_id = '{$_POST['question']}' 
					AND response = '{$_POST['response']}'");

				for($i = 0; $i < count($_POST['scores']); $i++)
				{
					$sqlc->query("INSERT INTO user_scores (test_id, question_id, user_id, score_id, response) VALUES(
						'{$_POST['test']}',
						'{$_POST['question']}',
						'{$_POST['user']}',
						'{$_POST['scores'][$i]}',
						'{$_POST['response']}'
						)");
				}

				echo 'success';
				break;
			}
			case 'save_user_score_comment':
			{
				$sqlc->query("DELETE FROM text_scores WHERE user_id = '{$_POST['user']}' AND question_id = '{$_POST['question']}' AND response = '{$_POST['response']}'");

				$escape = $sqlc->real_escape_string($_POST['comment']);

				if($escape == '')
				{
					echo 'success';
					break;
				}

				$sqlc->query("INSERT INTO text_scores (test_id, question_id, user_id, response, rate) VALUES (
						'{$_POST['test']}',
						'{$_POST['question']}',
						'{$_POST['user']}',
						'{$_POST['response']}',
						'{$escape}'
					)");

				echo 'success';
				break;
			}
			case 'sort_users_list':
			{
				$sort_col = $cols[$_POST['col']];

				$direction = ' ORDER BY ' . $sort_col;
				$sec_name = '';

				if($_POST['direction'] == '2')
				{
					$direction .= ' DESC';
				}

				if($_POST['col'] == 0)
				{
					$sec_name = " OR sec_name LIKE '%{$_POST['keyword']}%'";
				}

				$query = "SELECT id, name, sec_name, email, completed, invited, scored FROM users WHERE {$sort_col} LIKE '%{$_POST['keyword']}%'" . $sec_name;


				if($_POST['direction'] != '0')
				{
					$query .= $direction;
				}

				$sql_res = mysqli_fetch_all($sqlc->query($query), MYSQLI_ASSOC);
				$results = array();

				//Additional search filters
				if($_POST['addtIndex'] != '-1')
				{
					//Filter for invited to simulation users
					switch($_POST['addtIndex'])
					{
						case '0':
						{
							$invites = mysqli_fetch_all($sqlc->query("SELECT user_id 
								FROM invite_count 
								WHERE test_id = '{$_POST['addtTest']}'"), MYSQLI_ASSOC);

							for($i = 0; $i < count($sql_res); $i++)
							{
								$add_trigger = false;
								for($j = 0; $j < count($invites); $j++)
								{
									if($sql_res[$i]['id'] == $invites[$j]['user_id'])
									{
										$add_trigger = true;
									}
								}

								if($add_trigger)
								{
									array_push($results, $sql_res[$i]);
								}
							}

							$sql_res = $results;
							break;
						}
						case '1':
						{
							$completed_users = array();
							$question_count = $sqlc->query("SELECT id 
								FROM questions 
								WHERE test_id = '{$_POST['addtTest']}'")->num_rows;
							$users = $sqlc->query("SELECT DISTINCT user_id AS id 
								FROM answers 
								WHERE test_id = '{$_POST['addtTest']}'");

							foreach($users as $user)
							{
								$completed_count = $sqlc->query("SELECT id 
									FROM answers 
									WHERE test_id = '{$_POST['addtTest']}' 
									AND user_id = '{$user['id']}'
									AND state = 'finished'")->num_rows;

								if($completed_count == $question_count)
									array_push($completed_users, $user['id']);
							}

							for($i = 0; $i < count($sql_res); $i++)
							{
								$add_trigger = false;
								for($j = 0; $j < count($completed_users); $j++)
								{
									if($sql_res[$i]['id'] == $completed_users[$j]['id'])
									{
										$add_trigger = true;
									}
								}

								if($add_trigger)
								{
									array_push($results, $sql_res[$i]);
								}
							}

							$sql_res = $results;

							break;
						}
						case '2':
						{
							$scored_users = array();
							$users = $sqlc->query("SELECT DISTINCT user_id AS id 
								FROM answers WHERE test_id = '{$_POST['addtTest']}'");

							$questions = $sqlc->query("SELECT id, answer_slides
								FROM questions
								WHERE test_id = '{$_POST['addtTest']}'");

							foreach($users as $user)
							{
								$questions->data_seek(0);

								foreach($questions as $question)
								{
									$value_scores = mysqli_fetch_all($sqlc->query("SELECT DISTINCT response, question_id
										FROM user_scores
										WHERE user_id = '{$user['id']}'
										AND question_id = '{$question['id']}}'
										ORDER BY response"), MYSQLI_ASSOC);

									$text_scores = mysqli_fetch_all($sqlc->query("SELECT DISTINCT response, question_id
										FROM text_scores
										WHERE user_id = '{$user['id']}'
										AND question_id = '{$question['id']}}'
										ORDER BY response"), MYSQLI_ASSOC);

									$result_scores = array_unique(array_merge($value_scores, $text_scores), SORT_REGULAR);
								}

								if(count($result_scores) >= intval($question['answer_slides']))
									array_push($scored_users, ['id' => $user['id']]);
							}

							for($i = 0; $i < count($sql_res); $i++)
							{
								$add_trigger = false;
								for($j = 0; $j < count($scored_users); $j++)
								{
									if($sql_res[$i]['id'] == $scored_users[$j]['id'])
									{
										$add_trigger = true;
									}
								}

								if($add_trigger)
								{
									array_push($results, $sql_res[$i]);
								}
							}

							$sql_res = $results;

							break;
						}
					}
				}

				echo json_encode($sql_res);

				break;
			}
			case 'save_user_data':
			{
				$email = $sqlc->query("SELECT id, email
					FROM users
					WHERE email = '{$_POST['data'][2]}'
					AND id <> '{$_POST['user']}'")->fetch_assoc();

				if($email != NULL)
				{
					echo json_encode($sqlc->query("SELECT name, sec_name, email 
						FROM users 
						WHERE id = '{$_POST['user']}'")->fetch_all(MYSQLI_ASSOC));
				}
				else
				{
					$sqlc->query("UPDATE users 
						SET name = '{$_POST['data'][0]}', 
						sec_name = '{$_POST['data'][1]}', 
						email = '{$_POST['data'][2]}' 
						WHERE id = '{$_POST['user']}'");

					echo 'success';
				}
				break;
			}
			case 'update_link_token_data':
			{
				$sqlc->query("UPDATE invite_tokens
					SET test_id = '{$_POST['test']}'
					WHERE id = '{$_POST['id']}'");

				echo 'success';

				break;
			}
			case 'remove_invite_token':
			{
				$sqlc->query("DELETE FROM invite_tokens WHERE id = '{$_POST['id']}'");
				echo 'success';
				break;
			}
			case 'create_link_token':
			{
				while(true)
				{
					$token = bin2hex(random_bytes(7));
					if($sqlc->query("SELECT token FROM invite_tokens WHERE token = '{$token}'")->num_rows == 0)
					{
						$sqlc->query("INSERT INTO invite_tokens (token, test_id) 
							VALUES('{$token}', '{$_POST['test']}')");
						break;
					}
				}
				$output =  mysqli_fetch_assoc($sqlc->query("SELECT id, token FROM invite_tokens ORDER BY id DESC LIMIT 1"));
				$output['data'] = 'success';
				echo json_encode($output);

				break;
			}
			case 'copy_test':
			{
				$test_proto = mysqli_fetch_assoc($sqlc->query("SELECT name, score_select FROM tests WHERE id = '{$_POST['test']}'"));
				$order = intval(mysqli_fetch_assoc($sqlc->query("SELECT ordering FROM tests ORDER BY ordering DESC LIMIT 1"))['ordering']);
				$order = $order + 1;
				$sqlc->query("INSERT INTO tests (name, ordering, score_select) VALUES('{$test_proto['name']}', '{$order}', '{$test_proto['score_select']}')");
				$response = mysqli_fetch_assoc($sqlc->query("SELECT id, name, created, takers, ordering FROM tests ORDER BY id DESC LIMIT 1"));

				$questions = $sqlc->query("SELECT id FROM questions WHERE test_id = '{$_POST['test']}'");
				foreach($questions as $question)
				{
					$sqlc->query("INSERT INTO questions (
						title, test_id, ordering, item_number,
						 context, question, scoring_mode_1, scoring_mode_2,
						  scoring_mode_3, question_token, strong_require,
						   strong_token, rev_of_ans, ans_af_rev,
						    ans_af_str, mins, secs, rerecord, for_test, answer_slides
						) 
						SELECT 
						title,
						'{$response['id']}',
						ordering, item_number, context, question,
						 scoring_mode_1, scoring_mode_2, scoring_mode_3,
						  question_token, strong_require, strong_token,
						   rev_of_ans, ans_af_rev, ans_af_str,
						    mins, secs, rerecord, for_test, answer_slides 
						FROM questions 
						WHERE id = '{$question['id']}'");

					$last_question = mysqli_fetch_assoc($sqlc->query("SELECT id FROM questions 
						WHERE test_id = '{$response['id']}' 
						ORDER BY id DESC LIMIT 1"))['id'];
				}

				$new_test = mysqli_fetch_assoc($sqlc->query("SELECT id FROM tests ORDER BY id DESC LIMIT 1"))['id'];

				copy_test_scores($_POST['test'], $new_test);

				$time_buffer = strtotime($response['created']);
				$response['created'] = date('m.d.Y', $time_buffer);

				echo json_encode($response);
				break;
			}
			case 'copy_question':
			{
				$ordering = mysqli_fetch_assoc($sqlc->query("SELECT ordering 
					FROM questions 
					WHERE test_id = '{$_POST['test']}' 
					ORDER BY ordering DESC LIMIT 1"))['ordering'];
				$ordering += 1;

				$sqlc->query("INSERT INTO questions (
					title, test_id, ordering, item_number,
					 context, question, scoring_mode_1,
					  scoring_mode_2, scoring_mode_3, question_token,
					   strong_require, strong_token,
					    rev_of_ans, ans_af_rev, ans_af_str,
					     mins, secs, rerecord, for_test, answer_slides)
					SELECT 
					title, test_id, '{$ordering}', item_number,
					 context, question, scoring_mode_1,
					  scoring_mode_2, scoring_mode_3, question_token,
					   strong_require, strong_token,
					    rev_of_ans, ans_af_rev, ans_af_str,
					     mins, secs, rerecord, for_test, answer_slides
					     FROM questions WHERE id = '{$_POST['question']}'
					");

				$response = mysqli_fetch_assoc($sqlc->query("SELECT id, title 
					FROM questions
					WHERE test_id = '{$_POST['test']}'
					ORDER BY id DESC LIMIT 1"));

				copy_question_scores($_POST['question'], $response['id']);

				echo json_encode($response);
				break;
			}
			case 'assign_users_sims':
			{
				$time = strtotime($_POST['month'] . '/' . $_POST['day'] . '/' . $_POST['year']);

				if($time < time())
				{
					echo 'time_error';
					break;
				}

				$first_question = mysqli_fetch_assoc($sqlc->query("SELECT id 
					FROM questions 
					WHERE test_id = '{$_POST['test']}' 
					ORDER BY ordering LIMIT 1"))['id'];

				if($first_question == NULL)
				{
					echo 'question_error';
					break;
				}

				$current_time = time();

				for($i = 0; $i < count($_POST['users']); $i++)
				{
					$sqlc->query("DELETE FROM answers WHERE user_id = '{$_POST['users'][$i]}' AND test_id = '{$_POST['test']}'");
					$sqlc->query("DELETE FROM user_tests WHERE user_id = '{$_POST['users'][$i]}' AND test_id = '{$_POST['test']}'");


					$sqlc->query("INSERT INTO user_test_invites (user_id, test_id, date) VALUES(
						'{$_POST['users'][$i]}',
						'{$_POST['test']}',
						'{$current_time}'
					)");

					$sqlc->query("INSERT INTO user_tests (user_id, test_id, question_id, required_date) VALUES(
						'{$_POST['users'][$i]}',
						'{$_POST['test']}',
						'{$first_question}',
						'{$time}'
					)");

				}

				echo 'success';
				break;
			}
			case 'check_user_tested':
			{
				$tested = false;

				$user_test = $sqlc->query("SELECT id FROM answers WHERE user_id = '{$_POST['user']}' AND test_id = '{$_POST['test']}'");
				if($user_test->num_rows > 0)
					$tested = true;

				$user_invites = $sqlc->query("SELECT id FROM user_tests WHERE user_id = '{$_POST['user']}' AND test_id = '{$_POST['test']}'");
				if($user_invites->num_rows > 0)
					$tested = true;

				if($tested == true)
					echo 'success';

				break;
			}
			case 'admin_password_check':
			{
				$old_password = hash('sha256', $_POST['password']);

				if($sqlc->query("SELECT id FROM admin_users WHERE password = '{$old_password}'")->num_rows > 0)
				{
					echo 'success';
				}
				else
				{
					echo 'error';
				}

				break;
			}
			case 'change_admin_password':
			{
				$new_password = hash('sha256', $_POST['password']);
				$sqlc->query("UPDATE admin_users SET password = '{$new_password}' WHERE email = 'admin'");
				setcookie('core_password', $new_password, time()+2592000);
				echo $new_password;
				break;
			}
			case 'leave_admin_panel':
			{
				setcookie('admin_hash', '', time() - 100);
				setcookie('admin_auth_token', '', time() - 100);

				echo 'success';
				break;
			}
			case 'save_admin_settings':
			{
				//save practice question data
				$sqlc->query("UPDATE questions SET for_test = '0' WHERE for_test = '1'");
				$sqlc->query("UPDATE questions SET for_test = '1' WHERE id = '{$_POST['practice_question']}'");

				//save start question data
				$sqlc->query("UPDATE questions SET for_start = '0' WHERE for_start = '1'");
				$sqlc->query("UPDATE questions SET for_start = '1' WHERE id = '{$_POST['start_questions']}'");
				echo 'success';
				break;
			}
			case 'get_score_details':
			{
				$res = $sqlc->query("SELECT * FROM score_details WHERE score_id = '{$_POST['score_id']}' ORDER BY ordering");

				echo json_encode(mysqli_fetch_all($res, MYSQLI_ASSOC));

				break;
			}
			case 'save_score_details':
			{
				for($i = 0; $i < count($_POST['removed']); $i++)
				{
					$sqlc->query("DELETE FROM score_details WHERE id = '{$_POST['removed'][$i]}'");
				}

				for($i = 0; $i < count($_POST['new']); $i++)
				{
					$sqlc->query("INSERT INTO score_details (score_id, ordering, title, description) VALUES(
						'{$_POST['score_id']}',
						'{$_POST['new'][$i]['ordering']}',
						'{$_POST['new'][$i]['title']}',
						'{$_POST['new'][$i]['desc']}'
					)");
				}

				for($i = 0; $i < count($_POST['changed']); $i++)
				{
					$sqlc->query("UPDATE score_details 
						SET ordering = '{$_POST['changed'][$i]['ordering']}', 
						title = '{$_POST['changed'][$i]['title']}', 
						description = '{$_POST['changed'][$i]['desc']}' 
						WHERE id = '{$_POST['changed'][$i]['id']}'");
				}

				echo 'success';
				break;
			}
			case 'copy_score_item':
			{
				$ordering = $sqlc->query("SELECT ordering FROM 
					question_scores 
					WHERE question_id = '{$_POST['question']}'
					AND response = '{$_POST['response']}'
					ORDER BY ordering DESC LIMIT 1");

				if($ordering->num_rows == 0)
					$ordering = '1';
				else
					$ordering = intval(mysqli_fetch_assoc($ordering)['ordering']) + 1;

				$sqlc->query("INSERT INTO question_scores (
						question_id, response, score_text, value, ordering)
					SELECT 
						'{$_POST['question']}', '{$_POST['response']}', score_text, value, '{$ordering}'
					FROM question_scores WHERE id = '{$_POST['item']}'
					");

				$new_score = mysqli_fetch_assoc($sqlc->query("SELECT id 
					FROM question_scores 
					ORDER BY id DESC 
					LIMIT 1"))['id'];

				$score_details = $sqlc->query("SELECT id 
					FROM score_details 
					WHERE score_id = '{$_POST['item']}'");

				foreach($score_details as $score_detail)
				{
					$sqlc->query("INSERT INTO score_details (
						score_id, title, description, ordering, single_list
						) SELECT '{$new_score}', title, description, ordering, single_list
						FROM score_details WHERE id = '{$score_detail['id']}'");
				}

				echo json_encode(mysqli_fetch_assoc($sqlc->query("
					SELECT id, response, score_text, value, ordering 
					FROM question_scores 
					WHERE id = '{$new_score}'")));

				break;
			}
			case 'get_random_hash':
			{
				echo bin2hex(random_bytes(intval($_POST['length']) / 2));
				break;
			}
			case 'check_admin_email':
			{
				$res = $sqlc->query("SELECT email FROM admin_users WHERE email = '{$_POST['email']}'");
				$tokens = $sqlc->query("SELECT email FROM admin_register_tokens WHERE email = '{$_POST['email']}'");
				if($res->num_rows > 0 || $tokens->num_rows > 0)
					echo 'exists';
				else
					echo 'success';

				break;
			}
			case 'create_admin_user':
			{
				//Creating token
				$time = time();
				$sqlc->query("DELETE FROM admin_register_tokens WHERE expire < '{$time}'");

				$expire = time() + 60 * 60 * 24 * 10;
				$pass = hash('sha256', $_POST['password']);
				$token = bin2hex(random_bytes(7));
				$sqlc->query("INSERT INTO admin_register_tokens (email, password, user_mime_type_id, register_token, expire) VALUES(
					'{$_POST['email']}',
					'{$pass}',
					'{$_POST['mime_type']}',
					'{$token}',
					'{$expire}'
					)");

				//Creating rules records in temporary rules table
				$token_id = mysqli_fetch_assoc($sqlc->query("SELECT id 
					FROM admin_register_tokens 
					ORDER BY id DESC 
					LIMIT 1"))['id'];

				for($i = 0; $i < count($_POST['rules']); $i++)
				{
					if($_POST['rules'][$i]['visibility'] == 'true')
						$_POST['rules'][$i]['visibility'] = '1';
					else
						$_POST['rules'][$i]['visibility'] = '0';

					$sqlc->query("INSERT INTO admin_temp_rules (page_id, visibility, type, token_id) 
						VALUES ('{$_POST['rules'][$i]['id']}', '{$_POST['rules'][$i]['visibility']}', '{$_POST['rules'][$i]['rule']}', '{$token_id}')");
				}

				//Sending message to user
				$message = '<html><body>';
				$message .= '<p style="font-size: 18px;">Hello , you had invited to be as administrator on our platform.<br><br>Your login is: ' . $_POST['email'] . '<br>Your password is: ' . $_POST['password'] . '<br>To confirm your account, please visit <a href="http://' . DOMAIN . 
				'/admin?page=confirm-account&token=' . $token . '">this link.</a></p>';
				$message .= "</body></html>";
				
				$to = strip_tags($_POST['email']);
				$subject = 'Confirm your JavelinSim account';
				
				$headers = "From: JavelinSim@mail.com\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				mail($to, $subject, $message, $headers);

				echo 'success';

				break;
			}
			case 'change_user_active':
			{
				//Set "active" to false set 0 or 1 to set true

				$in_users = '';

				$i = 0;
				foreach($_POST['users'] as $user)
				{
					$i++;
					$in_users .= "'" . $user . "'";
					
					if($i < count($_POST['users']))
						$in_users .= ', ';
				}

				$query = "UPDATE admin_users SET active = '{$_POST['state']}' WHERE id IN ({$in_users})";
				$sqlc->query($query);

				echo 'success';
				break;
			}
			case 'remove_admin_users':
			{
				foreach($_POST['users'] as $user)
				{
					$sqlc->query("DELETE FROM admin_users WHERE id = '{$user}'");
					$sqlc->query("DELETE FROM admin_user_rules WHERE user_id = '{$user}'");
				}

				echo 'success';
				break;
			}
			case 'change_admin_type':
			{
				foreach($_POST['users'] as $user)
				{
					$sqlc->query("UPDATE admin_users SET user_mime_type_id = '{$_POST['type']}' WHERE id = '{$user}'");
				}

				echo mysqli_fetch_assoc($sqlc->query("SELECT title FROM admin_user_mime_types WHERE id = '{$_POST['type']}'"))['title'];
				break;
			}
			case 'update_admin_email':
			{
				$prev_email = mysqli_fetch_assoc($sqlc->query("SELECT email 
					FROM admin_users 
					WHERE id = '{$_POST['id']}'"))['email'];

				$sqlc->query("UPDATE admin_users SET email = '{$_POST['email']}' WHERE id = '{$_POST['id']}'");

				$message = '<html><body>';
				$message .= '<p style="font-size: 18px;">Hello , your email (Login) for admin account was changed to: ' . $_POST['email'] . '</p>';
				$message .= "</body></html>";

				$to = strip_tags($prev_email);
				$subject = 'Email for JavelinSim account updated';

				$headers = "From: JavelinSim@mail.com\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				mail($to, $subject, $message, $headers);

				break;
			}
			case 'update_admin_password':
			{
				$email = mysqli_fetch_assoc($sqlc->query("SELECT email FROM admin_users WHERE id = '{$_POST['id']}'"))['email'];
				$pass = hash('sha256', $_POST['password']);
				$sqlc->query("UPDATE admin_users SET password = '{$pass}' WHERE id = '{$_POST['id']}'");

				$message = '<html><body>';
				$message .= '<p style="font-size: 18px;">Hello , your password for admin account was changed to: ' . $_POST['password'] . '</p>';
				$message .= "</body></html>";

				$to = strip_tags($email);
				$subject = 'Email for JavelinSim account updated';

				$headers = "From: JavelinSim@mail.com\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				mail($to, $subject, $message, $headers);

				break;
			}
			case 'update_admin_rules':
			{
				$sqlc->query("DELETE FROM admin_user_rules WHERE user_id = '{$_POST['id']}'");

				for($i = 0; $i < count($_POST['rules']); $i++)
				{
					if($_POST['rules'][$i]['visibility'] == 'true')
						$_POST['rules'][$i]['visibility'] = '1';
					else
						$_POST['rules'][$i]['visibility'] = '0';

					$sqlc->query("INSERT INTO admin_user_rules (user_id, page_id, visibility, type) 
						VALUES ('{$_POST['id']}', '{$_POST['rules'][$i]['id']}', '{$_POST['rules'][$i]['visibility']}', '{$_POST['rules'][$i]['rule']}')");
				}

				break;
			}
			case 'remove_user_invite':
			{
				$sqlc->query("DELETE FROM answers WHERE user_id = '{$_POST['user_id']}' AND test_id = '{$_POST['test_id']}'");
				$sqlc->query("DELETE FROM user_tests WHERE user_id = '{$_POST['user_id']}' AND test_id = '{$_POST['test_id']}'");
				$sqlc->query("DELETE FROM user_scores WHERE user_id = '{$_POST['user_id']}' AND test_id = '{$_POST['test_id']}'");
				$sqlc->query("DELETE FROM text_scores WHERE user_id = '{$_POST['user_id']}' AND test_id = '{$_POST['test_id']}'");

				break;
			}
			case 'update_user_invite':
			{
				$time = strtotime($_POST['month'] . '/' . $_POST['day'] . '/' . $_POST['year']);

				if($time < time())
				{
					echo 'time_error';
					break;
				}
				else
				{
					$sqlc->query("UPDATE user_tests SET required_date = '{$time}' WHERE id = '{$_POST['invite_id']}'");

					echo 'success';
				}
				break;
			}
		}
	}
	else if(isset($_GET['admin_action']))
	{
		switch($_GET['admin_action'])
		{
			case 'save_video':
			{
				$arguments = [
				'file' => $_FILES['key']['tmp_name'],
				'min_duration' => '1',
				'max_duration' => '180000',
				'tags' => '',
				'key' => '',
				'volatile' => 'false'];

				$token = $ziggeo->videos()->create($arguments);
				$token = $token->token;

				if($_GET['type'] == 'question')
				{
					$sqlc->query("UPDATE questions SET question_token = '{$token}' WHERE id = '{$_GET['question_id']}'");
				}
				else if($_GET['type'] == 'strong')
				{
					$sqlc->query("UPDATE questions SET strong_token = '{$token}' WHERE id = '{$_GET['question_id']}'");
				}


				echo 'success';

				break;
			}
			case 'update_list':
			{
				$pages = ['tests', 'test', 'answers', 'persons', 'link-tokens', 'admin-rules'];

				switch($_GET['page'])
				{
					case 'tests': 			tests_list(); 						break;
					case 'test': 				questions_list(); 				break;
					case 'answers': 		answers_list(); 					break;
					case 'persons': 		answered_persons_list(); 	break;
					case 'link-tokens': tokens_list(); 						break;
					case 'admin-rules': rules_list(); 						break;
				}
				

				break;
			}
		}
	}

	$sqlc->close();
?>
