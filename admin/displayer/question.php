<?php
	function displayQuestionCheckboxes()
	{
		global $sql_res, $meta;
		$checkbox_classes = 'blur-wrapper click-blocker';

		$checkbox_spec_classes = array(
			'rev_of_ans',
			'ans_af_rev',
			'req_stn',
			'ans_af_str'
		);

		$checkbox_labels = array(
			'Require review of answer',
			'Require answer after review',
			'Require review of strong response',
			'Require answer after strong response'
		);

		$checkbox_temp_data = array(
			$sql_res['rev_of_ans'],
			$sql_res['ans_af_rev'],
			$sql_res['strong_require'],
			$sql_res['ans_af_str']
		);
		?>
		<div class="question-seq-wrap">
		<?php
		for($i = 0; $i < count($checkbox_spec_classes); $i++)
		{
			$disable_temp_class = '';

			if($meta)
			{
				if($i == 1 && $checkbox_temp_data[$i-1] != 'checked')
					$disable_temp_class = $checkbox_classes;
				else if ($i == 3 && $checkbox_temp_data[$i-1] != 'checked')
					$disable_temp_class = $checkbox_classes;
			}
			else
			{
				if($i == 1 || $i == 3)
					$disable_temp_class = $checkbox_classes;
			}

			?>
			<div>
				<div class="checkbox-block checkbox-after <?php echo $disable_temp_class ?>">
					<input <?php echo $checkbox_temp_data[$i] ?> type="checkbox" name="" id="checkbox-<?php echo $i ?>" class="<?php echo $checkbox_spec_classes[$i] ?>">
					<label for="checkbox-<?php echo $i ?>"><?php echo $checkbox_labels[$i] ?></label>
				</div>
			</div>
		<?php
		}
		?>
		</div>
		<?php
	}
?>