var ajax = 'ajax.php';
var user = $.cookie('core_user');
var email_regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var months = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

var getRandom = function(random)
{
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < parseInt(random); i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

var parseGet = function()
{
	var forParse = window.location.search,
	title = '',
	value = '',
	response = {},
	state = true;

	for(var i = 1; i < forParse.length; i++)
	{
		if(state)
			title += forParse[i];
		else
			value +=forParse[i];

		if(forParse[i+1] == '=')
		{
			state = false;
			i++;
		}
		else if(forParse[i+1] == '&' || i+1 == forParse.length)
		{
			i++;
			response[title] = value;
			state = true;
			title = '';
			value = '';
		}
	}

	response['admin_action'] = 'update_list';
	return response;
}

$('.calendar-widget .option').click(function(){
	$(this).closest('.calendar-widget').attr('value', $(this).attr('value'));
	$(this).closest('.calendar-widget').find('.text').text($(this).text());
});

// var pageGet = parseGet();

// var pageHTML = $('.page-list').clone();
// $(pageHTML).find('.list-item').removeClass('loaded-list-item');
// var pageHTML = $(pageHTML).html().replace(/\s/g,'');
// console.log(pageHTML);

// $.get(ajax, pageGet, 
// 	function(data){
// 		data = data.replace(/\s/g,'');
// 		console.log(data);

// 		if(pageHTML !== data)
// 		{
// 			$('.page-list').html(data);
// 		}
// 	});

// setInterval(function(){
// 	var pageHTML = $('.page-list').clone();
// 	$(pageHTML).find('.list-item').removeClass('loaded-list-item');
// 	var pageHTML = $(pageHTML).html().replace(/\s/g, '');
// 	console.log(pageHTML);

// 	$.get(ajax, pageGet, 
// 		function(data){
// 			data = data.replace(/\s/g, '');
// 			console.log(data);
			
// 			if(pageHTML !== data)
// 			{
// 				$('.page-list').html(data);
// 			}
// 		});
// }, 4000);

/*Permisssions halt*/

var permissionsHalt = function(response)
{
	if(response == 'permissions_halt')
	{
		location.href = 'permissions.php?message=You has no permissions to visit this page or perform this action.';
		throw new Error();
	}
};

/*Vertical select widget*/

$('.vertical-select-widget').each(function(){
	$(this).find('.select-mode').css('height', $(this).find('.select-item').first().outerHeight());
});

$('.vertical-select-widget .select-item').click(function(){
	$(this).closest('.vertical-select-widget').attr('value', $(this).attr('value'));
	$(this).siblings('.select-mode').css('top', $(this).position().top);
	$(this).siblings('.select-mode').css('height', $(this).outerHeight());

	$(this).trigger('triggered');
});

/*Hover button*/

$('.hover-button').each(function(){
	var image = 'url(' + $(this).find('.img').attr('img') + ')';
	$(this).find('.img').css('background-image', image);
});

/*Switch loading*/

var switchLoading = function()
{
	var loading = $('.loading-hidden');
	var visibleClass = 'loading-visible'
	if(loading.hasClass(visibleClass)) {
		loading.removeClass(visibleClass);
		loadingAnimation('hidden');
	}
	else {
		loading.addClass(visibleClass);
		$('.progress-bar span').removeClass('hidden');
		loadingAnimation('visible');
		setTimeout(function() {
			loading.removeClass(visibleClass)}, 2000);
	}
};

/*Loading animation*/

function loadingAnimation(param) {
	if(param === 'visible') {
	$('.progress-bar').css('width', '100%');
	}
	else 
	$('.progress-bar').css('width', '0%');
}

/*Trigger widget*/

$('body').on('click', '.trigger-area', function(){
	var item = $(this).parents('.trigger-widget');

	if(item.attr('status') == '')
	{
		item.attr('status', 'checked')
		item.trigger('ontrigger');
	}
	else
	{
		item.attr('status', '');
		item.trigger('offtrigger');
	}

	item.toggleClass('trigger-widget-active');
});

/*Trigger widget ends*/

var getBrowser = function()
{
	var ua= navigator.userAgent, tem,
	M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	if(/trident/i.test(M[1])){
		tem= /\brv[ :]+(\d+)/g.exec(ua) || [];
		return 'IE '+(tem[1] || '');
	}
	if(M[1]=== 'Chrome'){
		tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
		if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
	}
	M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
	if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
	return M.join(' ');
};

var setTokenSize = function()
{
	$('.token-list-item').each(function(){
		var sumWidth = 0;
		$(this).children().each(function(){
			sumWidth += $(this).outerWidth();
			sumWidth += 10;
		});
		sumWidth -= 20;
		sumWidth -= $(this).children('.token-value').outerWidth();
		$(this).children('.token-value').css('width', $(this).width() - sumWidth - 10);
	});
};

$(window).resize(function(){
	setTokenSize();
});

$(document).resize(function(){
	setTokenSize();
});

if($('.dropdown').length > 0)
{
	var dropItemClick = function()
	{
		$(this).parent().siblings('.text').attr('value', $(this).attr('value'));
		$(this).parent().siblings('.text').html($(this).html());
	}

	var activateDropdown = function()
	{
		$(this).addClass('down-active');
	}

	$('body').on('click', '.dropdown .item', dropItemClick);

	$('body').on('click', '.test-choose', function(){
		$('.choose-question').remove();

		if($('.test-chooser .text').attr('value') == '0')
			return;

		$.post(ajax, {
			admin_action: 'get_questions',
			test: $('.test-chooser .text').attr('value')
		}, function(data){
			permissionsHalt(data);
			var questions = JSON.parse(data);

			if(questions.length > 0)
			{
				$('.choose-question-block').css('display', 'table');
				$('.report-download').css('display', 'block');
				setTimeout(function(){
					$('.report-download .icon').css('transform', 'translateX(-50%) scale(1) rotate(0deg)');
				}, 400);
			}
			else
			{
				$('.choose-question-block').css('display', 'none');
				$('.report-cols-wrapper').css('display', 'none');
				$('.report-download').css('display', 'none');
			}

			for(var i = 0; i < questions.length; i++)
			{
				$('.question-chooser .items').append('<div value="' + questions[i].id + 
					'" class="choose-question item">' + questions[i].title + '</div>');
			}
		});
	});

	$('body').on('click', '.choose-question', function(){
		if($('.test-chooser .text').attr('value') != '0')
			$('.report-cols-wrapper').css('display', 'block');
	});
}

var showNotify = function(trigger)
{
	if(trigger)
	{
		$('.main-notify').css('top', '0');
		setTimeout(function(){
			$('.main-notify').css('top', '-100px');
		}, 1500);
	}
	else if($.cookie('notify') == 'show')
	{
		$.removeCookie('notify');
		$('.main-notify').css('top', '0');
		setTimeout(function(){
			$('.main-notify').css('top', '-100px');
		}, 1500);
	}
};

var setNotify = function(string)
{
	if(string.replace(/\d+/g, '') == 'success')
	{
		$.cookie('notify', 'show');
	}
};

function getParameterByName(name, url) {
	if (!url) {
		url = window.location.href;
	}
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
};

var questionChangedTrigger = false;
var scoreDetailChanged = false;

var questionSettingsChangedEvent = function()
{
	questionChangedTrigger = true;
};

var scoreDetailChangedEvent = function()
{
	scoreDetailChanged = true;
};

var onLeaveEvent = function(){
	if(questionChangedTrigger == true)
	{
		return 'Changes on this page could be not saved';
	}
	else if(scoreDetailChanged)
	{
		return 'Changes on this page could be not saved';
	}
};

$(document).ready(function(){
	if($('.question-page-flag').length > 0)
	{
		$(window).on('beforeunload', onLeaveEvent);

		$('.question-description-tab input').on('change', questionSettingsChangedEvent);
		$('.question-description-tab textarea').on('change', questionSettingsChangedEvent);
		$('.scores-data-tab input').on('change', questionSettingsChangedEvent);
		$('.add-score-block .add-button').on('click', questionSettingsChangedEvent);
		$('.score-item .remove-button').on('click', questionSettingsChangedEvent);
		$('.score-item .up-score-button').on('click', questionSettingsChangedEvent);
		$('.score-item .down-score-button').on('click', questionSettingsChangedEvent);
		$('.scoring-mode .item').on('click', questionSettingsChangedEvent);
		$('.add-detail-item-button').on('click', scoreDetailChangedEvent);
	}

	showNotify();

	setTokenSize();

	//TESTS LIST
	var delay = 0, interval = 30;
	$('.list-item').each(function(){
		var buffer = this;
		setTimeout(function(){
			$(buffer).addClass('loaded-list-item');
		}, delay);
		delay += interval;
	});

	$('.top-controls').css({
				'transform': 'translateX(0px)',
				'opacity': '1'
	});

	$('.list-header').css({
		'transform': 'translateX(0px)',
		'opacity': '1'
	});

	$('.bar-item').each(function(){
		if(getParameterByName('page') == getParameterByName('page', $(this).attr('href')))
		{
			$(this).addClass('bar-item-active');
			return false;
		}
	});

	//Main page stats part

	if($('.stats-wrapper').length > 0)
	{
		var tryUsers = $('.tryed-users');
		var usersData;
		$.post(ajax, {
			admin_action: 'started_users'
		}, function(data){
			permissionsHalt(data);
			usersData = JSON.parse(data);

			var readyUsersChart = new Chart(tryUsers, {
				type: 'doughnut',
				data: {
				labels: ["Has no answers", "Answered"],
				datasets: [{
				data: [usersData.notReady, usersData.ready],
				backgroundColor: [
							'#D6455A',
							'#715FD6',
				],
				hoverBackgroundColor: [
							'#C9233B',
							'#644CEC'
				],
				borderWidth: 0
				}]
				}
			});

			tryUsers = null;
			userData = null;
		});

		var visitUsers = $('.visitors');
		var visitData;
		$.post(ajax, {
			admin_action: 'get_visits'
		}, function(data){
			permissionsHalt(data);
			visitData = JSON.parse(data);
			var count = new Array();
			var days = new Array();
			for(var i = 0; i < visitData.length; i++)
			{
				count.push(String(visitData[i].count));
				days.push(visitData[i].day + ' of ' + visitData[i].mounth);
			}

			var readyUsersChart = new Chart(visitUsers, {
				type: 'bar',
				label: 'Visits',
				data: {
					labels: days,
					datasets: [
						{
							label: "Visits",
							backgroundColor: '#715FD6',
							hoverBackgroundColor: '#767FD3',
							borderWidth: 0,
							data: count,
						}
					]
				},
				options: {
					scales: {
						xAxes: [{
							stacked: true
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});

			visitUsers = null;
			visitData = null;
		});
	}

	if(getParameterByName('filter') == 'true')
	{
		$('.additional-search .addt-block').slideToggle(500, 'easeInOutExpo');
		$('.additional-search .addt-header img').toggleClass('menu-active');
		$('.additional-search .addt-header').toggleClass('header-active');

		$('.test-state-chooser').eq(getParameterByName('type'))
		.find('.item[value=' + getParameterByName('test-id') + ']').trigger('click');
	}
});

var loginClick = function()
{
	if($('.cr-email').val() == '')
	{
		$('.cr-email').css('border-bottom', '2px solid red');
	}
	else
	{
		$('.cr-email').css('border-bottom', '2px solid transparent');
	}

	if($('.cr-password').val() == '')
	{
		$('.cr-password').css('border-bottom', '2px solid red');
	}
	else
	{
		$('.cr-password').css('border-bottom', '2px solid transparent');
	}

	if($('.cr-email').val() != '' && $('.cr-password').val() != '')
	{
		$.post(ajax, {
			admin_action: 'login',
			email: $('.cr-email').val(),
			pass: $('.cr-password').val(),
			remember: $('.remember-login').is(':checked')
		}, function(data){
			console.log(data);
			permissionsHalt(data);
			if(data == 'success')
			{
				location.href = 'index.php';
			}
			else
			{
				$('.cr-password').css('border-bottom', '2px solid red');
				$('.cr-email').css('border-bottom', '2px solid red');
			}
		});
	}
};

$('.login-button').click(loginClick);

$(document).keypress(function(event){
	if(event.which == 13){
		$(".login-button").click();
	}
});

/***************************/
/*******Ligin part end******/
/***************************/

/************************/
/*******Show modal*******/
/************************/

var pageBlockerTrigger = function(trigger)
{
	if(trigger)
	{
		$('.blur-blocker').addClass('blur-blocker-active');
		$('.page-blocker').addClass('page-blocker-active');
	}
	else
	{
		$('.blur-blocker').removeClass('blur-blocker-active');
		$('.page-blocker').removeClass('page-blocker-active');
	}
};

var modalTrigger = true;

var hideModal = function()
{
	var modal = $('.modal-block');
	pageBlockerTrigger(false);
	modal.toggleClass('modal-block-active');
	modal.find('.success').off();
	modal.find('.reject').off();

	setTimeout(function(){
		modal.find('.title').removeClass('mistake-title');
		modal.find('.reject').removeClass('hidden');
	}, 300);

	modalTrigger = true;
};

//Available modal types is: common = 1 / error = 2
var showModal = function(success, reject, title, content, type = 1)
{
	if(modalTrigger == false)
		return;

	modalTrigger = false;

	var modal = $('.modal-block');

	switch(type)
	{
		case 1:
		{
			modal.find('.reject').on('click', reject);
			modal.find('.title').html(title);
			break;
		}
		case 2:
		{
			modal.find('.reject').addClass('hidden');
			modal.find('.title').html('Error!');
			modal.find('.title').addClass('mistake-title');
			break;
		}
	}

	modal.find('.success').on('click', success);
	modal.find('.content').html(content);

	modal.find('.success').on('click', hideModal);
	modal.find('.reject').on('click', hideModal);

	pageBlockerTrigger(true);

	modal.toggleClass('modal-block-active');
};

/**************************************/
/*******Add/remove/copy/save test******/
/**************************************/

$('.add-test-button').click(function(){
	if($('.test-name').val() != '')
	{
		$.post(ajax, {
			admin_action: 'add_test',
			name: $('.test-name').val(),
			use_template: $('.use-scoring-model').is(':checked')
		}, function(data){
			console.log(data);
			permissionsHalt(data);
			setNotify(data);
			location.href = 'index.php?page=tests';
		});
	}
	else
	{
		$('.test-name').css('border-bottom', '2px solid red');
	}
});

$('.rm-test').click(function(){
	$.post(ajax, {
		admin_action: 'remove_test'
	}, function(data)
	{
		permissionsHalt(data);
		setNotify(data);
		location.href = 'index.php?page=tests';
	});
});

var copyState = false;

var copyButtonState = function()
{
	if(copyState == false)
	{
		$('.top-button').not($(this)).addClass('disabled-top-button');
		$(this).addClass('copy-test-button-active');
		$('.copy-block').addClass('copy-block-visible');
		$('.invite-button').addClass('hidden');
		copyState = true;
	}
	else
	{
		$('.top-button').not($(this)).removeClass('disabled-top-button');
		$(this).removeClass('copy-test-button-active');
		$('.copy-block').removeClass('copy-block-visible');
		$('.invite-button').removeClass('hidden');
		copyState = false;
	}
};

$('.copy-test-button').on('click', copyButtonState);
$('.copy-question-button').on('click', copyButtonState);

$('body').on('click', '.copy-block', function(){
	if(getParameterByName('page') == 'tests')
	{
		var testItem = this;
		var testId = $(this).attr('test-id');
		var proto = $('.test-list-item-proto').clone();
		$('.list-item').last().after(proto);
		proto.addClass('list-item');
		proto.removeClass('test-list-item-proto');
	
		$.post(ajax, {
			admin_action: 'copy_test',
			test: testId
		}, function(data){
			permissionsHalt(data);
			var response = JSON.parse(data);
			proto.find('.list-col').eq(0).html(response.name);
			proto.find('.list-col').eq(1).html(response.created);
			proto.find('.list-col').eq(3).html(response.takers);

			//Copying data from html for new test
			proto.find('.list-col').eq(5).html($(testItem).parent().find('.list-col').eq(5).html());
			proto.find('.list-col').eq(6).html($(testItem).parent().find('.list-col').eq(6).html());
			//Copying end

			proto.find('.copy-block').attr('test-id', response.id);
			proto.find('.order-block').attr('test-id', response.id);
			proto.find('a').attr('href', proto.find('a').attr('href') + response.id);
		});

		setTimeout(function(){
			proto.addClass('loaded-list-item');
			$('.top-button').not($(this)).removeClass('disabled-top-button');
			$('.copy-test-button').removeClass('copy-test-button-active');
			$('.copy-block').removeClass('copy-block-visible');
			copyState = false;

			setNotify('success');
			showNotify();
		}, 100);
	}
	else if(getParameterByName('page') == 'test')
	{
		var question = $(this).attr('test-id');
		var proto = $('.test-list-item-proto').clone();
		$('.list-item').last().after(proto);
		proto.addClass('list-item');
		proto.removeClass('test-list-item-proto');

		$.post(ajax, {
			admin_action: 'copy_question',
			test: getParameterByName('id'),
			question: question
		}, function(data){
			permissionsHalt(data);
			var response = JSON.parse(data);
			proto.find('.list-col').eq(0).html(response.title);
			proto.find('.copy-block').attr('test-id', response.id);
			proto.find('.order-block').attr('test-id', response.id);
			proto.find('a').attr('href', proto.find('a').attr('href') + response.id);
		});

		setTimeout(function(){
			proto.addClass('loaded-list-item');
			$('.top-button').not($(this)).removeClass('disabled-top-button');
			$('.copy-question-button').removeClass('copy-test-button-active');
			$('.copy-block').removeClass('copy-block-visible');
			copyState = false;

			setNotify('success');
			showNotify();
		}, 100);
	}

});

/**************************************/
/*******Add/remove/copy/save test******/
/**************************************/

/************************/
/*******Tabs widget******/
/************************/

$('.head-tab').click(function(){
	if($(this).hasClass('active-tab') == true)
	{
		return;
	}

	$('.head-tab').each(function(){
		if($(this).hasClass('active-tab') == true)
		{
			$(this).removeClass('active-tab');
		}
	});

	$(this).addClass('active-tab');

	$('.tab-content').each(function(){
		if($(this).hasClass('active-tab-content') == true)
		{
			$(this).removeClass('active-tab-content');
			var forHide = this;
			setTimeout(function(){
				$(forHide).addClass('hidden-tab');
			}, 200);
			return;
		}
	});

	var forShow = $('.tab-content').eq($(this).index());
	setTimeout(function(){
		$(forShow).removeClass('hidden-tab');
	}, 200);
	setTimeout(function(){
		$(forShow).addClass('active-tab-content');
	}, 250);
});

/****************************/
/*******Tabs widget end******/
/****************************/

/******************/
/*******Score******/
/******************/

var checked = 0;

var countResponses = function(link, limit)
{
	checked++;
	if(checked == limit)
	{
		switchLoading();
		location.href = link;
	}
};

var removedScores = new Array();
var changedScores = new Array();

var saveQuestionData = function(question_id = getParameterByName('id'))
{
	switchLoading();
	checked = 0;
	var limit = 1;
	var link = 'index.php?page=test&id=' + $.cookie('id_buffer');


	if($('#question-video-input').val() != '')
	{
		var form = new FormData();
		limit++;
		form.append('key', $('#question-video-input').get(0).files[0]);

		$.ajax({
			url: ajax + '?admin_action=save_video&type=question&question_id=' + question_id,
			type: 'POST',
			data: form,
			cache: false,
			contentType: false,
			processData: false,
			success: function(data){
				permissionsHalt(data);
				if(data == 'success')
				{			
					countResponses(link, limit);
				}
			}
		});
	}

	if($('#strong-video-input').val() != '')
	{
		limit++;
		var form = new FormData();
		form.append('key', $('#strong-video-input').get(0).files[0]);

		$.ajax({
			url: ajax + '?admin_action=save_video&type=strong&question_id=' + question_id,
			type: 'POST',
			data: form,
			cache: false,
			contentType: false,
			processData: false,
			success: function(data){
				permissionsHalt(data);
				if(data == 'success')
				{
					countResponses(link, limit);
				}
			}
		});
	}

	var newScores = new Array();
	var newsBuffer = new Array();

	$('.inner-tab-content').each(function(){
		$(this).find('.score-item').each(function(){
			if($(this).attr('score-id') == 'new')
			{
				newsBuffer.push({
					text: $(this).find('.score-text input').val(),
					value: $(this).find('.score-value input').val(),
					ordering: $(this).attr('ordering')
				});
			}
		});

		if(newsBuffer.length == 0)
		{
			newsBuffer.push('none');
		}

		newScores.push(newsBuffer);
		newsBuffer = [];
	});

	for(var i = 0; i < removedScores.length; i++)
	{
		for(var j = 0; j < changedScores.length; j++)
		{
			if(removedScores[i] == changedScores[j].id)
			{
				changedScores.splice(j, 1);
			}
		}
	}

	//Scoring mode saving

	var scoringModes = new Array();

	if($('.new-question-meta').length == 0)
	{
		$('.scoring-mode .text').each(function(){
			scoringModes.push($(this).attr('value'));
		});
	}
	else if($('.new-question-meta').length > 0)
	{
		scoringModes = ['1', '1', '1'];
	}
	var reorderedScores = new Array();

	$('.score-items-wrap .score-item').each(function(){
		if($(this).attr('score-id') != 'new')
		{
			reorderedScores.push({
				id: $(this).attr('score-id'),
				ordering: $(this).attr('ordering')
			});
		}
	});

	var scoringListMode = ($('.scoring-list-mode').is(':checked') == true ? 'checked' : '');
	if($('.new-question-meta').length > 0)
		scoringListMode = 'checked';

		$.post(ajax, {
		admin_action: 'save_question',
		question_id: question_id,
		item_num: $('.item-num').val(),
		item_cont: $('.item-cont').val(),
		item_ques: $('.item-ques').val(),
		item_title: $('.item-title').val(),
		mins: $('.mins').val(),
		secs: $('.secs').val(),
		rec_mode: $('.rec-mode').attr('value'),
		roa : ($('.rev_of_ans').is(':checked') == true ? 'checked' : ''),
		aar : ($('.ans_af_rev').is(':checked') == true ? 'checked' : ''),
		afsr: ($('.ans_af_str').is(':checked') == true ? 'checked' : ''),
		rs : ($('.req_stn').is(':checked') == true ? 'checked' : ''),
		scoring_modes: scoringModes,
		scoring_list_mode: scoringListMode,
		removed: removedScores,
		changed: changedScores,
		new: newScores,
		ordering: reorderedScores
	}, function(data){
		permissionsHalt(data);
		if(data == 'success')
			{
				setNotify(data);
				countResponses(link, limit);
			}
	});
};

$('.s-test-button').click(function(){
	$.post(ajax, {
		admin_action: 'save_test',
		test_name: $('.test-name').val()
	}, function(data){
		permissionsHalt(data);
		if(data == 'success')
			showNotify(true);
	});
});

$('.s-qus-button').click(function(){
	$(window).off('beforeunload', onLeaveEvent);
	if($('.new-question-meta').length > 0)
	{
		$.post(ajax, {
			admin_action: 'create_question'
		}, function(data){
			console.log(data);
			permissionsHalt(data);
			saveQuestionData(data);
		});
	}
	else
	{
		saveQuestionData();
	}
});

$('.cancle-question-changes-button').click(function(){
	if(questionChangedTrigger == true){
		showModal(function(){
			$(window).off('beforeunload', onLeaveEvent);
			location.href = '?page=tests';
		}, null, 
		'Discard all changes?', 
		'Click "Ok" if you want discard all made changes.', 
		1);
	}
	else
	{
		location.href = '?page=tests';
	}
});

//Remove question

$('.remove-question-button').click(function(){
	showModal(function(){
			$.post(ajax, {
				admin_action: 'remove_question',
				id: getParameterByName('id')
			}, function(data){
				permissionsHalt(data);
				if(data == 'success')
				{
					setNotify(data);
					location.href = '?page=test&id=' + getParameterByName('tid');
				}
			});
		}, null, 'Remove question?', 
		'This action will remove all data related with this question, including user answers.',
		1);
});

//Score edit part

//----User side manage scores

var moveEnable = true;

$('body').on('change', '.score-item .score-text input', function(){
	var parent = $(this).parents('.score-item');

	if(parent.attr('score-id') == 'new')
		return;

	for(var i = 0; i < changedScores.length; i++)
	{
		if(changedScores[i].id == parent.attr('score-id'))
		{
			changedScores[i].text = $(this).val();
			return;
		}
	}

	changedScores.push({
		id: parent.attr('score-id'),
		text: $(this).val()
	});
});

$('body').on('change', '.score-item .score-value input', function(){
	var parent = $(this).parents('.score-item');

	if(parent.attr('score-id') == 'new')
		return;

	for(var i = 0; i < changedScores.length; i++)
	{
		if(changedScores[i].id == parent.attr('score-id'))
		{
			changedScores[i].value = $(this).val();
			return;
		}
	}

	changedScores.push({
		id: parent.attr('score-id'),
		value: $(this).val()
	});
});

$('body').on('click', '.score-item .up-score-button', function(){
	if(moveEnable == false)
		return;

	moveEnable = false;
	var index = $(this).parents('.score-items-wrap .score-item').index();

	if(index == 0)
	{
		moveEnable = true;
		return;
	}

	var current = $(this).parents('.score-item');
	var prev = $(this).parents('.score-item').prev();

	current.css('transform', 'translateY(calc(-100% - 5px))');
	prev.css('transform', 'translateY(calc(100% + 5px))');

	setTimeout(function(){
		current.addClass('trs-none');
		prev.addClass('trs-none');
		prev.before(current);
		current.removeAttr('style');
		prev.removeAttr('style');
		
		setTimeout(function(){
			current.removeClass('trs-none');
			prev.removeClass('trs-none');
			current.attr('ordering', parseInt(current.attr('ordering')) - 1);
			prev.attr('ordering', parseInt(prev.attr('ordering')) + 1);

			moveEnable = true;
		}, 350);
	}, 350);
});

$('body').on('click', '.score-item .down-score-button', function(){
	if(moveEnable == false)
		return;

	moveEnable = false;
	var setIndex = $('.score-items-wrap').index($(this).parents('.score-items-wrap'));
	var index = $(this).parents('.score-items-wrap').find('.score-item')
	.index($(this).parents('.score-item'));
	var listLength = $('.score-items-wrap').eq(setIndex).find('.score-item').length - 1;

	if(index == listLength)
	{
		moveEnable = true;
		return;
	}

	var current = $(this).parents('.score-item');
	var prev = $(this).parents('.score-item').next();

	current.css('transform', 'translateY(calc(100% + 5px))');
	prev.css('transform', 'translateY(calc(-100% - 5px))');

	setTimeout(function(){
		current.addClass('trs-none');
		prev.addClass('trs-none');
		prev.after(current);
		current.removeAttr('style');
		prev.removeAttr('style');
		
		setTimeout(function(){
			current.removeClass('trs-none');
			prev.removeClass('trs-none');
			current.attr('ordering', parseInt(current.attr('ordering')) + 1);
			prev.attr('ordering', parseInt(prev.attr('ordering')) - 1);

			moveEnable = true;
		}, 350);
	}, 350);
});

$('body').on('click', '.score-item .remove-button', function(){
	if(moveEnable == false)
		return;

	moveEnable = false;

	var parentItem = $(this).parents('.score-item');
	var highParent = parentItem.parents('.score-items-wrap');

	parentItem.css('margin-top', '0px');
	parentItem.css('padding-top', '0px');
	parentItem.css('padding-bottom', '0px');
	parentItem.css('max-height', '0px');

	var ordering = parseInt(parentItem.attr('ordering'));
	var items = highParent.find('.score-item');
	var index = items.index(parentItem);

	for(var i = index + 1; i < items.length; i++)
	{
		items.eq(i).attr('ordering', ordering);
		changedScores.push({
		id: items.eq(i).attr('score-id'),
		ordering: ordering
	});
		ordering += 1;
	}

	setTimeout(function(){
		if(parentItem.attr('score-id') != 'new')
		{
			removedScores.push(parentItem.attr('score-id'));
		}

		parentItem.remove();

		moveEnable = true;
	}, 300);
});

$('.add-score-block .add-button').click(function(){
	if(moveEnable == false)
		return;

	moveEnable = false;

	var scoreInit = $(this).parents('.inner-tab-content').find('.score-item').last();
	var proto = $('.score-proto').clone();
	var initExist = true;
	
	if(scoreInit.length == 0)
	{
		scoreInit = proto;
		scoreInit.attr('ordering', 0);
		initExist = false;
	}

	proto.removeClass('.score-proto');
	proto.addClass('score-item');
	proto.removeAttr('style');

	proto.find('.score-text input').val($(this).parents('.add-score-block').find('.score-text input').val());
	proto.find('.score-value input').val($(this).parents('.add-score-block').find('.score-value input').val());
	proto.attr('ordering', parseInt(scoreInit.attr('ordering')) + 1);
	proto.attr('score-id', 'new');

	$(this).parents('.add-score-block').find('.score-text input').val('')
	$(this).parents('.add-score-block').find('.score-value input').val('');

	proto.removeClass('score-proto');
	proto.addClass('score-item');
	proto.css('transform', 'translateY(calc(100% + 5px))');
	proto.css('margin-top', '0px');
	proto.css('padding-top', '0px');
	proto.css('padding-bottom', '0px');
	proto.css('max-height', '0px');

	if(initExist == false)
	{
		$(this).parents('.inner-tab-content').find('.score-items-wrap').append(proto);
	}
	else
	{
		scoreInit.last().after(proto);
	}

	setTimeout(function(){
		proto.css('margin-top', '5px');
		proto.css('padding-top', '5px');
		proto.css('padding-bottom', '5px');
		proto.css('max-height', '50px');
		proto.css('transform', 'translateY(0px)');
		proto.removeAttr('style');

		moveEnable = true;
	}, 200);
});

/**********************/
/*******Score end******/
/**********************/

/**********************/
/*******Set score******/
/**********************/


/**************************/
/*******Set score end******/
/**************************/

/************************/
/*******Report part******/
/************************/

$('.add-option-button').click(function(){
	var check = $(this).before('<div class="downwrapper hidden-select">' +
						'<div class="remove-button"></div>' +
						'<div class="dropdown clearfix">' +
							'<div value="" class="text">Option</div>' +
							'<div class="c-caret">' +
								'<img src="img/caret.png">' +
							'</div>' +
							'<div class="items">' +
								'<div value="1" class="item">Item 1</div>' +
								'<div value="2" class="item">Item 2</div>' +
								'<div value="3" class="item">Item 3</div>' +
								'<div value="4" class="item">Item 4</div>' +
							'</div>' +
						'</div>' +
					'</div>');
	var buffer = this;
	setTimeout(function(){
		$(buffer).prev().css('width', '152px');
		$(buffer).prev().css('visibility', 'visible');
	}, 100);

	setTimeout(function(){
		$(buffer).prev().css('overflow', 'visible');
	}, 400);
});

$('body').on('click', '.downwrapper .remove-button', function(){
	$(this).parent().css('transform', 'scale(0)');
	$(this).parent().css('opacity', '0');
	$(this).parent().css('width', '0');
	$(this).parent().css('visibility', 'hidden');

	var buffer = this;
	setTimeout(function(){
		$(buffer).parent().remove();
	}, 600);
})

/****************************/
/*******Report part end******/
/****************************/

/*********************/
/*******Ordering******/
/*********************/

$('.order-button').click(function(){
	if($(this).hasClass('order-button-active') == false)
	{
		$(this).addClass('order-button-active');
		$('.order-block').addClass('order-block-active');
		$('.top-button').not($(this)).addClass('disabled-top-button');
		$('.invite-button').addClass('hidden');
	}
	else
	{
		$(this).removeClass('order-button-active');
		$('.order-block').removeClass('order-block-active');
		$('.top-button').not($(this)).removeClass('disabled-top-button');
		$('.invite-button').removeClass('hidden');
	}
});

var listType;

if(getParameterByName('page') == 'tests')
	listType = 'test';
else if(getParameterByName('page') == 'test')
	listType = 'question';

var moveHandleTrigger = true;

$('.to-top').click(function(){
	var listItemIndex = $('.list-item').index($(this).parents('.list-item'));

	if(moveHandleTrigger == true)
		moveHandleTrigger = false;
	else
		return;

	var index = $(this).parents('.list-item');

	if(listItemIndex == 0)
	{
		moveHandleTrigger = true;
		return;
	}

	$(index).css('transform', 'translateY(-100%)');
	$(index).prev().css('transform', 'translateY(100%)');

	setTimeout(function(){
		$(index).addClass('color-transition');
		$(index).prev().addClass('color-transition');
		$(index).prev().before($(index));
		$(index).css('transform', '');
		$(index).next().css('transform', '');

		setTimeout(function(){
			$.post(ajax, {
				admin_action: 'order_up',
				list_type: listType,
				id: $(index).find('.order-block').attr('test-id')
			}, function(data){
				permissionsHalt(data);
				if(data == 'success')
				{
					$(index).removeClass('color-transition');
					$(index).next().removeClass('color-transition');
					moveHandleTrigger = true;
				}
			});
		}, 350);
	}, 350);
});

$('.to-bottom').click(function(){
	var listItemIndex = $('.list-item').index($(this).parents('.list-item'));

	if(moveHandleTrigger == true)
		moveHandleTrigger = false;
	else
		return;

	var index = $(this).parents('.list-item');

	if(listItemIndex == $('.list-item').length - 1)
	{
		moveHandleTrigger = true;
		return;
	}

	$(index).css('transform', 'translateY(100%)');
	$(index).next().css('transform', 'translateY(-100%)');

	setTimeout(function(){
		$(index).addClass('color-transition');
		$(index).next().addClass('color-transition');
		$(index).next().after($(index));
		$(index).css('transform', '');
		$(index).prev().css('transform', '');

		setTimeout(function(){
			$.post(ajax, {
				admin_action: 'order_down',
				list_type: listType,
				id: $(index).find('.order-block').attr('test-id')
			}, function(data){
				permissionsHalt(data);
				if(data == 'success')
				{
					$(index).removeClass('color-transition');
					$(index).prev().removeClass('color-transition');
					moveHandleTrigger = true;
				}
			});
		}, 350);
	}, 350);
});

/******************************/
/*******Inner tabs widget******/
/******************************/

var setCheckedScore;

$(document).ready(function(){

	$('.tab-button-back').css({
		'width': $('.tabs-button-container .tab-button').first().outerWidth(),
		'left': '0px'
	});
});

$('.tabs-button-container .tab-button').click(function(){
	if(moveEnable == false)
		return;

	moveEnable = false;
	var currentIndex = $('.current-inner-tab-button').index();

	$('.current-inner-tab-button').removeClass('current-inner-tab-button');
	$('.tab-button-back').css('left', $(this).position().left);
	$('.tab-button-back').css('width', $(this).outerWidth());
	$(this).addClass('current-inner-tab-button');

	var nextIndex = $('.current-inner-tab-button').index();

	$('.inner-tab-content').eq(currentIndex).addClass('inner-hidden-tab');

	if($('.score-content-tab').length > 1)
	{
		$('.score-content-tab').eq(currentIndex).addClass('inner-hidden-tab');
	}

	setTimeout(function(){
		$('.inner-tab-content').eq(currentIndex).addClass('inner-nodisplay');
		$('.inner-tab-content').eq(nextIndex).removeClass('inner-nodisplay');

		if($('.score-content-tab').length > 1)
		{
			$('.score-content-tab').eq(currentIndex).addClass('inner-nodisplay');
			$('.score-content-tab').eq(nextIndex).removeClass('inner-nodisplay');
		}

		setTimeout(function(){
			$('.inner-tab-content').eq(nextIndex).removeClass('inner-hidden-tab');

			if($('.score-content-tab').length > 1)
			{
				$('.score-content-tab').eq(nextIndex).removeClass('inner-hidden-tab');
			}
			moveEnable = true;
		}, 50);
	}, 350);
});

/**************************/
/*******Saving scores******/
/**************************/

var changeUserScore = function()
{
	switchLoading();
	var response = $(this).attr('name');
	var scores = new Array();

	$(this).parents('.score-content-tab').find('.score-input').each(function(){
		if($(this).is(':checked'))
		{
			scores.push($(this).attr('value'));
		}
	});

	$.post(ajax, {
		admin_action: 'save_user_score',
		response: response,
		scores: scores,
		user: getParameterByName('user-id'),
		question: getParameterByName('question-id'),
		test: getParameterByName('test-id')
	}, function(data){
		permissionsHalt(data);
		if(data == 'success')
		{
			switchLoading();
		}
	});
};

$('.score-tabs input[id^=score-id-]').change(changeUserScore);

$('.score-comment').change(function(){
	switchLoading();

	$.post(ajax, {
		admin_action: 'save_user_score_comment',
		comment: $(this).val(),
		user: getParameterByName('user-id'),
		question: getParameterByName('question-id'),
		test: getParameterByName('test-id'),
		response: $('.current-inner-tab-button').index() + 1
	}, function(data){
		permissionsHalt(data);
		if(data == 'success')
		{
			switchLoading();
		}
	});
});

/********************************/
/*******Upload video widget******/
/********************************/

$('.video-hidden-input').change(function(){
	var item = $(this).siblings('.file-title');
	item.html($(this).get(0).files[0].name);

	setTimeout(function(){
		item.addClass('file-title-visible');
	}, 700);
});

/*************************/
/*******List sorting******/
/*************************/

var searchStates = ['none', 'up', 'down'];

var searchFailSwitch = function(option)
{
	var item = $('.search-fail-wrap');
	var className = 'search-fail-visible';

	if(option)
	{
		item.addClass(className);
	}
	else
	{
		item.removeClass(className);
	}
	// if(option && item.hasClass(className) == false && $('.list-item').length == 0)
	// {
	// 	item.addClass(className);
	// }
	// else if(option && item.hasClass(className))
	// {
	// 	item.removeClass(className);
	// }
};

var removeListItems = function()
{
	$('.result-list-item').removeClass('loaded-list-item');
	setTimeout(function(){
		$('.result-list-item').remove();
	}, 350);
};

var displayResultList = function(proto, element)
{
	proto.addClass('list-item');
	proto.addClass('result-list-item');
	proto.removeClass('list-item-proto');
	proto.removeAttr('style');
	proto.find('a').attr('href', proto.find('a').attr('href') + element.id);
	proto.find('.list-col').eq(0).html(element.name + ' ' + element.sec_name);
	proto.find('.list-col').eq(1).html(element.email);
	proto.find('.list-col').eq(2).html(element.invited);
	proto.find('.list-col').eq(3).html(element.completed);
	proto.find('.list-col').eq(4).html(element.scored);
	proto.find('#checkbox-')
	.attr('id', proto.find('#checkbox-').attr('id') + element.id);
	proto.find('.assign-checkbox-block label').
	attr('for', proto.find('.assign-checkbox-block label').attr('for') + element.id);
	proto.attr('user-id', proto.attr('user-id') + element.id);
	$('.content-container').append(proto);

	setTimeout(function(){
		$('.result-list-item').addClass('loaded-list-item');
	}, 150);
}

var searchUser = function(arg)
{
	if($(arg).parents('.downwrapper').hasClass('choose-user-test'))
		return;

	searchFailSwitch(false);

	var itemIndex;
	var caretItem;
	var inputItem;
	var currentStateIndex;
	var addtIndex = -1;
	var addtId = -1;

	//Detecting additional search properties
	$('.test-state-chooser .text').each(function(index){
		if($(this).attr('value') != '0')
		{
			addtId = $(this).attr('value');
			addtIndex = index;
		}
	});

	if($(arg.currentTarget).parent().hasClass('search-input-wrap') || $(arg.currentTarget).hasClass('search-caret'))
	{
		//Getting a column index
		if($(arg.currentTarget)[0].tagName == 'DIV' && $(arg.currentTarget).hasClass('search-caret'))
			itemIndex = $(arg.currentTarget).index('.search-caret');
		else if($(arg.currentTarget)[0].tagName == 'INPUT')
			itemIndex = $(arg.currentTarget).index('.search-input-wrap input');

		//Getting data about position and value of search input
		caretItem = $('.search-caret').eq(itemIndex);
		inputItem = $('.search-input-wrap input').eq(itemIndex).val();

		//Dropping other inputs
		$('.search-input-wrap input').not($('.search-input-wrap input').eq(itemIndex)).val('');

		currentStateIndex = searchStates.indexOf($(caretItem).attr('state'));

		//Dropping other carets positions
		$('.search-caret').not($(caretItem)).attr('state', 'none');

		//Updating state for current caret
		if($(arg.currentTarget)[0].tagName == 'DIV' && $(arg.currentTarget).hasClass('search-caret'))
		{
			if(currentStateIndex < searchStates.length-1)
			{
				currentStateIndex++;
				$(caretItem).attr('state', searchStates[currentStateIndex]);
			}
			else
			{
				currentStateIndex = 0;
				$(caretItem).attr('state', searchStates[0]);
			}
		}

	}
	else if($(arg).hasClass('item'))
	{
		itemIndex = 0;
		currentStateIndex = 0;
		inputItem = '';
		for(var i = 0; i < $('.search-caret').length; i++)
		{
			if($('.search-caret').eq(i).attr('state') != 'none' || $('.search-input-wrap input').eq(i).val() != '')
			{
				itemIndex = i;
				currentStateIndex = searchStates.indexOf($('.search-caret').eq(i).attr('state'));
				inputItem = $('.search-input-wrap input').eq(i).val();
				break;
			}
		}
	}

	removeListItems();

	var response;
	var proto;

	setTimeout(function(){
		$.post(ajax, {
			admin_action: 'sort_users_list',
			col: itemIndex,
			direction: currentStateIndex,
			keyword: inputItem,
			addtIndex: addtIndex,
			addtTest: addtId
		}, function(data){
			permissionsHalt(data);
			response = JSON.parse(data);
			var assignUsers = new Array();
			$('.user-for-assign').each(function(){
				assignUsers.push($(this).attr('user-id'));
			});

			for(var i = 0; i < response.length; i++)
			{
				for(var j = 0; j < assignUsers.length; j++)
				{
					if(response[i].id == assignUsers[j])
					{
						response.splice(i, 1);
						i--;
						break;
					}
				}
			}

			response.forEach(function(element){
				proto = $('.list-item-proto').clone();

				displayResultList(proto, element);
			});

			if($('.result-list-item').length > 0)
				searchFailSwitch(false);
			else
				searchFailSwitch(true);
		});
	}, 400);
};

var searchInputChange = function(arg)
{
	$('.search-input-wrap input').not($(this)).val('');

	if(arg.keyCode == 8 || $('.search-input-wrap input').val() != '')
	{
		searchUser(arg);
	}
};

$('.search-input-wrap input').on('keyup', searchInputChange);
$('.search-caret').on('click', searchUser);
$('.test-state-chooser .item').click(function(){
	searchUser(this);
});

/*************************/
/*******Account page******/
/*************************/

//Changing user name

var editButtonState = false;

var saveEditedUserData = function()
{
	if(editButtonState == false)
	{
		$('.user-name-container .edit-button').css({
			'transform': 'scale(0) rotate(180deg)',
			'visibility': 'hidden'
		});

		setTimeout(function(){
			$('.user-name-container .save-edited').css({
				'transform': 'scale(1) rotate(0deg)',
				'visibility': 'visible'
			});
			editButtonState = true;
		}, 300);

		//Labels and inputs

		$('.user-name-container .title').css({
			'filter': 'blur(3px)',
			'transform': 'scale(.9) translateX(20px)',
			'visibility': 'hidden',
			'opacity': '0'
		});

		setTimeout(function(){
			$('.user-name-container .title').css('display', 'none');
			$('.user-name-container .input').css('display', 'block');


			setTimeout(function(){
				$('.user-name-container .input').css({
					'filter': 'blur(0px)',
					'transform': 'scale(1) translateX(0px)',
					'visibility': 'visible',
					'opacity': '1'
				});
			}, 50);
		}, 500);
	}
	else
	{
		var newData = new Array();
		$('.user-name-container input').each(function(index){
			newData.push($(this).val());
			$('.user-name-container .title').eq(index).html($(this).val());
		});

		$('.profile-header .name').html($('.user-name-container input').eq(0).val());

		$.post(ajax, {
			admin_action: 'save_user_data',
			data: newData,
			user: getParameterByName('id')
		}, function(data){
			permissionsHalt(data);
			if(data == 'success')
			{
				setNotify(data);
				setTimeout(function(){
					showNotify();
				}, 1000);
			}
			else
			{
				showModal(null, null,'Error', 'This email is not available.', 2);
				var response = JSON.parse(data)[0];

				$('.user-name-container input.name').val(response.name);
				$('.user-name-container input.sec_name').val(response.sec_name);
				$('.user-name-container input.email').val(response.email);

				$('.user-name-container .title-name').text(response.name);
				$('.user-name-container .title-sec_name').text(response.sec_name);
				$('.user-name-container .title-email').text(response.email);
			}
		});

		$('.user-name-container .save-edited').css({
			'transform': 'scale(0) rotate(-180deg)',
			'visibility': 'hidden'
		});

		setTimeout(function(){
			$('.user-name-container .edit-button').css({
				'transform': 'scale(1) rotate(0deg)',
				'visibility': 'visible'
			});
			editButtonState = false;
		}, 300);

		//Labels and inputs

		$('.user-name-container .input').css({
			'filter': 'blur(5px)',
			'transform': 'scale(.9) translateX(-20px)',
			'visibility': 'hidden',
			'opacity': '0'
		});

		setTimeout(function(){
			$('.user-name-container .input').css('display', 'none');
			$('.user-name-container .title').css('display', 'block');


			setTimeout(function(){
				$('.user-name-container .title').css({
					'filter': 'blur(0px)',
					'transform': 'scale(1) translateX(0px)',
					'visibility': 'visible',
					'opacity': '1'
				});
			}, 50);
		}, 500);
	}
};

$('.user-name-container .edit-button').click(saveEditedUserData);
$('.user-name-container .save-edited').click(saveEditedUserData);

//Changing current sim details

$('.drop-current-user-sim-button').click(function(){
	$('.choose-user-question .text').html('&nbsp');
	$('.choose-user-question .text').attr('value', '0');
	$('.choose-user-test .text').html('&nbsp');
	$('.choose-user-test .text').attr('value', '0');
	$('.current-user-question-choose').remove();
});

$('body').on('click', '.remove-token-button', function(){
	if(!confirm())
		return;

	var object = $(this).parent();
	object.addClass('overflow-hidden')
	object.css({
		'max-height': '0px',
		'margin-bottom': '0px',
		'padding-top': '0px',
		'padding-bottom': '0px'
	});

	$.post(ajax, {
		admin_action: 'remove_invite_token',
		id: object.attr('token-id')
	}, function(data){
		permissionsHalt(data);
		setNotify(data);
		showNotify();
	});

	setTimeout(function(){
		object.remove();
	}, 450);
});

/*************************/
/*******Account page******/
/*************************/

var updateTokenData = function()
{
	var object = $(this).parents('.token-list-item');

	$.post(ajax, {
		admin_action: 'update_link_token_data',
		id: object.attr('token-id'),
		test: $(this).attr('value')
	}, function(data){
		permissionsHalt(data);
		setNotify(data);
		showNotify();
		setTokenSize();
	});
};

$('.token-link-test-choose').on('click', updateTokenData);

$('.add-link-token-button').click(function(){
	var proto = $('.token-list-item-proto').clone();

	$.post(ajax, {
		admin_action: 'create_link_token',
		test: proto.find('.choose-link-test .text').attr('value')
	}, function(data){
		permissionsHalt(data);
		var response = JSON.parse(data);
		proto.find('.token-value').html(proto.find('.token-value').html() + response.token);
		proto.attr('token-value', response.token);
		proto.attr('token-id', response.id);
		setNotify(response.data);
		showNotify();
	});

	proto.removeClass('token-list-item-proto');
	proto.addClass('token-list-item');
	$('.link-tokens-list').prepend(proto);

	setTimeout(function(){
		proto.removeClass('proto-token-list-item-style');
	}, 50);


	setTimeout(function(){
		proto.removeClass('overflow-hidden');
		setTokenSize();
	}, 550);
});

/************************/
/*******Invite side******/
/************************/

$('.list-item .invite-button').click(function(){
	$.post(ajax, {
		admin_action: 'create_link_token',
		test: $(this).attr('test-id')
	}, function(data){
		permissionsHalt(data);
		var response = JSON.parse(data);
		setNotify(response.data);
		showNotify();
	});
});

/**********************************/
/*******Assign test for users******/
/**********************************/

var userTestTrigger = false;

$('.assign-user-test-button').click(function(){
	if(userTestTrigger == false)
	{
		userTestTrigger = true;
		$('.assign-user-test-button').addClass('user-test-assign-active');
		$('.assign-user-test-save-button').removeClass('user-test-assign-invisible');
		$('.test-users-chooser-block').slideToggle(600, 'easeInOutExpo');

		$('.user-list-item .assign-checkbox-block').addClass('assign-checkbox-block-visible');
		$('.assign-check-wrap').fadeToggle(600, 'easeInOutExpo');
	}
	else
	{
		userTestTrigger = false;
		$('.assign-user-test-button').removeClass('user-test-assign-active');
		$('.assign-user-test-save-button').addClass('user-test-assign-invisible');
		$('.test-users-chooser-block').slideToggle(600, 'easeInOutExpo');

		$('.user-list-item .assign-checkbox-block').removeClass('assign-checkbox-block-visible');
		$('.assign-check-wrap').fadeToggle(600, 'easeInOutExpo');
	}
});

var clickReject = false;

$('body').on('click', '.user-list-item .assign-check-wrap', function()
{
	if(clickReject)
		return;
	else
		clickReject = true;

	$(this).find('.user-list-checkbox').attr('checked', true);

	var item = $(this).parents('.list-item');
	$(item).addClass('trs-none');
	var proto = $(this).parents('.list-item').clone();

	$(this).parents('.list-item').slideToggle(300, 'easeInOutExpo', function(){
		$(item).removeClass('trs-none');
		$(item).remove();
	});

	proto.css('display', 'none');
	proto.removeClass('user-list-item');
	proto.removeClass('result-list-item');
	proto.addClass('user-for-assign');

	$('.users-for-assign-list').append(proto);

	proto.slideToggle(300, 'easeInOutExpo', function(){
		proto.removeClass('trs-none');
		$.post(ajax, {
			admin_action: 'check_user_tested',
			test: $('.choose-user-test .text').attr('value'),
			user: proto.attr('user-id')
		}, function(data){
			permissionsHalt(data);
			if(data == 'success')
				proto.find('.tested-badge').addClass('tested-badge-visible');
			clickReject = false;
		});
	});
});

$('body').on('click', '.user-for-assign .assign-check-wrap', function()
{
	if(clickReject)
		return;
	else
		clickReject = true;

	$(this).find('.user-list-checkbox').attr('checked', false);

	var proto = $(this).parents('.list-item').clone();
	var object = $(this).parents('.list-item');
	object.addClass('trs-none');
	object.slideToggle(300, 'easeInOutExpo', function(){
		object.remove();
	});

	proto.find('.tested-badge').removeClass('tested-badge-visible');
	proto.slideToggle(0);
	proto.addClass('trs-none');
	$('.search-fail-wrap').after(proto);
	proto.slideToggle(300, 'easeInOutExpo', function(){
		clickReject = false;
	});
	proto.removeClass('user-for-assign');
	proto.addClass('user-list-item');
	proto.addClass('result-list-item');
});

$('.assign-user-test-save-button').click(function(){
	switchLoading();
	var usersList = new Array();
	$('.user-for-assign').each(function(){
		usersList.push($(this).attr('user-id'));
	});

	$.post(ajax, {
		admin_action: 'assign_users_sims',
		users: usersList,
		test: $('.choose-user-test').find('.text').attr('value'),
		day: $('.assign-test-calendar .day').val(),
		month: $('.assign-test-calendar').attr('value'),
		year: $('.assign-test-calendar .year').val()
	}, function(data){
		permissionsHalt(data);

		if(data == 'time_error')
		{
			showModal(
				null,
				null,
				'Assignation error',
				"You can't assign completion date in past."
			);
			switchLoading();
		}
		else if(data == 'question_error')
		{
			showModal(
				null,
				null,
				'Assignation error',
				"Simulation has no questions."
			);
			switchLoading();
		}
		else if(data == 'success')
		{
			setTimeout(function(){
				switchLoading();
			}, 600 );
			
			$($('.user-for-assign').get()).each(function(){
				$(this).find('.user-list-checkbox').attr('checked', false);
				var proto = $(this).clone();
				var object = $(this);
				object.addClass('trs-none');
				object.slideToggle(600, 'easeInOutExpo', function(){
					object.remove();
				});

				proto.slideToggle(0);
				proto.addClass('trs-none');
				$('.search-fail-wrap').after(proto);
				proto.slideToggle(600, 'easeInOutExpo', function(){
					clickReject = false;
				});
				proto.removeClass('user-for-assign');
				proto.addClass('user-list-item');
				proto.addClass('result-list-item');
			});

			userTestTrigger = false;
			$('.assign-user-test-button').removeClass('user-test-assign-active');
			$('.assign-user-test-save-button').addClass('user-test-assign-invisible');
			$('.test-users-chooser-block').slideToggle(600, 'easeInOutExpo');

			$('.user-list-item .assign-checkbox-block').removeClass('assign-checkbox-block-visible');
			$('.assign-check-wrap').fadeToggle(600, 'easeInOutExpo');
			$('.tested-badge').removeClass('tested-badge-visible');
		}
	});
});

/**********************/
/*******Link col*******/
/**********************/

$('.tip-col .link-col').click(function(){
	event.preventDefault();
	location.href = $(this).attr('link');
});

/*******************************/
/*******Additional search*******/
/*******************************/

$('.additional-search .addt-header').click(function(){
	$('.additional-search .addt-block').slideToggle(500, 'easeInOutExpo');
	$('.additional-search .addt-header img').toggleClass('menu-active');
	$('.additional-search .addt-header').toggleClass('header-active');
});

$('.test-state-chooser .items .item').click(function(){
	$('.test-state-chooser .text').not($(this).parent().siblings('.text')).attr('value', '0');
	$('.test-state-chooser .text').not($(this).parent().siblings('.text')).html('None');
});

//Checking tested for assignel users list

var checkTested = function()
{
	$('.users-for-assign-list .list-item .tested-badge').removeClass('tested-badge-visible');

	var test = $(this).attr('value');
	$('.users-for-assign-list .list-item').each(function(){
		var element = $(this);
		$.post(ajax, {
			admin_action: 'check_user_tested',
			user: $(this).attr('user-id'),
			test: test
		}, function(data){
			console.log(data);
			permissionsHalt(data);

			if(data == 'success')
			{
				$(element).find('.tested-badge').addClass('tested-badge-visible');
			}
		});
	});

};

$('.current-user-test-choose').click(checkTested);

/************************/
/*******Leave admin******/
/************************/

$('.leave-admin-button').click(function(){
	showModal(function(){
		$.post(ajax, {
			admin_action: 'leave_admin_panel'
		}, function(data){
			permissionsHalt(data);
			if(data == 'success')
			{
				location.href = 'index.php';
			}
		});
	}, null, 'Notify', 'Are you sure, that you want log out from admin panel?', 1);
});

/*****************************/
/*******Change password*******/
/*****************************/

$('.change-password-submit').click(function(){
	var inputs = $('.change-password-block input')

	for(var i = 0; i < inputs.length; i++)
	{
		if($(inputs[i]).val() == '')
		{
			showModal(null, null, null, 'All fields should be filled out', 2);
			return;
		}
	}

	if($('.change-password-block .password-new').val() != 
		$('.change-password-block .password-new-repeat').val())
	{
		showModal(null, null, null, 'New passwords is not equal', 2);
		return;
	}

	$.post(ajax, {
		admin_action: 'admin_password_check',
		password: $('.change-password-block .admin-password').val()
	}, function(data){
		permissionsHalt(data);
		if(data == 'error')
		{
			showModal(null, null, null, 'Wrong current password', 2);
		}
		else if (data == 'success')
		{
			$.post(ajax, {
				admin_action: 'change_admin_password',
				password: $('.change-password-block .password-new').val()
			}, function(data){
				permissionsHalt(data);
				if(data != '')
				{
					setNotify('success');
					showNotify();

					for(var i = 0; i < inputs.length; i++)
					{
						$(inputs[i]).val('');
					}
				}
			});
		}
	});
});

/**************************/
/*******Save settings******/
/**************************/

$('.save-settings-button').click(function(){
	var practiceQuestion = $('.practice-question-select .text').attr('value');
	var startQuestion = $('.start-question-select .text').attr('value');
	$.post(ajax, {
		admin_action: 'save_admin_settings',
		practice_question: practiceQuestion,
		start_questions: startQuestion
	}, function(data){
		permissionsHalt(data);
		setNotify(data);
		showNotify();
	});
});


/***************************/
/*******Score details*******/
/***************************/

var areaBind = function()
{
	changedScoreDetails.push($(this).parents('.detail-list-item').attr('item-id'));
};

var switchScoreDetails = function(trigger, scoreId = '')
{
	if(trigger)
	{
		$('.score-details-wrap .current-score-id').html(scoreId);
		$('.score-details-loading-block').removeClass('hidden');
		$('.score-details-wrap').removeClass('hidden');
		setTimeout(function(){
			$('.score-details').addClass('score-details-visible');

			$.post(ajax, {
				admin_action: 'get_score_details',
				score_id: scoreId
			}, function(data){
				permissionsHalt(data);
				$('.score-details-loading-block').addClass('hidden');
				var details = JSON.parse(data);

				details.forEach(function(element){
					var proto = $('.detail-list-item-proto').clone();

					proto.attr('ordering', element.ordering);
					proto.attr('item-id', element.id);
					proto.find('.title-area').val(element.title);
					proto.find('.desc-area').val(element.description);

					proto.removeClass('hidden');
					proto.removeClass('detail-list-item-proto');
					proto.addClass('detail-list-item');
					proto.addClass('detail-trs');
					proto.removeClass('h0-state');
					proto.removeClass('detail-trs');
					proto.addClass('detail-list-item-minimized');

					$('.score-details-content').append(proto);
				});
				
				$('body').on('change', '.detail-list-item .title-area', areaBind);
				$('body').on('change', '.detail-list-item .desc-area', areaBind);

				$('body').on('change', '.detail-list-item .title-area', scoreDetailChangedEvent);
				$('body').on('change', '.detail-list-item .desc-area', scoreDetailChangedEvent);
				$('body').on('click', '.detail-list-item .up-button', scoreDetailChangedEvent);
				$('body').on('click', '.detail-list-item .down-button', scoreDetailChangedEvent);
				$('body').on('click', '.detail-list-item .remove-button', scoreDetailChangedEvent);
			});

		}, 50);
	}
	else
	{
		$('.score-details').removeClass('score-details-visible');
		setTimeout(function(){
			$('.score-details-wrap').addClass('hidden');

			$('.detail-list-item').remove();

		}, 200);
	}
};

$('body').on('click', '.score-item .details-button', function(){
	var scoreId = $(this).parents('.score-item').attr('score-id');
	pageBlockerTrigger(true);
	switchScoreDetails(true, scoreId);
});

$('.score-details-wrap .back-space-button').click(function(){
	if(scoreDetailChanged)
	{
		showModal(function(){
			switchScoreDetails(false);
			pageBlockerTrigger(false);
			scoreDetailChanged = false;
		}, null, 
		'Discard all changes?', 
		'Click "Ok" if you want discard all made changes.', 
		1);
	}
	else
	{
		switchScoreDetails(false);
		pageBlockerTrigger(false);
	}
});

//Detail item buttons

var detailButtonsActive = true;

////////////////////////////////
///Remove score detail button///
////////////////////////////////

var changedScoreDetails = new Array();
var removedScoreDetails = new Array();

$('body').on('click', '.detail-list-item .remove-button', function(){
	if(!detailButtonsActive)
		return;

	detailButtonsActive = false;

	var item = $(this).parents('.detail-list-item');
	var itemId = item.attr('item-id');

	if(itemId != 'new')
		removedScoreDetails.push(itemId);

	item.slideToggle(450, 'easeInOutExpo');

	var itemsList = $('.detail-list-item');
	var itemIndex = itemsList.index(item);
	var orderBuff = item.attr('ordering');

	for(var i = itemIndex + 1; i < itemsList.length; i++)
	{
		itemsList.eq(i).attr('ordering', orderBuff);
		orderBuff++;
		changedScoreDetails.push(itemsList.eq(i).attr('item-id'));
	}

	setTimeout(function(){
		item.remove();
		detailButtonsActive = true;
	}, 450);
});

$('body').on('click', '.detail-list-item .down-button', function(){
	if(!detailButtonsActive)
		return;

	changedScoreDetails.push($(this).parents('.detail-list-item').attr('item-id'));
	changedScoreDetails.push($(this).parents('.detail-list-item').next().attr('item-id'));
	detailButtonsActive = false;

	var allNum = $('.detail-list-item').length - 1;
	var item = $(this).parents('.detail-list-item');
	var index = $('.detail-list-item').index(item);

	if(allNum == index)
		return;

	var next = $('.detail-list-item').eq(index+1);
	var nextOrdering = next.attr('ordering');
	var itemOrdering = item.attr('ordering');

	next.attr('ordering', parseInt(nextOrdering) - 1);
	item.attr('ordering', parseInt(itemOrdering) + 1);

	next.addClass('detail-trs detail-up');
	item.addClass('detail-trs detail-down');

	setTimeout(function(){
		next.removeClass('detail-trs');
		item.removeClass('detail-trs');

		item.insertAfter(next);

		next.removeClass('detail-up');
		item.removeClass('detail-down');

		detailButtonsActive = true;
	}, 400);
});

$('body').on('click', '.detail-list-item .up-button', function(){
	if(!detailButtonsActive)
		return;

	changedScoreDetails.push($(this).parents('.detail-list-item').attr('item-id'));
	changedScoreDetails.push($(this).parents('.detail-list-item').prev().attr('item-id'));
	detailButtonsActive = false;

	var item = $(this).parents('.detail-list-item');
	var index = $('.detail-list-item').index(item);

	if(index == 0)
		return;

	var prev = $('.detail-list-item').eq(index-1);
	var nextOrdering = prev.attr('ordering');
	var itemOrdering = item.attr('ordering');

	prev.attr('ordering', parseInt(nextOrdering) + 1);
	item.attr('ordering', parseInt(itemOrdering) - 1);

	prev.addClass('detail-trs detail-down');
	item.addClass('detail-trs detail-up');

	setTimeout(function(){
		prev.removeClass('detail-trs');
		item.removeClass('detail-trs');

		prev.insertAfter(item);

		prev.removeClass('detail-down');
		item.removeClass('detail-up');

		detailButtonsActive = true;
	}, 400);
});

//Add detail button

$('.score-details-wrap .add-detail-item-button').click(function(){
	if(!detailButtonsActive)
		return;

	var proto = $('.score-details-wrap .detail-list-item-proto').clone();

	var lastOrdering = $('.detail-list-item').length;
	proto.attr('ordering', lastOrdering + 1);

	proto.addClass('detail-list-item-minimized');
	proto.removeClass('detail-list-item-proto');
	proto.addClass('detail-list-item');
	proto.addClass('detail-trs');
	proto.removeClass('hidden');

	$('.score-details-content').append(proto);

	setTimeout(function(){
		proto.removeClass('h0-state');
		setTimeout(function(){
			proto.removeClass('detail-trs');

			detailButtonsActive = true;
			showNotify(true);
		}, 450);
	}, 50);
});

//Save score details button

$('.save-score-details-button').click(function(){
	var changed = changedScoreDetails.filter(function(elem, index, self) {
		return index == self.indexOf(elem);
	});

	var changed = changed.filter(function(a){return a !== 'new'});
	var changedObjects	= new Array();
	var newObjects			= new Array();

	changed.forEach(function(val){
		var item = $('.score-details-wrap .detail-list-item[item-id=' + val + ']');

		changedObjects.push({
			id: val,
			ordering: item.attr('ordering'),
			title: item.find('.title-area').val(),
			desc: item.find('.desc-area').val()
		});
	});

	$('.score-details-wrap .detail-list-item[item-id=new]').each(function(){
		newObjects.push({
			ordering: $(this).attr('ordering'),
			title: $(this).find('.title-area').val(),
			desc: $(this).find('.desc-area').val()
		})
	});

	$.post(ajax, {
		admin_action: 'save_score_details',
		score_id: $('.score-details-wrap .current-score-id').html(),
		removed: removedScoreDetails,
		new: newObjects,
		changed: changedObjects
	}, function(data){
		permissionsHalt(data);
		if(data == 'success')
			showNotify(true);
			switchScoreDetails(false);
			pageBlockerTrigger(false);
			scoreDetailChanged = false;
	});
});

//Score detail list item size switching

$('body').on('focusin', '.detail-list-item textarea', function(){
	var item = $(this).parents('.detail-list-item');

	item.addClass('detail-trs-fast');
	item.removeClass('detail-list-item-minimized');

	setTimeout(function(){
		item.removeClass('detail-trs-fast');
	}, 300);
});

$('body').on('focusout', '.detail-list-item textarea', function(){
	var item = $(this).parents('.detail-list-item');

	item.addClass('detail-trs-fast');
	item.addClass('detail-list-item-minimized');

	setTimeout(function(){
		item.removeClass('detail-trs-fast');
	}, 300);
});

// Copy score to another response

var switchCopyScoreMode = function(trigger)
{
	if(trigger)
	{
		$('.question-scores-tabs .score-copy-block').removeClass('hidden');
		$('.copy-score-block').addClass('copy-score-block-hidden');
		setTimeout(function(){
			$('.stop-copy-score-button').removeClass('hidden');
			setTimeout(function(){
				$('.stop-copy-score-button').addClass('stop-copy-score-button-active');
			}, 50);
		},
		350);
	}
	else
	{
		$('.question-scores-tabs .score-copy-block').addClass('hidden');
		$('.stop-copy-score-button').removeClass('stop-copy-score-button-active');
		setTimeout(function(){
			$('.stop-copy-score-button').addClass('hidden');
			setTimeout(function(){
				$('.copy-score-block').removeClass('copy-score-block-hidden');
			}, 50);
		},
		350);
	}
};

var responseCopyNumber;
var tabButtonBackup;

var saveRestoreTabButton = function(trigger)
{
	var button = $('.question-scores-tabs .tab-button-back');
	var allWidth = 0;

	for(var i = 0; i < $('.question-scores-tabs .tab-button').length; i++)
	{
		allWidth += parseInt($('.question-scores-tabs .tab-button').eq(i).css('width'));
	}

	if(trigger)
	{
		tabButtonBackup = {
			width: button.css('width'),
			left: button.css('left')
		};

		button.css({
			width: allWidth + 'px',
			left: '0px'
		});
	}
	else
	{
		button.css({
			width: tabButtonBackup.width,
			left: tabButtonBackup.left
		});
	}
};

$('.copy-response-button').click(function(){
	responseCopyNumber = $(this).attr('response');
	switchCopyScoreMode(true);
	saveRestoreTabButton(true);
});

$('.stop-copy-score-button').click(function(){
	switchCopyScoreMode(false);
	saveRestoreTabButton(false);
});


$('body').on('click', '.score-item .score-copy-block', function(){
	if($(this).parents('.score-item').attr('score-id') == 'new')
	{
		return;
	}

	var itemId = $(this).parents('.score-item').attr('score-id');

	showModal(function(){
		$.post(ajax, {
			admin_action: 'copy_score_item',
			item: itemId,
			question: getParameterByName('id'),
			response: responseCopyNumber
		}, function(data){
			permissionsHalt(data);
			showNotify(true);

			saveRestoreTabButton(false);
			switchCopyScoreMode(false);

			var json_data = JSON.parse(data);

			var item = $('.score-item').first().clone();
			item.find('.score-text input').val(json_data.score_text);
			item.find('.score-value input').val(json_data.value);
			item.attr('score-id', json_data.id);
			item.attr('ordering', json_data.ordering);

			$('.score-items-wrap').eq(json_data.response - 1).append(item);
			setTimeout(function(){
				$('.question-scores-tabs .tab-button').eq(json_data.response - 1).click();
			}, 650);
		});
	},
	function(){
		switchCopyScoreMode(false);
		saveRestoreTabButton(false);
	},
	'Warning',
	'Result of this action will be saved immediately.',
	1);
});

/************************/
/*******Create user******/
/************************/

$('.generate-user-password').click(function(){
	$.post(ajax, {
		admin_action: 'get_random_hash',
		length: 10
	}, function(data){
		permissionsHalt(data);
		$('.new-user-password').val(data);
	});
});

$('.create-user-rule-buttons .all-read').click(function(){
	$('.page-rule-block .select-rule-dropdown .text').attr('value', 'read_only');
	$('.page-rule-block .select-rule-dropdown .text').html('View');
});

$('.create-user-rule-buttons .all-read-write').click(function(){
	$('.page-rule-block .select-rule-dropdown .text').attr('value', 'read_write');
	$('.page-rule-block .select-rule-dropdown .text').html('View and Change');
});

var parseRules = function(elements, parentVisible, pageRules)
{
	if(elements.length == 0)
		return;

	$(elements).each(function(){
		if(!parentVisible)
			visibleBuffer = false;
		else
			visibleBuffer = ($(this).find('.page-visible-trigger').attr('status') == 'checked' ? false : true);

		pageRules.push({
			id: $(this).attr('page-id'),
			visibility: visibleBuffer,
			rule: $(this).children('.page-rule-controls').find('.select-rule-dropdown .text').attr('value')
		});

		parseRules($(this).children('.page-rule-content').children('.page-rule-block'), pageRules[pageRules.length-1].visibility, pageRules);
	});
};

$('.create-user-button').click(function(){
	$('.input-error').each(function(element){
		if($(this).attr('style') != undefined || $(this).attr('style') == 'block')
			$(this).removeAttr('style');
	});

	var form = $('.create-admin-user-form');
	form.find('input').removeClass('red-input');

	//Checking inputs for empty
	if(form.find('.new-user-email').val() == '' || form.find('.new-user-password').val() == '')
	{
		$('.input-error.all-fields').slideToggle(300, 'easeInOutExpo');
		form.find('input').addClass('red-input');
		return;
	}

	//Checking for email wrong
	if(!email_regexp.test($('.new-user-email').val()))
	{
		$('.wrong-email').slideToggle(300, 'easeInOutExpo');
		form.find('.new-user-email').addClass('red-input');
		return;
	}

	//Path length
	if(form.find('.new-user-password').val().length < 8)
	{
		$('.pass-length').slideToggle(300, 'easeInOutExpo');
		form.find('.new-user-password').addClass('red-input');
		return;
	}

	//Grubbing rules data

	var pageRules = new Array();

	parseRules($('.rule-pages-list').children('.page-rule-block'), true, pageRules);

	$.post(ajax, {
		admin_action: 'check_admin_email',
		email: $('.new-user-email').val()
	}, function(data){
		permissionsHalt(data);
		if(data == 'success')
		{
			$.post(ajax, {
				admin_action: 'create_admin_user',
				rules: pageRules,
				email: $('.new-user-email').val(),
				password: $('.new-user-password').val(),
				mime_type: $('.user-mime-type-select').attr('value')
			}, function(data){
				permissionsHalt(data);
				if(data == 'success')
				{
					setNotify(data);
					location.href = 'index.php?page=admin-rules';
				}
			});
		}
		else if (data == 'exists')
		{
			$('.email-exists').slideToggle(300, 'easeInOutExpo');
			form.find('.new-user-email').addClass('red-input');
		}
	});
});

$('.page-visible-trigger').on('ontrigger', function(){
	var dropworn = $(this).siblings('.select-rule-dropdown');
	var ruleBlock = $(this).closest('.page-rule-block');

	ruleBlock.children('.page-rule-content').children('.disabled').removeClass('hidden');
	ruleBlock.children('.page-rule-content').addClass('blur-wrapper');

	dropworn.addClass('blur-wrapper');
	dropworn.find('.disabled').removeClass('hidden');
});

$('.page-visible-trigger').on('offtrigger', function(){
	var dropdown = $(this).siblings('.select-rule-dropdown');

	var ruleBlock = $(this).closest('.page-rule-block');

	ruleBlock.children('.page-rule-content').children('.disabled').addClass('hidden');
	ruleBlock.children('.page-rule-content').removeClass('blur-wrapper');

	dropdown.removeClass('blur-wrapper');
	dropdown.find('.disabled').addClass('hidden');
});

$('.page-rule-block .rule-caret').click(function(){
	item = $(this).closest('.page-rule-controls').siblings('.page-rule-content');

	if($(item).children().length == 1)
		return;

	$(this).toggleClass('rule-caret-active');
	item.toggleClass('page-rule-content-active');
	item.slideToggle(300, 'easeInOutExpo');
});

/************************/
/*******Admins list******/
/************************/

$('.user-actions-switch-wrap .switch').click(function(){
	if(!$(this).hasClass('switch-active') && $(this).attr('value') == 'show') // Show elements
	{
		$('.admin-actions-wrap').slideToggle(300, 'easeInOutExpo');
		$('.users-action-block').fadeToggle(300);
		$('.assign-checkbox-block').addClass('assign-checkbox-block-visible');
	}
	else if(!$(this).hasClass('switch-active') && $(this).attr('value') == 'hide') //Hide elements
	{
		$('.admin-actions-wrap').slideToggle(300, 'easeInOutExpo');
		$('.users-action-block').fadeToggle(300);
		$('.assign-checkbox-block').removeClass('assign-checkbox-block-visible');
	}

	$('.user-actions-switch-wrap .switch-active').removeClass('switch-active');
	$(this).addClass('switch-active');
});

$('.users-action-block').click(function(){
	var checkbox = $(this).find('.user-list-checkbox');

	if(checkbox.is(':checked'))
	{
		checkbox.attr('checked', false);
	}
	else
	{
		checkbox.attr('checked', 'checked');
	}
});

var getCheckedAdminUsers = function()
{
	var users = new Array();

	$('.list-item').each(function(){
		if($(this).find('.user-list-checkbox').is(':checked'))
		{
			users.push($(this).attr('user-id'));
		}
	});

	return users;
};

var deactivateAdminsList = function()
{
	$('.user-actions-switch-wrap .switch').first().trigger('click');
	$('.list-item .user-list-checkbox').attr('checked', false);
};

//Activate/Deactivate users

//Active 1 to actovate and 0 to deactivate
var setAdminActive = function()
{
	var users = getCheckedAdminUsers();
	var active = 0;

	if($(this).hasClass('activate-users'))
		active = 1;

	$.post(ajax, {
		admin_action: 'change_user_active',
		users: users,
		state: active
	}, function(data){
		permissionsHalt(data);
		showNotify(true);

		$('.list-item').each(function(){
			if($(this).find('.user-list-checkbox').is(':checked'))
			{
				if(active == 1)
					$(this).find('.active-col').html('yes');
				else
					$(this).find('.active-col').html('no');
			}
		});

		deactivateAdminsList();
	});
};

$('.admin-buttons .activate-users').click(setAdminActive);
$('.admin-buttons .deactivate-users').click(setAdminActive);

$('.admin-buttons .remove-users').click(function(){
	showModal(function(){
		var users = getCheckedAdminUsers();

		$.post(ajax, {
			admin_action: 'remove_admin_users',
			users: users
		}, function(data){
			permissionsHalt(data);
			showNotify(true);

			$('.list-item').each(function(){
				if($(this).find('.user-list-checkbox').is(':checked'))
				{
					$(this).addClass('trs-none');
					$(this).slideToggle(300, 'easeInOutExpo');
					var elementBuffer = $(this);

					setTimeout(function(){
						elementBuffer.remove();
					}, 350);
				}
			});

			setTimeout(function(){
				deactivateAdminsList();
			}, 400);
		});
	}, null, 
	'Warning', 
	'All user data will be completely removed.', 
	1);
});

$('.admin-buttons .change-type').click(function(){
	var users = getCheckedAdminUsers();

	$.post(ajax, {
		admin_action: 'change_admin_type',
		users: users,
		type: $('.user-mime-type-select').attr('value')
	}, function(data){
		permissionsHalt(data);
		showNotify(true);

		$('.list-item').each(function(){
			if($(this).find('.user-list-checkbox').is(':checked'))
			{
				$(this).find('.type-col').html(data);
			}
		});

		deactivateAdminsList();
	})
});

$('.update-admin-email').click(function(){
	$('.update-admin-email-form .input-error').each(function(element){
		if($(this).attr('style') != undefined || $(this).attr('style') == 'block')
			$(this).removeAttr('style');
	});

	$('.new-user-email').removeClass('red-input');

	if($('.new-user-email').val().length == 0)
	{
		$('.update-admin-email-form .required-field').slideToggle(300, 'easeInOutExpo');
		$('.new-user-email').addClass('red-input');
		return;
	}

	if(!email_regexp.test($('.new-user-email').val()))
	{
		$('.wrong-email').slideToggle(300, 'easeInOutExpo');
		$('.new-user-email').addClass('red-input');
		return;
	}

	$.post(ajax, {
		admin_action: 'check_admin_email',
		email: $('.new-user-email').val()
	}, function(data){
		permissionsHalt(data);
		if(data == 'success')
		{
			$.post(ajax, {
				admin_action: 'update_admin_email',
				email: $('.new-user-email').val(),
				id: getParameterByName('id')
			}, function(data){
				permissionsHalt(data);
				showNotify(true);
			});
		}
		else if (data == 'exists')
		{
			$('.email-exists').slideToggle(300, 'easeInOutExpo');
			$('.new-user-email').addClass('red-input');
		}
	});
});

$('.update-admin-password').click(function(){
	$('.update-admin-password-form .input-error').each(function(element){
		if($(this).attr('style') != undefined || $(this).attr('style') == 'block')
			$(this).removeAttr('style');
	});

	$('.new-user-password').removeClass('red-input');

	if($('.new-user-password').val().length == 0)
	{
		$('.update-admin-password-form .required-field').slideToggle(300, 'easeInOutExpo');
		$('.new-user-password').addClass('red-input');
		return;
	}

	if($('.new-user-password').val().length < 8)
	{
		$('.update-admin-password-form .pass-length').slideToggle(300, 'easeInOutExpo');
		$('.new-user-password').addClass('red-input');
		return;
	}

	$.post(ajax, {
		admin_action: 'update_admin_password',
		password: $('.new-user-password').val(),
		id: getParameterByName('id')
	}, function(data){
		permissionsHalt(data);
		showNotify(true);
		$('.new-user-password').val('');
	});
});

$('.update-admin-rules').click(function(){
	var pageRules = new Array();

	parseRules($('.rule-pages-list').children('.page-rule-block'), true, pageRules);

	$.post(ajax, {
		admin_action: 'update_admin_rules',
		rules: pageRules,
		id: getParameterByName('id')
	}, function(data){
		permissionsHalt(data);
		showNotify(true);
	});
});

/*Create test preset items*/

$('body').on('click', '.score-preset .remove-preset-button', function(){
	var item = $(this).closest('.score-preset');
	item.slideToggle(400, 'easeInOutExpo', function(){
		$(item).remove();
	});
});

$('.add-preset-button').click(function(){
	var proto = $('.score-preset-proto').clone();
	$('.presets-container').append(proto);
	proto.addClass('score-preset');
	proto.removeClass('score-preset-proto');
	proto.removeClass('hidden');
	setTimeout(function(){
		proto.slideToggle(400, 'easeInOutExpo');
	}, 100);
});

/**********************/
/*******User page******/
/**********************/

$('.user-invites .invite-item .remove-user-invite').click(function(){
	var item = $(this).closest('.invite-item');
	var inviteId = item.attr('item-id');
	var testId = item.attr('test-id');

	showModal(
		function(){
			item.slideToggle(400, 'easeInOutExpo');
			setTimeout(function(){
				item.remove();
			}, 450);

			$.post(ajax, {
				admin_action: 'remove_user_invite',
				user_id: getParameterByName('id'),
				invite_id: inviteId,
				test_id: testId
			}, function(data){
				permissionsHalt(data);
			});
		},
		null,
		'Remove user invite?',
		'All user progress will be removed.'
		);
});

$('.user-invites .invite-item .save-user-invite').click(function(){
	var item = $(this).closest('.invite-item');
	var inviteId = item.attr('item-id');
	var testId = item.attr('test-id');

	showModal(
		function(){
			switchLoading();
			$.post(ajax, {
				admin_action: 'update_user_invite',
				user_id: getParameterByName('id'),
				invite_id: inviteId,
				day: $(item).find('.calendar-widget .day').val(),
				month: $(item).find('.calendar-widget').attr('value'),
				year: $(item).find('.calendar-widget .year').val()
			}, function(data){
				permissionsHalt(data);

				if(data == 'time_error')
				{
					$(item).find('.calendar-widget .day').val($(item).attr('day'));
					$(item).find('.calendar-widget .year').val($(item).attr('year'));
					$(item).find('.calendar-widget').attr('value', $(item).attr('month'));
					$(item).find('.calendar-widget .text').text(months[parseInt($(item).attr('month')) - 1]);

					showModal(
						null,
						null,
						'Assignation error',
						"You can't assign completion date in past."
					);
				}
				else if(data == 'success')
				{
					$(item).attr('day', $(item).find('.calendar-widget .day').val());
					$(item).attr('year', $(item).find('.calendar-widget').attr('value'));
					$(item).attr('month', $(item).find('.calendar-widget .year').val());

					setNotify('success');
					showNotify();
				}

				switchLoading();
			});
		},
		null,
		'Update completion date?',
		'Are you really want update completion date?.'
		);
});

var ajaxWrap = function(options, callback = function(){}, method = 'POST', async = true)
{
	options['request_page'] = getParameterByName('page');

	$.ajax(ajax, {
		method: method,
		async: async,
		data: options,
		success: function(data){
			callback(data);
		}
	});
};

//Create user popup

var flushUserDialog = function()
{
	$('.create-user-popup .input-block input[type=text]').not('.password').val('');
};

$('.create-simple-user-button').click(function(){
	$('.create-user-popup').addClass('active');
	$('.page-blocker').addClass('page-blocker-active');
	$('.blur-blocker').addClass('blur-blocker-active');

	flushUserDialog();
	$('.create-user-popup .password').val(getRandom(8));
});

$('.create-user-popup .buttons-block .reject').click(function(){
	$('.create-user-popup').removeClass('active');
	$('.page-blocker').removeClass('page-blocker-active');
	$('.blur-blocker').removeClass('blur-blocker-active');
});

$('.create-user-popup .buttons-block .success').click(function(){
	var item = $(this).closest('.create-user-popup');
	var button = $(this);
	item.find('input').removeClass('wrong-input');

	var forReject = false;
	$(item).find('input').each(function(){
		if($(this).val() == '')
		{
			$(this).addClass('wrong-input');
			forReject = true;
			return false;
		}
	});

	if(forReject)
		return;

	if(!email_regexp.test(item.find('.email').val()))
	{
		item.find('.email').addClass('wrong-input');
		return;
	}

	if(item.find('.password').val().length < 8)
	{
		item.find('.password').addClass('wrong-input');
		return;
	}

	ajaxWrap({
		admin_action: 'email_exists',
		email: item.find('.email').val()
	}, function(data){
		if(data == 'rejected')
		{
			forReject = true;
			item.find('.email').addClass('wrong-input');
			return;
		}
		else
		{
			var currentTime = new Date().getTime();
			var assignTime = new Date(
				$('.create-user-assign-test-calendar .year').val(),
				$('.create-user-assign-test-calendar').attr('value'),
				$('.create-user-assign-test-calendar .day').val()
			).getTime();

			if($('#checkbox-create-user').is(':checked') && assignTime < currentTime)
			{
				showModal(
					null,
					null,
					'Assignation error',
					"You can't assign completion date in past."
				);
				
				forReject = true;
				return false;
			}

			switchLoading();
			$.post(ajax, {
				admin_action: 'create_simple_user',
				f_name:item.find('.f-name').val(),
				l_name: item.find('.l-name').val(),
				email: item.find('.email').val(),
				password: item.find('.password').val(),
				send_email: button.attr('send-email'),

				assign_sim: $('#checkbox-create-user').is(':checked'),
				test: $('.choose-user-test-create-user').find('.text').attr('value'),
				day: $('.create-user-assign-test-calendar .day').val(),
				month: $('.create-user-assign-test-calendar').attr('value'),
				year: $('.create-user-assign-test-calendar .year').val()
			}, function(data){
				permissionsHalt(data);

				flushUserDialog();
				$('.create-user-popup .password').val(getRandom(8));

				$('.create-user-popup').removeClass('active');
				$('.page-blocker').removeClass('page-blocker-active');
				$('.blur-blocker').removeClass('blur-blocker-active');

				setNotify('success');
				showNotify(true);

				if(data == 'time_error')
					showModal(
						null,
						null,
						'Assignation error',
						"You can't assign completion date in past."
					);

				$('.content-container').append($(data));
				setTimeout(function(){
					$('.user-list-item.result-list-item').last().addClass('loaded-list-item');
					switchLoading();
				}, 600);
			});
		}
	}, 'POST', false);

	if(forReject)
		return;
});

//Time picker

$('.time-picker .day-time-picker').click(function(){
	$(this).find('.day-item').toggleClass('active');
});

var disableQCheckbox = function(index)
{
	$('.question-seq-wrap input[type=checkbox]').eq(index).prop('checked', false);
	$('.question-seq-wrap input[type=checkbox]').eq(index).closest('.checkbox-block').addClass('blur-wrapper click-blocker');
}

var enableQCheckbox = function(index)
{
	$('.question-seq-wrap input[type=checkbox]').eq(index).closest('.checkbox-block').removeClass('blur-wrapper click-blocker');
}

$(".question-seq-wrap input.rev_of_ans").change(function(){
	if($(this).is(':checked'))
	{
		enableQCheckbox($('.question-seq-wrap input[type=checkbox]').index($(this))+1);
	}
});

$(".question-seq-wrap input.rev_of_ans").change(function(){
	if(!$(this).is(':checked'))
	{
		disableQCheckbox($('.question-seq-wrap input[type=checkbox]').index($(this))+1);
	}
});

$(".question-seq-wrap input.req_stn").change(function(){
	if($(this).is(':checked'))
	{
		enableQCheckbox($('.question-seq-wrap input[type=checkbox]').index($(this))+1);
	}
});

$(".question-seq-wrap input.req_stn").change(function(){
	if(!$(this).is(':checked'))
	{
		disableQCheckbox($('.question-seq-wrap input[type=checkbox]').index($(this))+1);
	}
});

//Assign sim while creating user

$('.create-user-assign-sim').change(function(){
	$('.create-user-popup .assign-sim-user-create').slideToggle(300, 'easeInOutExpo');
});