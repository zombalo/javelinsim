<?php
	require_once 'template/header.php';
?>

<div class="confirm-block">
	<div class="title">Error, page not found.</div>
	<div class="tologin-block">
		<a href="index.php" class="fa">
			<div class="button">Main page</div>
		</a>
	</div>
</div>

<?php
	require_once 'template/footer.php';
?>