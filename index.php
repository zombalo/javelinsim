<?php
	require_once 'config.php';
	ob_start();
	function colorRate($some, $of)
	{
		if($some < $of / 2)
			echo 'red';
		else if($some < $of * 0.75 && $some > $of / 2)
			echo 'yellow';
		else if($some > $of * 0.75)
			echo 'green';
	}

	function writeLog($message = '')
	{
		global $sqlc;
		$sqlc->query("INSERT INTO logs (data) VALUES('{$message}')");
	}

	function notFoundRedirect($object = '', $message = '')
	{
		global $sqlc;
		if($object == '' || $object != '' && $object == NULL)
		{
			if($message != '')
				writeLog($message);

			$sqlc->close();
			header('Location: 404.php');
			exit();
		}
	}

	function slidesCount($question)
	{
		$slides = 1;
		if($question['rerecord'] != 'None')
			$slides++;
		else if ($question['rerecord'] == 'None')
			return $slides;

		if($question['rev_of_ans'] == 'checked')
			$slides++;

		if($question['ans_af_rev'] == 'checked')
			$slides++;

		if($question['strong_require'] == 'checked')
			$slides++;

		if($question['ans_af_str'] == 'checked')
			$slides++;

		return $slides;
	}

	//Auth check
	$USER_ID;

	if(!isset($_GET['page']))
	{
		header('Location: ?page=overview');
	}
	else if(isset($_GET['page']) && 
		$_GET['page'] != 'login' && 
		$_GET['page'] != 'register' && 
		$_GET['page'] != 'confirm-account' &&
		$_GET['page'] != 'forgot-password' &&
		$_GET['page'] != 'restore-password')
	{
		if(isset($_COOKIE['user_hash']) && isset($_COOKIE['user_ss_id']))
		{
			$user_hash = hash('sha256', $_SERVER['HTTP_USER_AGENT']);
			if($user_hash == $_COOKIE['user_hash'])
			{
				session_id($_COOKIE['user_ss_id']);
				session_start();
			}
			else
			{
				setcookie('user_hash', null, -1);
				setcookie('user_ss_id', null, -1);
				header('Location: ?page=login');
			}
		}
		else
		{
			$user_hash = hash('sha256', $_SERVER['HTTP_USER_AGENT']);
			session_start();
			if(!isset($_SESSION['user_hash']) || $_SESSION['user_hash'] != $user_hash)
			{
				header('Location: ?page=login');
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://assets-cdn.ziggeo.com/v1-stable/ziggeo.css" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
	<link rel="stylesheet" href="stylesheet/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="js/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/less" href="stylesheet/test.less">
</head>
<body>
	<div class="container-fluid nopadding">
	<?php
	if(isset($_GET['page']))
	{
		if($_GET['page'] != 'register' && $_GET['page'] != 'login'&& $_GET['page'] != 'confirm-account' && $_GET['page'] != 'forgot-password' && $_GET['page'] != 'restore-password')
		{
		?>
		<div class="header-menu clearfix">
			<div class="top-part clearfix">
				<a href="?page=overview"><img class="logo" src="img/javelin-logo.jpg"></a>
				<div class="menu-button">
					<div class="button-row row-1"></div>
					<div class="button-row row-2"></div>
					<div class="button-row row-3"></div>
				</div>
			</div>
			<div class="items clearfix uselectn">
				<a href="?page=overview" class="menu-item">Overview</a>
				<a href="?page=take-a-sim" class="menu-item">Take a Simulation</a>
				<a href="?page=score-feedback" class="menu-item">Scores &amp; feedback</a>
				<a href="?page=account-settings&&id=<?php echo $_SESSION['user_id']?>" class="menu-item">Account &amp; Profile</a>
				<a href="?page=faqs" class="menu-item">FAQs</a>
				<div class="menu-item logout-button">Sign out</div>
			</div>
		</div>
		<?php
		}
		switch($_GET['page'])
		{
			case 'restore-password':
			{
				?>
				<div class="forgot-page">
					<div class="forgot-completed input-box hidden">
						<div class="title">Email sent</div>
						<div class="description">Please check your email for future instructions.</div>
						<a href="">
							<div class="forgot-button">Login page</div>
						</a>
					</div>
					<div class="forgot-email-input-box input-box">
						<div class="title">Change password</div>
						<div class="description">Please enter your new password<br>Password should contain more 7 characters</div>
						<div class="input-block clearfix">
							<div class="title">Password</div>
							<input class="new-password" type="password">
						</div>
						<div class="input-block clearfix">
							<div class="title">Password again</div>
							<input class="new-password-again" type="password">
						</div>
						<div class="change-forgoten-password forgot-button">Change password <i class="hidden fa fa-circle-o-notch active-loading"></i></div>
					</div>
				</div>
				<?php
				break;
			}
			case 'forgot-password':
			{
				?>
				<div class="forgot-page">
					<div class="forgot-completed input-box hidden">
						<div class="title">Email sent</div>
						<div class="description">Please check your email for future instructions.</div>
						<a href="?page=login">
							<div class="forgot-button">Login page</div>
						</a>
					</div>
					<div class="forgot-email-input-box input-box">
						<div class="title">Forgot password?</div>
						<div class="description">Let us help you, enter your email and you will get message with instructions.</div>
						<div class="input-block clearfix">
							<div class="title">Email</div>
							<input class="forgot-password-email" type="text">
						</div>
						<div class="sent-forgot-email forgot-button">Send email <i class="hidden fa fa-circle-o-notch active-loading"></i></div>
					</div>
				</div>
				<?php
				break;
			}
			case 'confirm-account':
			{
				$token = $_GET['token'];
				$link = 'login';
				$button = 'in';
				$message = 'Your email successfully confirmed, now you can Sign in';
				$action = true;

				$token_data = $sqlc->query("SELECT * FROM register_tokens WHERE register_token = '{$token}'")->fetch_assoc();

				if($token_data == NULL)
				{
					$message = 'Your registration requiest not found, you can go to Registration page to Sign up';
					$action = false;
				}
				else if($token_data['register_token'] == $token)
				{
					if(time() > intval($token_data['expire']))
					{
						$sqlc->query("DELETE FROM register_tokens WHERE register_token = '{$token}'");

						$message = 'Your registration request has expired, you can go to Registration page to Sign up';
						$action = false;
					}
					else
					{
						$sqlc->query("INSERT INTO users (name, sec_name, email, password) VALUES(
							'{$token_data['name']}',
							'{$token_data['sec_name']}',
							'{$token_data['email']}',
							'{$token_data['password']}'
						)");

						$sqlc->query("DELETE FROM register_tokens WHERE register_token = '{$token}'");
					}
				}

				if(!$action)
				{
					$button = 'up';
					$link = 'register';
				}

				?>
				<div class="confirm-page">
					<div class="confirm-block">
						<span><?php echo $message ?></span>
						<a href="?page=<?php echo $link ?>">
							<div class="button login-button preload">Sign <?php echo $button ?></div>
						</a>
					</div>
				</div>
				<?php
				break;
			}
			case 'sim-progress':
			{
				$test_data = $sqlc->query("SELECT * FROM tests WHERE id = '{$_GET['id']}'")->fetch_assoc();

				notFoundRedirect($test_data);

				$invite = $sqlc->query("SELECT * 
					FROM user_tests 
					WHERE user_id = '{$_SESSION['user_id']}' 
					AND test_id = '{$_GET['id']}'
					AND status = 'inprogress'")->fetch_assoc();

				notFoundRedirect($invite);

				$q_data = $sqlc->query("SELECT * 
					FROM questions 
					WHERE test_id = '{$_GET['id']}' 
					AND ordering = '{$invite['progress']}'")->fetch_assoc();

				notFoundRedirect($q_data);

				$answer = $sqlc->query("SELECT * FROM answers WHERE test_id = '{$_GET['id']}'
					AND user_id =  '{$_SESSION['user_id']}'
					AND question_id = '{$q_data['id']}'")->fetch_assoc();

				if($answer == NULL)
				{
					$sqlc->query("INSERT INTO answers (user_id, test_id, question_id) VALUES (
						'{$_SESSION['user_id']}',
						'{$_GET['id']}',
						'{$q_data['id']}'
					)");

					$answer = $sqlc->query("SELECT * FROM answers WHERE test_id = '{$_GET['id']}'
						AND user_id =  '{$_SESSION['user_id']}'
						AND question_id = '{$q_data['id']}'
						ORDER BY id DESC LIMIT 1")->fetch_assoc();
				}
				else if ($answer['state'] == 'finished')
				{
					notFoundRedirect();
				}
				else
				{
					$sqlc->query("UPDATE answers SET 
						record_1_token = '', 
						record_2_token = '', 
						record_3_token = ''
						WHERE id = '{$answer['id']}'");
				}

				if($q_data['answer_slides'] == '0')
				{
					$sqlc->query("UPDATE answers SET state = 'finished' WHERE id = '{$answer['id']}'");

					$last_order = $sqlc->query("SELECT ordering 
						FROM questions 
						WHERE test_id = '{$_GET['id']}' 
						ORDER BY ordering DESC 
						LIMIT 1")->fetch_assoc()['ordering'];


					if($invite['progress'] == $last_order)
						$sqlc->query("UPDATE user_tests SET status = 'finished' WHERE id = '{$invite['id']}'");
					else
						$sqlc->query("UPDATE user_tests SET progress = progress+1 WHERE id = '{$invite['id']}'");
				}

				$last_question_check = $sqlc->query("SELECT id 
					FROM questions 
					WHERE test_id = '{$_GET['id']}' 
					AND ordering > '{$q_data['ordering']}'
					ORDER BY ordering LIMIT 1")->fetch_assoc();
				?>

				<div class="hidden ans-id"><?php echo $answer['id'] ?></div>
				<div class="hidden invite-id"><?php echo $invite['id'] ?></div>

				<?php
				$slide_count = 1;
				$limit = $q_data['mins'] * 60 + $q_data['secs'];
				$rerecord = "false";

				if($q_data['rerecord'] == 'Re-record')
					$rerecord = "true";
				else
					$rerecord = "false";
				?>
				<div class="video-page">
					<div class="video-wrap">
						<div class="question-data clearfix">
							<div class="question-content">
								<div class="title">Question</div>
								<div class="value">
									<?php echo $q_data['question'] ?>
								</div>
							</div>
						</div>
						<div class="video-item">
							<div class="video-slide">
								<div class="video-header">Watch the video</div>
								<div class="video-content">
									<ziggeoplayer
									class="z-player"
									ziggeo-video=<?php echo $q_data['question_token'] ?>
									ziggeo-responsive=true
									ziggeo-height="340"
									></ziggeoplayer>
								</div>
							</div>
							<?php 
							if($q_data['rerecord'] != 'None')
							{
								$slide_count++;
							?>
							<div class="video-slide next hidden">
								<div class="video-header">Now record your response</div>
								<div class="video-content">
									<ziggeorecorder
									class="z-recorder for-rev-rec"
									ziggeo-picksnapshots=false
									ziggeo-countdown=0
									ziggeo-responsive=true
									ziggeo-timelimit=<?php echo $limit ?>
									ziggeo-height="340"
									ziggeo-width="600"
									ziggeo-auto-pad="true"
									ziggeo-rerecordable="<?php echo $rerecord ?>"
									></ziggeorecorder>
								</div>
							</div>
							<?php if($q_data['rev_of_ans'] == 'checked') {
								$slide_count++;
							?>
							<div class="video-slide next hidden">
								<div class="video-header">Review the answer you gave</div>
								<div class="video-content for-rev-pl">
									<div class="video-preload"><span>Video in processing</span><i class="fa fa-circle-o-notch active-loading"></i></div>
								</div>
							</div>
							<?php } 
							if($q_data['ans_af_rev'] == 'checked' && $q_data['rev_of_ans'] == 'checked') {
								$slide_count++;
							?>
							<div class="video-slide next hidden">
								<div class="video-header">Please rerecord your response</div>
								<div class="video-content">
									<ziggeorecorder
									class="z-recorder ans-af-rev"
									ziggeo-picksnapshots=false
									ziggeo-countdown=0
									ziggeo-responsive=true
									ziggeo-timelimit=<?php echo $limit ?>
									ziggeo-height="340"
									ziggeo-auto-pad="true"
									ziggeo-rerecordable="<?php echo $rerecord ?>"
									></ziggeorecorder>
								</div>
							</div>
							<?php } 
							if($q_data['strong_require'] == 'checked') {
								$slide_count++;
							?>
							<div class="video-slide next hidden">
								<div class="video-header">Review this example of a strong response</div>
								<div class="video-content">
									<ziggeoplayer
									class=z-player
									ziggeo-video=<?php echo $q_data['strong_token'] ?>
									ziggeo-responsive=true
									ziggeo-height="340"
									></ziggeoplayer>
								</div>
							</div>
							<?php } 
							if($q_data['strong_require'] == 'checked' && $q_data['ans_af_str'] == 'checked') {
								$slide_count++;
							?>
							<div class="video-slide next hidden">
								<div class="video-header">And now record your final version of your response</div>
								<div class="video-content">
									<ziggeorecorder
									class="z-recorder ans-af-str"
									ziggeo-picksnapshots=false
									ziggeo-responsive=true
									ziggeo-countdown=0
									ziggeo-timelimit=<?php echo $limit ?>
									ziggeo-height="340"
									ziggeo-auto-pad="true"
									ziggeo-rerecordable="<?php echo $rerecord ?>"
									></ziggeorecorder>
								</div>
							</div>
							<?php } ?>
						<?php }

						if($last_question_check == NULL)
						{
							?>
							<div class="video-slide next hidden">
								<div class="video-header">Simulation is done!</div>
								<div class="video-content">
									<div class="video-preload"><span>Thank you for submitting your simulation</span></div>
								</div>
							</div>
							<?php
						}
						?>
						</div>
						</div>
						<?php
						if($q_data['rerecord'] != 'None')
						{
							?>
							<div class="video-loadings">
								<div class="msg">Uploaded videos</div>
								<?php
								for($i = 0; $i < $q_data['answer_slides']; $i++)
								{
									?>
									<div class="load-item"><?php echo $i+1 ?></div>
									<?php
								}
								?>
							</div>
							<?php
						}
						?>
					</div>
					<?php
					//Prev next slides counter

					$prev_is = false;
					$next_is = false;

					$prev_count = 0;
					$next_count = 0;
					$current_count = 0;

					$prev_questions = $sqlc->query("SELECT * FROM questions WHERE test_id = '{$_GET['id']}' AND ordering < '{$q_data['ordering']}'");
					$next_questions = $sqlc->query("SELECT * FROM questions WHERE test_id = '{$_GET['id']}' AND ordering > '{$q_data['ordering']}'");

					foreach($prev_questions as $prev_question)
					{
						$prev_count += slidesCount($prev_question);
					}

					foreach($next_questions as $next_question)
					{
						$next_count += slidesCount($next_question);
					}

					$current_count = slidesCount($q_data);

					if($last_question_check == NULL)
						$current_count++;

					?>
					<div class="stats-wrap">
						<div class="question-titles clearfix">
							<div class="info-item clearfix">
								<div class="title">Simulation name</div>
								<div class="value"><?php echo $test_data['name'] ?></div>
							</div>
							<div class="info-item clearfix">
								<div class="title">Question name</div>
								<div class="value"><?php echo $q_data['title'] ?></div>
							</div>
						</div>
						<div class="ilcenter">
							<div class="controls clearfix">
								<?php
								if(intval($q_data['answer_slides']) > 0)
								{
								?>
								<div class="sim-button prev-button point-left"><i class="fa fa-chevron-left"></i>Prev. step</div>
								<?php
								}
								if($prev_count > 0)
								{
								?>
								<div class="prev-progress direction-progress"><?php echo $prev_count ?></div>
								<?php
								}
								?>
								<div class="progress-items inline floatleft">
									<?php
									$active_class = 'active';
									for($i = $prev_count; $i < $prev_count + $current_count; $i++)
									{
									?>
									<div class="progress-item <?php echo $active_class ?>"><?php echo $i + 1 ?></div>
									<?php
									$active_class = '';
									}
									?>
								</div>
								<?php
								if($next_count > 0)
								{
								?>
								<div class="next-progress direction-progress"><?php echo $next_count ?></div>
								<?php
								}

								$next_question = $sqlc->query("SELECT id 
									FROM questions 
									WHERE test_id = '{$_GET['id']}' 
									AND ordering > '{$q_data['ordering']}' ORDER BY ordering LIMIT 1")->fetch_assoc();

								if($q_data['rerecord'] != 'None')
								{
									?>
									<div class="sim-button next-button">Next step<i class="fa fa-chevron-right"></i></div>
									<?php
									if($next_question != NULL)
									{
										?>
										<a href="?page=sim-progress&id=<?php echo $_GET['id'] ?>">
											<div class="sim-button next-main-button hidden">Next question<i class="fa fa-play"></i></div>
										</a>
										<div class="sim-button next-main-button-loading hidden">Next question<i class="fa  fa-circle-o-notch active-loading"></i></div>
										<?php
									}
									else
									{
										?>
										<a href="?page=take-a-sim">
											<div class="sim-button next-main-button hidden">Submit<i class="fa fa-home"></i></div>
										</a>
										<div class="sim-button next-main-button-loading hidden">Submit<i class="fa fa-circle-o-notch active-loading"></i></div>
										<?php
									}
								}
								else
								{
									if($next_question != NULL)
									{
										?>
										<a href="?page=sim-progress&id=<?php echo $_GET['id'] ?>">
											<div class="sim-button next-main-button">Next question<i class="fa fa-play"></i></div>
										</a>
										<?php
									}
									else
									{
										?>
										<a href="?page=take-a-sim">
											<div class="sim-button next-main-button">Submit<i class="fa fa-home"></i></div>
										</a>
										<?php
									}
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<?php
				break;
			}
			case 'overview':
			{
				?>
				<div class="content-area">
					<div class="page-head clearfix">
						<img class="floatleft" src="img/overview.png">
						<div class="title floatleft">JAVELIN LEARNING SOLUTIONS SIMULATION PROGRAM PORTAL</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="content">
								Welcome to the Javelin Learning Solutions Portal!<br><br>
								The purpose of this program is to develop, improve, and sustain your communication, empathy, and leadership competencies through video simulations and feedback.
								<div class="note clearfix">
									<div class="icon"></div>
									<div class="content">Before you continue, please watch the following program overview video</div>
								</div>
								<video controls>
									<source src="https://s3.amazonaws.com/javelinio-hcahealthcare-videos/Program+Portal+Overview+Video+-+Healthcare+-+Kori+output.mp4?_=1">
								</video>
								To get started, click on <a href="?page=take-a-sim" class="link">take a simulation</a> at the top of the page.<br>
								Good luck on your simulation!
							</div>
						</div>
						<div class="col-sm-6">
							<div class="content">
								<!-- <div class="head">News</div> -->
								<div class="blog-post preload">
									<div class="head">Welcome to Javelin Learning Solutions' simulation platform</div>
									<div class="date"><i class="fa fa-calendar" aria-hidden="true"></i><?php echo date(' d.m.Y ') ?></div>
									<div class="content"> Please take your time to take a look around and orient yourself. We would love to hear your feedback. Send an email to <b><a href="mailto:assessments@javelinlearning.com?Subject=Suggestions">assessments@javelinlearning.com</a></b> and let us know what you like and what can be improved.</div>
								</div>
								</div>
						</div>
					</div>
				</div>
				<?php
				break;
			}
			case 'take-a-sim':
			{
				?>
				<div class="content-area">
					<div class="page-head clearfix">
						<img class="floatleft" src="img/sim-icon.png">
						<div class="title floatleft">Take a sim</div>
					</div>
					<div class="content">
						To begin a new simulation, select from the Pending Simulations below and click “Take Simulation”. You can access Pending Simulations and view your Completed Simulations on this page.<br><br>
						Please access your simulation through a laptop or desktop computer for the best user experience. The next version of the mobile app for Tablet, Android, and iPhone will be coming soon!
						<div class="note clearfix">
							<div class="icon"></div>
							<div class="content">The simulation platform requires the use of Firefox 3+, Internet Explorer (IE 9+ recommended), Safari 4+, or Chrome 3+, with the Flash 10.3 or greater browser plug-in and JavaScript enabled.</div>
						</div>
					</div>
					<div class="row">
						<?php
						$user_sims = $sqlc->query("SELECT * FROM user_tests WHERE user_id = '{$_SESSION['user_id']}' AND status <> 'finished' ORDER BY required_date");

						foreach($user_sims as $user_sim)
						{
							$required_time = date('m.d.Y', intval($user_sim['required_date']));
							$time_left = intval($user_sim['required_date']) - time();
							if($time_left > 0)
								$time_left = round($time_left / 86400) . ' Days';
							else
								$time_left = '0 Days (Expired)';

							$sim_name = $sqlc->query("SELECT name FROM tests WHERE id = '{$user_sim['test_id']}'")->fetch_assoc();

						?>
						<div class="col-sm-4">
							<div class="sim-block preload">
								<div class="head"><?php echo $sim_name['name'] ?></div>
								<div class="sim-details clearfix">
									<div class="title floatleft">Required Completion Date</div>
									<div class="value floatright"><?php echo $required_time ?></div>
								</div>
								<div class="sim-details clearfix">
									<div class="title floatleft">Time Remaining Until Close</div>
									<div class="value floatright"><?php echo $time_left ?></div>
								</div>
								<div class="take-sim-wrap">
									<a href="?page=sim-progress&id=<?php echo $user_sim['test_id'] ?>">
										<div class="regular-button">Start simulation <i class="fa fa-play"></i></div>
									</a>
								</div>
							</div>
						</div>
						<?php
						}
						?>
					</div>
				</div>
				<?php
				break;
			}
			case 'video-feedback':
			{
				?>
				<div class="content-area">
					<div class="page-head clearfix">
						<img class="floatleft" src="img/feedback.png">
						<div class="title floatleft">Scores &amp; feedback</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="video-page-head clearfix">
								<div class="title">Nurse feedback (action)</div>
								<div class="score <?php colorRate(66,7, 100) ?>">Your Score: 6/9 behaviors (66.67%)</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="dropdown clearfix margin-10 margin-right-20">
								<div class="title">Choose question <i class="fa fa-caret-down"></i></div>
								<div class="items">
									<a href="?page=overview"><div class="item">Question 1</div></a>
									<a href="?page=overview"><div class="item">Question 2</div></a>
									<a href="?page=overview"><div class="item">Question 3</div></a>
									<a href="?page=overview"><div class="item">Question 4</div></a>
								</div>
							</div>
							<a href="?page=overview"><div class="regular-button floatleft margin-10 margin-right-20 left-hover"><i class="fa fa-chevron-left"></i> View your results</div></a>
							<a href="?page=overview"><div class="regular-button floatleft margin-10 margin-right-20">View your results <i class="fa fa-chevron-right"></i></div></a>
						</div>
						<div class="col-sm-6">
							<div class="video-block">
								<div class="head">Question</div>
								<video controls>
									<source src="https://s3.amazonaws.com/montagedataserverprod/clients/3746a915-6c90-45e0-a67a-3c23706fc3d1/candidates/f75b9167-cbeb-468f-9f24-56091f63885f/media/596db0cd-addb-4629-8f6d-8f9d91e8ad6f.mp4" type="video/mp4">
								</video>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="video-block">
								<div class="head">Your Response</div>
								<video controls>
									<source src="https://s3.amazonaws.com/montagedataserverprod/clients/3746a915-6c90-45e0-a67a-3c23706fc3d1/candidates/9f39e3f3-704f-405d-af9b-540bdc58ed3d/media/294412ce-30aa-4d34-8120-89cbcbfe4a8f.mp4" type="video/mp4">
								</video>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="observe">
								<div class="title">Observe</div>
								<div class="content">
									As you reflect on the scenario you just watched, think about the interpersonal competencies of Communication, Empathy and Physician Leadership and use them to guide your response.
									<br>
									<br>
									<ul>
										<li>What did you see or hear Dr. West do that indicated that she both monitored and provided corrective guidance to Frank's patient care?</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="col-sm-6">
								<div class="video-block">
									<div class="head">Strong response</div>
									<video controls>
										<source src="https://s3.amazonaws.com/javelinio-hcahealthcare-videos/838.mp4" type="video/mp4">
									</video>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="score-col">
								<div class="main-head">Communication</div>
								<div class="sub-head">4/5 behaviors demonstrated</div>
								<div class="score-block">
									<table class="score-row">
										<tbody>
											<tr>
												<td>Behavior 1</td>
												<td><i class="fa fa-check green"></i> Express Your Thoughts (Message)</td>
											</tr>
											<tr>
												<td>What is this important?</td>
												<td>Articulating your ideas allows others to understand your actions and behaviors. It helps to lower the risk of misunderstandings and helps teams to work together better.</td>
											</tr>
											<tr>
												<td>Suggested Key Phrase</td>
												<td></td>
											</tr>
										</tbody>
										<tbody>
											<tr>
												<td>Behavior 1</td>
												<td><i class="fa fa-times red"></i> Express Your Thoughts (Message)</td>
											</tr>
											<tr>
												<td>What is this important?</td>
												<td>Articulating your ideas allows others to understand your actions and behaviors. It helps to lower the risk of misunderstandings and helps teams to work together better.</td>
											</tr>
											<tr>
												<td>Suggested Key Phrase</td>
												<td></td>
											</tr>
										</tbody>
										<tbody>
											<tr>
												<td>Behavior 1</td>
												<td><i class="fa fa-check green"></i> Express Your Thoughts (Message)</td>
											</tr>
											<tr>
												<td>What is this important?</td>
												<td>Articulating your ideas allows others to understand your actions and behaviors. It helps to lower the risk of misunderstandings and helps teams to work together better.</td>
											</tr>
											<tr>
												<td>Suggested Key Phrase</td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="score-col">
								<div class="main-head">Communication</div>
								<div class="sub-head">4/5 behaviors demonstrated</div>
								<div class="score-block">
									<table class="score-row">
										<tbody>
											<tr>
												<td>Behavior 1</td>
												<td><i class="fa fa-check green"></i> Express Your Thoughts (Message)</td>
											</tr>
											<tr>
												<td>What is this important?</td>
												<td>Articulating your ideas allows others to understand your actions and behaviors. It helps to lower the risk of misunderstandings and helps teams to work together better.</td>
											</tr>
											<tr>
												<td>Suggested Key Phrase</td>
												<td></td>
											</tr>
										</tbody>
										<tbody>
											<tr>
												<td>Behavior 1</td>
												<td><i class="fa fa-times red"></i> Express Your Thoughts (Message)</td>
											</tr>
											<tr>
												<td>What is this important?</td>
												<td>Articulating your ideas allows others to understand your actions and behaviors. It helps to lower the risk of misunderstandings and helps teams to work together better.</td>
											</tr>
											<tr>
												<td>Suggested Key Phrase</td>
												<td></td>
											</tr>
										</tbody>
										<tbody>
											<tr>
												<td>Behavior 1</td>
												<td><i class="fa fa-check green"></i> Express Your Thoughts (Message)</td>
											</tr>
											<tr>
												<td>What is this important?</td>
												<td>Articulating your ideas allows others to understand your actions and behaviors. It helps to lower the risk of misunderstandings and helps teams to work together better.</td>
											</tr>
											<tr>
												<td>Suggested Key Phrase</td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="score-col">
								<div class="main-head">Communication</div>
								<div class="sub-head">4/5 behaviors demonstrated</div>
								<div class="score-block">
									<table class="score-row">
										<tbody>
											<tr>
												<td>Behavior 1</td>
												<td><i class="fa fa-check green"></i> Express Your Thoughts (Message)</td>
											</tr>
											<tr>
												<td>What is this important?</td>
												<td>Articulating your ideas allows others to understand your actions and behaviors. It helps to lower the risk of misunderstandings and helps teams to work together better.</td>
											</tr>
											<tr>
												<td>Suggested Key Phrase</td>
												<td></td>
											</tr>
										</tbody>
										<tbody>
											<tr>
												<td>Behavior 1</td>
												<td><i class="fa fa-times red"></i> Express Your Thoughts (Message)</td>
											</tr>
											<tr>
												<td>What is this important?</td>
												<td>Articulating your ideas allows others to understand your actions and behaviors. It helps to lower the risk of misunderstandings and helps teams to work together better.</td>
											</tr>
											<tr>
												<td>Suggested Key Phrase</td>
												<td></td>
											</tr>
										</tbody>
										<tbody>
											<tr>
												<td>Behavior 1</td>
												<td><i class="fa fa-check green"></i> Express Your Thoughts (Message)</td>
											</tr>
											<tr>
												<td>What is this important?</td>
												<td>Articulating your ideas allows others to understand your actions and behaviors. It helps to lower the risk of misunderstandings and helps teams to work together better.</td>
											</tr>
											<tr>
												<td>Suggested Key Phrase</td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				break;
			}
			case 'tables-feedback':
			{
				?>
				<div class="content-area">
					<div class="page-head clearfix">
						<img class="floatleft" src="img/feedback.png">
						<div class="title floatleft">Scores &amp; feedback</div>
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div class="sim-block preload">
								<div class="head">Nurse feedback</div>
								<div class="sim-details table-head clearfix">
									<div class="left floatleft">Question</div>
									<div class="center floatleft">Scores (behavior exhibited)</div>
									<div class=" right floatleft">Percent</div>
								</div>
								<div class="sim-details table-content clearfix">
									<div class="left floatleft">Observe</div>
									<div class="center floatleft <?php colorRate(3.5, 9) ?>">3.5/9</div>
									<div class="right floatleft <?php colorRate(38.9, 100) ?>">38.9%</div>
								</div>
								<div class="sim-details table-content clearfix">
									<div class="left floatleft">Action</div>
									<div class="center floatleft <?php colorRate(6, 9) ?>">6.0/9</div>
									<div class="right floatleft <?php colorRate(66.7, 100) ?>">66.7%</div>
								</div>
								<div class="sim-details table-content clearfix">
									<div class="left floatleft">TOTAL</div>
									<div class="center floatleft <?php colorRate(9.5, 18) ?>">9.5/18</div>
									<div class="right floatleft <?php colorRate(52.8, 100) ?>">52.8%</div>
								</div>
								<div class="take-sim-wrap">
									<a href="?page=video-feedback">
										<div class="regular-button">View your results <i class="fa fa-play"></i></div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				break;
			}
			case 'terms-of-use':
			{
				?>
				<div class="content-area">
					<div class="page-head clearfix">
						<img class="floatleft" src="img/terms-icon.png">
						<div class="title floatleft">JAVELIN HR TERMS OF USE</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="content">
								<p><strong>Javelin Platform End User License Agreement</strong></p>
								<p>Updated July 14, 2016</p>
								<p>IMPORTANT: READ CAREFULLY.&nbsp; This End User License Agreement (“<strong>EULA</strong>”) constitutes a legal agreement between you, the end user, and ePsychometrics, Inc. d/b/a Javelin HR Solutions (“we,” “us,” or “<strong>Javelin</strong>”), for the use of Javelin’s online assessment administration and training platform (the “<strong>Platform</strong>”).&nbsp; The Platform is intended and authorized for use only by end users holding valid usernames and passwords, and only during the term of a Subscription.&nbsp; A “<strong>Subscription</strong>” is a right to access the Platform for the period during which: (a) a third party entity has contracted with Javelin for your access to the Platform (such contracting entity is referred to herein as a “<strong>Customer</strong>”); (b) you have contracted with Javelin for your access to the Platform; or (c) Javelin has otherwise granted access to you, by issuing login credentials to you.&nbsp; The Platform is designed to record assessment results, to assist in the evaluation and assessment of these results, to provide interactive training resources based on these results and assessments, to track, analyze, and compare all such results, and to provide assessment and other information to the Customer, together with such other related features and functionality as may be incorporated into the Platform from time to time (the “<strong>Purpose</strong>”).&nbsp; By clicking on the “Accept” button or otherwise using the Platform, you agree to be bound by the terms of this EULA as set out below.&nbsp; If you do not agree to the terms of this EULA, you may not use the Platform.</p>
								<ol>
								<li><strong>Access to Platform and Documentation.</strong> During the Subscription, we grant you a limited, revocable, non-exclusive, non-transferable right to (i) use the Platform as provided in this EULA; and (ii) use the training and educational materials, including video content we provide in connection with the Platform (the “<strong>Documentation</strong>”) solely as necessary to allow you to use the Platform for Purpose. The Documentation may not be copied, modified or used in any way not expressly authorized herein. Ownership, rights and intellectual property in the Platform shall remain with us or our licensors.&nbsp; No rights to the Platform are granted except as specifically provided herein. &nbsp;You agree to use the Platform only in accordance with any terms of use, honor codes and other rules posted on the Platform or provided by the Customer arranging for your access, if applicable; among other remedies, any failure to comply with the foregoing may be reported to such Customer.</li>
								</ol>
								<ol start="2">
								<li><strong>Your Use of the Platform.</strong></li>
								</ol>
								<p><strong>a.</strong> You may be asked to submit materials, to provide responses, or to otherwise interact through the Platform in a way that results in recorded data and materials necessary or useful for the Purpose (“<strong>Your Data and Materials</strong>”) that may be transmitted to the Customer, or otherwise used, assessed and provided to the Customer.</p>
								<p><strong>b.</strong> You grant to us and our licensors and service providers a non-exclusive, world-wide, irrevocable, perpetual, royalty-free license to reproduce, use, copy or transmit Your Data and Materials (including the right to aggregate Your Data and Materials with other data): (1) for the Purpose; (2) for improving the Platform and similar services; (3) for ensuring compliance with the terms applicable to the Platform; (4) for verifying the charges and payments applicable to the Subscription; (5) in aggregated form, to create Javelin proprietary analyses, reports, databases and whitepapers, for distribution and publication in connection with our business.</p>
								<p><strong>c.</strong> You grant us and our licensors and service providers the irrevocable, perpetual right to compile statistics about your use of the Platform, and to otherwise evaluate and improve the Platform based on your usage and Your Data and Materials, and to make such statistics available to third parties only in a de-identified form.</p>
								<p><strong>d.</strong> You authorize us to amend or delete any of Your Data and Materials where we become aware (through receipt of a third party notice or otherwise) that Your Data and Materials are, or are alleged to be, falsified, not undertaken in good faith, in breach of copyright, illegal or otherwise not appropriate in our opinion.</p>
								<p><strong>e.</strong> Javelin makes no warranty or guarantee that the Platform will be compatible with your existing hardware software or environment.</p>
								<p><strong>f.</strong> To the fullest extent permitted by law, you access and use the Platform at your own risk. You accept responsibility for all of Your Data and Materials and indemnify us, our licensors and service providers against any liability, costs, losses, claims, or damages (including legal fees and disbursements) incurred or suffered by us in relation to:</p>
								<p style="padding-left: 30px;"><strong>i.</strong> Your Data and Materials, including but not limited to claims for: infringement of intellectual property rights or other proprietary rights; violation of laws, regulations, or codes; injury or death; breach of privacy or confidence;</p>
								<p style="padding-left: 30px;"><strong>ii.</strong> Breach of this EULA, including (without limitation) Sections 3 (Platform Usage Restrictions), 4 (Platform Usage Obligations), 5 (Confidential Information) and 13 (Term of EULA), and enforcing our rights under the EULA.</p>
								<ol start="3">
								<li><strong>Platform Usage Restrictions.</strong> You may not: (a) copy, reproduce, translate, adapt, modify, reverse engineer, decompile or modify the Platform; (b) resell, rent, lease, assign or otherwise transfer rights to the Platform; or (c) translate all or any portion of the Platform onto or into other computer software.</li>
								</ol>
								<ol start="4">
								<li><strong>Platform Usage Obligations.</strong> You shall: (a) use the Platform for lawful purposes only; (b) not use the Platform in a way which interferes with or disrupts the hosting facilities or any other user’s use of the Platform; (c) comply with the operational rules (if any), acceptable use policies and technical requirements that we make available to you from time to time; (d) take such actions as are required to ensure that your access and use of the Platform complies with any laws, statutes, regulations, decrees or orders applicable to your use of the Platform (including but not limited to laws regarding privacy, export and import); (e) not take any steps to defeat any host system controls over the download of parts of the Site or software that can be downloaded. You hereby acknowledge that you are not required to provide Data and Materials without having received Notice and Choice regarding such Data and Materials.&nbsp; “Notice and Choice” means that you have, at the time Your Data and Materials are supplied by you to us, received disclosure of the Purpose as stated above, and/ or of the Customer’s information practices and you have had an opportunity to exercise choice regarding your sharing of such information, including your provision of such information through the Javelin Platform.</li>
								</ol>
								<p>You acknowledge and agree that the Platform may not be exported or otherwise used in Cuba, Iran, Libya, North Korea, Syria or any other country to which the United States has embargoed goods, or to a national or resident thereof, or to anyone on the United States Treasury Department’s List of Specially Designated Nations, or the United States Commerce Department’s Table of Denial Orders. You further represent and warrant that you are not located in, under the control of, or a national or resident of any such country or on any such list.</p>
								<p>You will indemnify us and our licensors and service providers fully against all liabilities, costs and expenses that we may suffer as a result of your breach of Section 3 or 4.</p>
								<ol start="5">
								<li><strong>Confidential Information.</strong> You agree to keep confidential and not to use for any purpose other than as necessary to allow you to use the Platform as permitted herein, all information relating to the performance, operation, structure, methods, programming and documentation of the Platform, unless you have obtained our prior written approval to your proposed use or disclosure.</li>
								</ol>
								<ol start="6">
								<li><strong>Software Version.</strong> We reserve the right to modify or update the Platform as we deem necessary. The Platform may be modified to better meet the needs of Javelin’s customers.</li>
								</ol>
								<ol start="7">
								<li><strong>Shared Environment.</strong> The Platform may be provided through shared use of hosting facilities with other customers. We will take all reasonable steps to ensure that design problems, errors or outages with any particular customer’s instance of the Platform do not cause damage or outages in our other customers’ instances. However, you acknowledge that: (a) the risk of such damage, outages, or data corruption cannot be eliminated entirely; (b) where an outage occurs, the procedure and timing for restoring the Platform will be at our discretion; and (c) we may suspend Platform to you where, in our opinion, your usage of the Platform is or is likely to adversely affect our Platform or our ability to supply services.</li>
								</ol>
								<ol start="8">
								<li>We may suspend, disconnect or discontinue the Platform in whole or in part at any time without notice and without compensation if in our reasonable opinion: (a) it is necessary to safeguard the provision of the Platform or the integrity of the hosting facilities; (b) the Platform or the hosting facilities fails or requires modification or maintenance; (c) there is or has been unauthorized, unlawful or fraudulent use of the Platform or your use of the Platform is causing or may potentially cause damage or interference to the hosting facilities; (d) it is necessary to comply with a direction, order or request of any government authority or other competent authority; or (e) you do not comply with any of the terms of an agreement with us.</li>
								</ol>
								<ol start="9">
								<li>You are fully responsible for the safekeeping of your user identification codes and passwords and for all use of or access to the Platform by anyone using your user identification and passwords. You are solely responsible for any disclosure of the user identification codes or passwords provided to you. We may, by notice in writing, require you to immediately change any of your user identification codes or passwords.&nbsp; You agree to contact us immediately if you suspect any unauthorized use or disclosure of your user identification codes or passwords.&nbsp; We may immediately terminate or suspend user passwords if we (in our discretion) consider this to be necessary or appropriate, including where (without limitation) the relevant project (or your involvement in it) ends, or for the reasons described in Section 9.</li>
								</ol>
								<ol start="10">
								<li><strong>Warranty Disclaimer.</strong> THE PLATFORM IS PROVIDED “AS IS” BY JAVELIN AND WITHOUT WARRANTY OF ANY KIND. JAVELIN, AND EACH OF ITS LICENSORS AND SERVICE PROVIDERS, EXPRESSLY DISCLAIM ALL WARRANTIES IN RELATION TO THE PLATFORM, EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. NEITHER JAVELIN NOR ANY LICENSOR OR SERVICE PROVIDER WARRANTS THAT THE FUNCTIONS CONTAINED IN THE PLATFORM WILL MEET YOUR NEEDS, OR THAT THE OPERATION OF THE PLATFORM WILL BE UNINTERRUPTED OR ERROR-FREE.&nbsp; NEITHER JAVELIN, NOR ANY LICENSOR OR SERVICE PROVIDER, MAKES ANY WARRANTIES, EXPRESS OR IMPLIED, WITH RESPECT TO DATA OR MATERIALS ASSOCIATED WITH THE PLATFORM, OR FOR THE ACCURACY, CURRENCY OR COMPREHENSIVENESS OF THE SAME. NO OTHER ORAL OR WRITTEN INFORMATION PROVIDED BY JAVELIN, OR BY ANY THIRD PARTY SHALL CREATE A WARRANTY OR INCREASE THE SCOPE OF ANY WARRANTY.</li>
								</ol>
								<ol start="11">
								<li><strong>Limitation of Liability.</strong> To the maximum extent permitted by applicable law, neither we nor any licensor or service provider shall be liable for any damages whatsoever (including, without limitation, damages for loss of business profits, business interruption, loss of business information, or other pecuniary loss) arising out of the use of or inability to use the Platform even if we have been advised of the possibility of such damages. You acknowledge and agree that the Platform is an online service used for the Purpose, but that services and materials available through the Platform may be provided by or on behalf of the Customer, by assessors, or by others, and that Javelin has no responsibility or liability with respect to the assessments, results or reports made available in connection with the Platform, or with any other communication undertaken through the Platform.</li>
								</ol>
								<ol start="12">
								<li><strong>Copyright and Trademark.</strong> You acknowledge that the Platform and the Documentation are protected by copyright laws and international copyright treaties, as well as other intellectual property rights and treaties. You will not, during or any time after the termination of this EULA or discontinuance of the Platform, commit or permit any act which infringes those intellectual property rights. You acknowledge that certain marks identified as registered or unregistered trademarks or service marks are the exclusive property of Javelin or its licensors or service providers, and that no right to use such marks is granted pursuant to this EULA.&nbsp; You will not remove, deface or combine with any other mark or symbol, any marks contained in the service or provided by Javelin.</li>
								</ol>
								<ol start="13">
								<li><strong>Term of EULA.</strong> This EULA commences when you receive your login credentials for the Platform and shall continue until the expiration of the applicable Subscription or your access is terminated in accordance herewith. This EULA shall terminate automatically and immediately if you fail to comply with any of the terms and conditions set forth herein.</li>
								</ol>
								<ol start="14">
								<li><strong>Updates and Modifications</strong>.&nbsp; You acknowledge and agree that the Purpose for which the Platform may be used may change over time.&nbsp; To the extent that any changes are reflected in this EULA, you consent and agree to all such changes: (A) of which you are notified, and after which notification you continue to use the Platform; or (B) reflected in a revised EULA accepted by you by clicking “Accept” or other methods of acceptance.</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
				<?php
				break;
			}
			case 'charts-feedback':
			{
				?>
				<div class="content-area">
					<div class="page-head clearfix">
						<img class="floatleft" src="img/feedback.png">
						<div class="title floatleft">Scores &amp; feedback</div>
					</div>
					<div class="row">
						<div class="col-sm-5">
							<div class="content">
								<div>
									<a href="?page=tables-feedback">
										<div class="regular-button margin-10">Review your results <i class="fa fa-play"></i></div>
									</a>
								</div>
								<div class="under-head">Brandon Session 1</div>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum numquam sed, nesciunt deserunt assumenda itaque rem adipisci ratione nisi ullam minima culpa voluptate maiores perferendis beatae id est, atque eligendi similique excepturi dicta? Quidem quos, accusantium distinctio dolore deserunt soluta dolorum laudantium nihil, non repellat, officia. Id, quis autem error eveniet alias. Deleniti, aperiam, earum? Cupiditate, magni voluptatum sapiente quam tempora, amet dicta dolorum quia molestiae minus officia, veritatis nostrum perferendis. Quis error ducimus officia deleniti cum, magnam tenetur necessitatibus impedit! Nulla corporis rem repellat deleniti dolores, quisquam velit sed earum illum ea saepe totam doloribus omnis officia possimus architecto natus enim. Possimus nisi earum velit nulla maxime reprehenderit, et totam quam ea aliquam illum. Ullam rerum consequatur quisquam labore quas veniam aliquid? Officia aut eveniet, aliquam quis, ea similique quia! Repudiandae amet id blanditiis, ea suscipit consequuntur a aspernatur rerum possimus consequatur quasi, voluptas quis ipsum qui doloremque sunt minus officiis illum corrupti exercitationem illo alias ut magni sint! Quas, ratione voluptatibus ipsum velit accusantium id porro qui repellendus excepturi voluptatum amet mollitia ex aliquid adipisci repudiandae earum accusamus illum, similique nisi voluptate, quis hic, vitae soluta. Mollitia nam repellendus magnam fugiat ipsa, ratione praesentium obcaecati in. Eius, minima.
							</div>
						</div>
						<div class="col-sm-7">
							<div class="tabs-widget">
								<div class="tabs clearfix">
									<label class="tab-item active">Tab name 1</label>
									<label class="tab-item">Tab name 2</label>
									<label class="tab-item">Tab name 3</label>
									<label class="tab-item">Tab name 4</label>
								</div>
								<div class="cards">
									<label class="card-tab-item active block">
										<div class="feed-back-1-tab-chart visual-chart"></div>
									</label>
									<label class="card-tab-item">
										<div class="row">
											<div class="col-sm-4">
												<!-- <div class="feed-back-2-tab-chart-1 visual-chart"></div> -->
											</div>
											<div class="col-sm-4">
												
											</div>
											<div class="col-sm-4">
												
											</div>
										</div>
									</label>
									<label class="card-tab-item"></label>
									<label class="card-tab-item"></label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				break;
			}
			case 'score-feedback':
			{
				?>
				<div class="content-area">
					<div class="page-head clearfix">
						<img class="floatleft" src="img/feedback.png">
						<div class="title floatleft">Scores &amp; feedback</div>
					</div>
					<div class="content">
						The link below will take you to a summary of all the simulations you have completed across all events:<br>
						<div class="regular-button margin-10">View All Scenariors</div><br>
						Select a test date to review details of your score and gain insight on areas for improvement.
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div class="sim-block preload">
								<div class="head">Simulation name</div>
								<div class="sim-details clearfix">
									<div class="title floatleft">Assessment Date</div>
									<div class="value floatright">09/25/2017</div>
								</div>
								<div class="sim-details clearfix">
									<div class="title floatleft">Your Score</div>
									<div class="value floatright">25.0/36</div>
								</div>
								<div class="sim-details clearfix">
									<div class="title floatleft">Your Previous Score</div>
									<div class="value floatright">N/A</div>
								</div>
								<div class="sim-details clearfix">
									<div class="title floatleft">Change from Previous Score</div>
									<div class="value floatright">N/A</div>
								</div>
								<div class="sim-details clearfix">
									<div class="title floatleft">Average Peer Score</div>
									<div class="value floatright">0.0/36</div>
								</div>
								<div class="take-sim-wrap">
									<a href="?page=charts-feedback">
										<div class="regular-button">Review your results <i class="fa fa-play"></i></div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				break;
			}
			case 'account-settings':
			{
				if($_GET['id'] == $_SESSION['user_id']) {
					$data = $sqlc->query("SELECT name, sec_name, email FROM users WHERE id = '{$_GET['id']}'")->fetch_assoc();
					print_r($_SESSION['user_id']);

				?>
				<div class="content-area">
					<div class="page-head clearfix">
						<img class="floatleft" src="img/user-icon.png">
						<div class="title floatleft">Account settings</div>
					</div>
					<div class="row">
						<div class="col-sm-5">

						<div class="register-complete hidden">
								<img class="preload" src="img/javelin-logo.jpg">
								<div class="title preload">Done</div>
								<span>Confirmation email was sent to you</span>
								<div class="ilcenter preload">
									<a href="?page=login" class="action-link white">Sign in</a>
								</div>
						</div>

							<div class="input-block fullwidth hightlight account-inputs preload">
								<div class="required-field">Field required</div>
								<input type="text" class="hidden-id"  aria-hidden="true" value=<?php echo $_GET['id'] ?>>
								<input type="text" class="f-name"  placeholder="First name" value=<?php echo $data["name"] ?>>
								<input type="text" class="s-name" placeholder="Second name" value="<?php echo $data['sec_name']?>">
								<input type="text" class="user-email input-check" placeholder="Email" value="<?php echo $data['email']?>">
								<input type="password" class="password" placeholder="Password" value="">
								<input type="password" class="password-again" placeholder="Password again" value="">
								<div class="update-button">Update</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				break; }
			}
			case 'login':
			{
				?>
				<div class="row nomargin login-page">
					<div class="col-sm-6 nopadding">
						<div class="sides left-side">
							<div class="input-block margin-top-110">
								<img class="preload" src="img/javelin-logo.jpg">
								<div class="title preload">Sign in</div>
								<div class="required-field">Field required</div>
								<input class="input-check user-email preload" type="text" placeholder="Login">
								<input class="input-check password preload" type="password" placeholder="Password">
								<div class="illeft preload">
									<div class="terms checkbox clearfix white inline">
										<input class="input-check" type="checkbox" id="input-id-1">
										<label for="input-id-1" class="check"></label>
										<a href="?page=terms-of-use">
											<label for="input-id-1" class="head">I agree with terms and conditions</label>
										</a>
									</div>
								</div>
								<div class="illeft preload">
									<div class="remember checkbox clearfix white inline margin-bottom-10 uselectn">
										<input class="input-check" type="checkbox" id="input-id-2">
										<label for="input-id-2" class="check"></label>
										<label for="input-id-2" class="head">Remember me</label>
									</div>
								</div>
								<div class="button login-button preload">Sign in</div>
								<div class="ilcenter preload">
									<a href="?page=register" class="action-link">Sign up</a>
								</div>
								<div class="ilcenter preload">
									<a href="?page=forgot-password" class="action-link">Forgot password</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 nopadding hidden-xs">
						<div class="sides right-side">
							<img class="imac preload" src="img/imac.png">
							<img class="iphone preload" src="img/iphone.png">
							<img class="nexus preload" src="img/nexus.png">
						</div>
					</div>
				</div>
				<?php
				break;
			}
			case 'register':
			{
				?>
				<div class="row nomargin login-page">
					<div class="col-sm-6 nopadding">
						<div class="sides left-side ohidden">
							<div class="register-complete hidden">
								<img class="preload" src="img/javelin-logo.jpg">
								<div class="title preload">Done</div>
								<span>Confirmation email was sent to you</span>
								<div class="ilcenter preload">
									<a href="?page=login" class="action-link white">Sign in</a>
								</div>
							</div>
							<div class="input-block margin-top-70">
								<img class="preload" src="img/javelin-logo.jpg">
								<div class="title preload">Sign up</div>
								<div class="required-field">Field required</div>
								<input class="preload input-check f-name" type="text" placeholder="First name">
								<input class="preload input-check s-name" type="text" placeholder="Second name">
								<input class="preload input-check user-email" type="text" placeholder="Email">
								<input class="preload input-check password" type="password" placeholder="Password">
								<input class="preload input-check password-again" type="password" placeholder="Password again">
								<div class="ilcenter preload margin-15">
									<a href="test.php" class="action-link">Terms and conditions</a>
								</div>
								<div class="login-button button preload">Sign up</div>
								<div class="ilcenter preload">
									<a href="?page=login" class="action-link">Sign in</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 nopadding hidden-xs">
						<div class="sides right-side ohidden">
							<img class="imac preload" src="img/imac.png">
							<img class="iphone preload" src="img/iphone.png">
							<img class="nexus preload" src="img/nexus.png">
						</div>
					</div>
				</div>
				<?php
				break;
			}
			case 'faqs':
			{
				?>
				<div class="content-area">
					<div class="page-head clearfix">
						<img class="floatleft"src="img/chat-icon.png">
						<div class="title floatleft">Frequently asked questions</div>
					</div>
					<div class="row">
						<!-- ================== -->
						<!-- GREAT COMMENT MARK -->
						<!-- ================== -->
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">Who do I contact if I have a question?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">For general or technical support questions about the program or portal, email: assessments@javelinhr.com.<br><br>Support services are available from 9am – 5pm EST Monday to Friday.</div>
							</div>
						</div>
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">What competencies do Javelin Learning Solutions measure in the simulations?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">Javelin Learning Solutions measure 3 core competencies: Communication, Empathy, and Leadership.</div>
							</div>
						</div>
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">When will I receive my scores and feedback?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">You will receive notification via email that your scores and feedback reports are ready <strong>2-3 days after</strong> you submit your completed simulation.</div>
							</div>
						</div>
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">Do you have any tips for answering observe and action questions?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">Tips for answering the Observe questions:<br>
									<ul>
										<li><span>Consider both verbal and nonverbal actions</span></li>
										<li><span>Clearly articulate main points</span></li>
										<li><span>Speak at an appropriate pace</span></li>
										<li><span>Provide a complete response</span></li>
									</ul>
									<br>Tips for answering the Action questions:<br>
									<ul>
										<li><span>Maintain eye contact when appropriate</span></li>
										<li><span>Maintain positive body language</span></li>
										<li><span>Respond directly to the character in the question</span></li>
										<li><span>Ask questions when appropriate</span></li>
										<li><span>Avoid distracting behaviors</span></li>
										<li><span>Use volume and tone of voice for increased emphasis</span></li>
									</ul>
								</div>
							</div>
						</div>
					<!-- ================== -->
					<!-- GREAT COMMENT MARK -->
					<!-- ================== -->
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">What is the goal of Javelin Learning Solutions?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">The goal of this program is to develop, improve, and sustain communication, empathy, and leadership competencies through high-fidelity video simulations, individualized feedback, and coaching.</div>
							</div>
						</div>
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">How many questions are on my simulation?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">A total of 5 <strong>questions</strong> are on your simulation: 4 scenario-based questions and 1 instructional demo video question.<br>Each simulation is comprised of 2 unique job scenarios. For each scenario, you will be asked to respond to two questions: an observe question and an action question. At the start of each simulation, there is 1 instructional demo video question to ensure you watch our video on how to respond to each observe and action question.</div>
							</div>
						</div>
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">Do I have to complete the whole simulation at one time?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">We recommend completing the entire simulation in one sitting, but you do have the ability to exit the simulation platform and continue later where you left off.</div>
							</div>
						</div>
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">Who is Javelin Learning Solutions Solutions?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">Javelin is an industrial-organizational consultancy aimed at improving workforces via simulations and assessments. The company has administered over 500,000 simulations for healthcare and business customers since 1985. Please visit <a href="http://www.javelinlearningsolutions.com/" class="link">www.javelinlearningsolutions.com</a> for more information about our company.</div>
							</div>
						</div>
					<!-- ================== -->
					<!-- GREAT COMMENT MARK -->
					<!-- ================== -->
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">How does the program work?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">There are three steps to the program:<br>1. You will watch a series of real-life video scenarios, provide video responses to each scenario and electronically submit each recorded video response<br>
									2. Your submitted video responses are scored by our team of certified assessors utilizing behaviorally-based scoring checklists<br>
									3. Your results are presented based on the subscription package your program has chosen. For more information, please contact your program administrator.
								</div>
							</div>
						</div>
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">How long does a simulation take?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">From start to finish, the simulation will take approximately <strong>15 – 30 minutes</strong>, depending on how many times you choose to re-record your responses.</div>
							</div>
						</div>
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">I’m new to the Javelin Learning Solutions. Do you have any tips for taking my simulation?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">
									<ul>
										<li><span>Read and follow the instructions carefully</span></li>
										<li><span>Test your webcam and audio</span></li>
										<li><span>Ensure you have a reliable internet connection</span></li>
										<li><span>Record your video responses in a quiet room with ample lighting and a simple background</span></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="qa-block preload">
							<div class="question clearfix">
								<div class="icon"></div>
								<div class="content">What other companies has Javelin Learning Solutions Solutions worked with?</div>
							</div>
							<div class="answer clearfix">
								<div class="icon"></div>
								<div class="content">Javelin’s parent company has worked with companies for over 30 years, including the following:<br>
									<ul>
										<li><span>GlaxoSmithKline</span></li>
										<li><span>ViiV Healthcare</span></li>
										<li><span>Bristol Myers</span></li>
										<li><span>ITT</span></li>
										<li><span>AIG</span></li>
										<li><span>Philip Morris</span></li>
										<li><span>Alcoa Foundation</span></li>
										<li><span>Ameriprise</span></li>
										<li><span>NASA</span></li>
										<li><span>Olin Corporation</span></li>
										<li><span>Sherwin Alumina Company</span></li>
										<li><span>McDonnell Douglas</span></li>
										<li><span>American Express</span></li>
										<li><span>University of Hawai’i</span></li>
										<li><span>University of Louisville</span></li>
										<li><span>Oak Hill Hospital</span></li>
										<li><span>Brandon Regional Hospital</span></li>
										<li><span>Aventura Hospital</span></li>
										<li><span>University of Texas Medical Branch</span></li>
										<li><span>Dayton VA Medical Center</span></li>
										<li><span>University of Miami</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				break;
			}
			default:
			{
				header('Location: ?page=overview');
				break;
			}
		}
		if(isset($_GET['page']) && $_GET['page'] != 'register' && $_GET['page'] != 'login' && $_GET['page'] != 'confirm-account' && $_GET['page'] != 'forgot-password' && $_GET['page'] != 'restore-password')
		{
		?>
		<footer>
			<div class="links">
				<a href="?page=terms-of-use">Terms of use</a>
				<a href="">Support</a>
			</div>
			<div class="copr">© ePsychometrics Inc 2016</div>
		</footer>
		<?php
		}
	}

	ob_flush();
	?>
	</div>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery-easing.js"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script src="https://assets-cdn.ziggeo.com/v1-stable/ziggeo.js"></script>
	<script> ZiggeoApi.token = "7e796a759bc43e47ab7d80957adae320";</script>
	<script type="text/javascript" src="js/less.min.js"></script>
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>