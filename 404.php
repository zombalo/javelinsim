<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
	<link rel="stylesheet" href="stylesheet/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/less" href="stylesheet/test.less">
</head>
<body>
	<div class="not-found-page">
		<span>Oops, this page not found!</span>
		<div class="ilcenter">
			<a href="index.php">
				<div class="button not-found-button">Home page <i class="fa fa-home"></i></div>
			</a>
		</div>
	</div>
	<script type="text/javascript" src="js/less.min.js"></script>
</body>
</html>